<?php
/**
 * routes.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */
$routes = $routeFiles = [];

// Load all route files
foreach (new DirectoryIterator(__DIR__ . '/routes/') as $fileInfo) {
    if (false === $fileInfo->isDot()) {
        $routeFiles[] = $fileInfo->getPathname();
    }
}

// Natural sort
uksort($routeFiles, 'strnatcasecmp');

// Reverse and iterate
foreach (array_reverse($routeFiles) as $routeFile) {
    $routes = array_merge($routes, require $routeFile);
}

return $routes;
