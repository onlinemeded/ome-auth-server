<?php
/**
 * 00-default.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */
return [
    // Health
    'get@/v1/heartbeat[/]' => [
        'controller' => 'Domain\\Platform\\Controller\\HealthController',
        'action'     => 'heartbeat',
        'feature'    => 'core'
    ]
];
