<?php
/**
 * 01-auth.php
 *
 * @Copyright 2019 OnlineMedEd, LLC
 */
return [
    // SSO
    'post@/v1/sso/resource'                                => [
        'controller' => 'Application\\Http\\Controller\\Auth\\ResourceController',
        'action'     => 'post',
        'feature'    => 'auth'
    ],
    'post@/v1/sso/token'                                   => [
        'controller' => 'Application\\Http\\Controller\\Auth\\AccessTokenController',
        'action'     => 'post',
        'feature'    => 'auth'
    ],
    'get@/v1/sso/authorize'                                => [
        'controller' => 'Application\\Http\\Controller\\Auth\\AuthorizationCodeController',
        'action'     => 'get',
        'feature'    => 'auth'
    ],
    'post@/v1/sso/authenticate'                            => [
        'controller' => 'Application\\Http\\Controller\\Auth\\AuthenticationController',
        'action'     => 'post',
        'feature'    => 'auth'
    ],
    // Apps
    'get@/v1/apps[/]'                                      => [
        'controller' => 'Application\\Http\\Controller\\Auth\\ApiApplicationController',
        'action'     => 'listAll',
        'feature'    => 'auth:apps'
    ],
    // Client
    'get@/v1/clients[/]'                                   => [
        'controller' => 'Application\\Http\\Controller\\Auth\\ClientController',
        'action'     => 'listAll',
        'feature'    => 'auth:client'
    ],
    'get@/v1/client/{identifier}'                          => [
        'controller' => 'Application\\Http\\Controller\\Auth\\ClientController',
        'action'     => 'get',
        'feature'    => 'auth:client'
    ],
    // Users
    'get@/v1/client/{client_identifier}/users[/]'          => [
        'controller' => 'Application\\Http\\Controller\\Auth\\UsersController',
        'action'     => 'listAll',
        'feature'    => 'auth:users'
    ],
    'get@/v1/client/{client_identifier}/user/{identifier}' => [
        'controller' => 'Application\\Http\\Controller\\Auth\\UsersController',
        'action'     => 'get',
        'feature'    => 'auth:users'
    ]
];
