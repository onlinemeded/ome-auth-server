<?php
/**
 * cli-config.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */
define('BASE_DIR', realpath(__DIR__) . '/../');

require BASE_DIR . 'vendor/autoload.php';

use Infrastructure\Configuration\ConfigurationManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;

try {
    $configManager = new ConfigurationManager;
    $configManager->addConfigurationFromArray(
        require (false === array_key_exists('TEST', $_SERVER) || 1 !== (int)$_SERVER['TEST'])
            ? BASE_DIR . '/config/app.php'
            : BASE_DIR . '/tests/phpunit/app.php'
    );

    $databaseConfig = $configManager->getDirective('database');

    $doctrineConfig = Setup::createYAMLMetadataConfiguration(
        $databaseConfig['path']['yaml'],
        $databaseConfig['devMode'],
        $databaseConfig['connection']['default']['proxy']['path']
    );

    // Proxies
    $doctrineConfig->setProxyNamespace(
        $databaseConfig['connection']['default']['proxy']['namespace']
    );

    $entityManager = EntityManager::create(
        $databaseConfig['connection']['default'],
        $doctrineConfig
    );

    return ConsoleRunner::createHelperSet($entityManager);
} catch (Exception $e) {
    echo sprintf('Could not run console. %s', $e->getMessage());
}
