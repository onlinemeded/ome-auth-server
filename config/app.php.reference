<?php
/**
 * app.php.reference
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */

use Application\Http\ApplicationInterface;
use Monolog\Logger;

return [
    'env'           => ApplicationInterface::MODE_DEVELOPMENT,
    'appKey'        => 'somesecretkeyforapp',
    'database'      => [
        'devMode'    => true,
        'connection' => [
            'default' => [
                'driver'        => 'pdo_mysql',
                'host'          => '__DB_HOST__',
                'dbname'        => '__DB_NAME__',
                'user'          => '__DB_USER__',
                'password'      => '__DB_PASS__',
                'port'          => __DB_PORT__,
                'charset'       => 'UTF8',
                'driverOptions' => [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
                ],
                'proxy'         => [
                    'path'      => BASE_DIR . 'storage/proxy',
                    'namespace' => 'DoctrineProxy'
                ]
            ]
        ],
        'path'       => [
            'metadata' => [BASE_DIR . 'src/Domain/Auth/Infrastructure/Entity'],
            'yaml'     => [BASE_DIR . 'model']
        ],
        'cache'      => [
            'metadata' => 'Doctrine\\Common\\Cache\\ArrayCache',
            'query'    => 'Doctrine\\Common\\Cache\\ArrayCache',
            'result'   => 'Doctrine\\Common\\Cache\\ArrayCache'
        ]
    ],
    'errorHandling' => [
        'handlers' => [
            'phpErrorHandler'   => [
                'controller' => 'Application\\Http\\Controller\\ErrorController',
                'action'     => 'phpError'
            ],
            'errorHandler'      => [
                'controller' => 'Application\\Http\\Controller\\ErrorController',
                'action'     => 'error'
            ],
            'notFoundHandler'   => [
                'controller' => 'Application\\Http\\Controller\\ErrorController',
                'action'     => 'notFound'
            ],
            'notAllowedHandler' => [
                'controller' => 'Application\\Http\\Controller\\ErrorController',
                'action'     => 'notAllowed'
            ]
        ]
    ],
    'features'      => [
        'auth' => [
            'name'    => 'Auth',
            'enabled' => true
        ]
    ],
    'routing'       => [
        'routes' => require __DIR__ . '/routes.php'
    ],
    'logging'       => [
        'name'     => 'system',
        'handlers' => [
            'Monolog\\Handler\\StreamHandler' => [
                BASE_DIR . 'storage/logs/system.log',
                Logger::ERROR
            ]
        ]
    ],
    'slim'          => [
        'settings' => [
            'displayErrorDetails'               => true,
            'determineRouteBeforeAppMiddleware' => true
        ]
    ],
    'middleware'    => [
        'jwt'           => [
            'secure'    => false,
            'algorithm' => ['HS512'],
            'path'      => ['/v1'],
            'ignore'    => ['/v1/heartbeat', '/v1/sso/login'],
            'nbf'       => 10,
            'exp'       => 10,
            'relaxed'   => []
        ],
        'signedRequest' => [
            'headerName'     => 'OME-SIG',
            'headerDateName' => 'OME-Date',
            'timeToLive'     => 30,
            'ignore'         => ['/v1/heartbeat', '/v1/sso/login']
        ],
        'cors'          => [
            'origin'         => ['*'],
            'methods'        => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
            'headers.allow'  => [
                'X-Requested-With',
                'Content-Type',
                'Accept',
                'Origin',
                'Authorization',
                'OME-SIG',
                'OME-Date'
            ],
            'headers.expose' => [],
            'credentials'    => false,
            'cache'          => 0
        ],
        'oauth2'        => [
            'access_lifetime'        => 7200,
            'auth_code_lifetime'     => 30,
            'refresh_token_lifetime' => 30
        ]
    ]
];
