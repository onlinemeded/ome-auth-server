<?php
/**
 * EntityManagerTest.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Infrastructure\Entity;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use Infrastructure\Configuration\ConfigurationManager;
use Infrastructure\Entity\EntityManager;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;

/**
 * Class EntityManagerTest
 */
class EntityManagerTest extends BaseTestObject
{
    /**
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testCreation()
    {
        $config = ApplicationFactory::getContainerStaticProxy()->offsetGet(
            ConfigurationManager::class
        )->getDirective(
            'database'
        );

        $doctrineConfig = Setup::createAnnotationMetadataConfiguration(
            $config['path']['metadata'],
            $config['devMode'],
            $config['connection']['default']['proxy']['path']
        );

        $doctrineConfig->setMetadataDriverImpl(
            new AnnotationDriver(
                new AnnotationReader,
                $config['path']['metadata']
            )
        );

        $instance = EntityManager::create(
            $config['connection']['default'],
            $doctrineConfig
        );

        $this->assertInstanceOf(EntityManager::class, $instance);
    }
}
