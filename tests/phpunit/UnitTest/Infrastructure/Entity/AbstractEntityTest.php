<?php
/**
 * AbstractEntityTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Infrastructure\Entity;

use UnitTest\BaseTestObject;

/**
 * Class AbstractEntityTest
 */
class AbstractEntityTest extends BaseTestObject
{
    const TEST_VALUE_1 = 'someRandomValue';
    const TEST_VALUE_2 = 'someOtherRandomValue';

    /**
     *
     */
    public function testEncryptField()
    {
        $sut = $this->newSUT();

        $result = $sut->testEncryptField(self::TEST_VALUE_1);

        $this->assertTrue(password_verify(self::TEST_VALUE_1, $result));
    }

    /**
     *
     */
    public function testVerifyEncryptedFieldValue()
    {
        $sut = $this->newSUT();

        $result = $sut->testEncryptField(self::TEST_VALUE_2);

        $this->assertEquals(
            password_verify(self::TEST_VALUE_2, $result),
            $sut->testVerifyEncryptedFieldValue($result, self::TEST_VALUE_2)
        );
    }

    /**
     * @return TestEntity
     */
    private function newSUT(): TestEntity
    {
        return new TestEntity;
    }
}
