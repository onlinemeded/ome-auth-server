<?php
/**
 * TestEntity.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Infrastructure\Entity;

use Infrastructure\Entity\AbstractEntity;

/**
 * Class TestEntity
 */
class TestEntity extends AbstractEntity
{
    /**
     * @param $value
     *
     * @return bool|string
     */
    public function testEncryptField($value)
    {
        return $this->encryptField($value);
    }

    /**
     * @param string $encryptedValue
     * @param string $value
     *
     * @return bool
     */
    public function testVerifyEncryptedFieldValue($encryptedValue, $value): bool
    {
        return $this->verifyEncryptedFieldValue($encryptedValue, $value);
    }
}
