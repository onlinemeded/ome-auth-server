<?php
/**
 * EntityBackedTestObject.php
 *
 * @copyright 2019 Patrick Loven. All Rights Reserved.
 */

namespace UnitTest\Infrastructure\Entity;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use Application\Http\Application;
use Infrastructure\Entity\AbstractEntity;
use Infrastructure\Entity\EntityManager;

/**
 * Class EntityBackedTestObject
 */
class EntityBackedTestObject extends BaseTestObject
{
    /**
     * @var EntityManager
     */
    protected static $em;

    /**
     * @var AbstractEntity[];
     */
    protected static $entities = [];

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$em = ApplicationFactory::getContainerStaticProxy()->offsetGet(Application::APP_ENTITY_MANAGER);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public static function tearDownAfterClass()
    {
        foreach (array_reverse(self::$entities) as $entity) {
            self::$em->remove($entity);
            self::$em->flush($entity);
        }

        parent::tearDownAfterClass();
    }
}
