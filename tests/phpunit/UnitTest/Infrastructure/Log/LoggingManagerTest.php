<?php
/**
 * LoggingManagerTest.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Infrastructure\Log;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use Infrastructure\Configuration\ConfigurationManager;
use Infrastructure\InfrastructureException;
use Infrastructure\Log\LoggingManager;

/**
 * Class LoggingManagerTest
 */
class LoggingManagerTest extends BaseTestObject
{
    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     * @throws \Infrastructure\InfrastructureException
     */
    public function testCreation()
    {
        $config = ApplicationFactory::getContainerStaticProxy()->offsetGet(
            ConfigurationManager::class
        )->getDirective('logging');
        $instance = LoggingManager::create(
            $config['name'],
            $config['handlers']
        );

        $this->assertInstanceOf(LoggingManager::class, $instance);
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testCreationException()
    {
        $config = ApplicationFactory::getContainerStaticProxy()->offsetGet(
            ConfigurationManager::class
        )->getDirective('logging');
        $config['handlers'] = array_merge(
            [
                'Non\\Existent\\Handler' => []
            ],
            $config['handlers']
        );

        $this->assertThrows(
            function (array $config) {
                LoggingManager::create($config['name'], $config['handlers']);
            },
            new InfrastructureException,
            $config
        );
    }
}
