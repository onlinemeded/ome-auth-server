<?php
/**
 * AesHmacCryptographerTest.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Infrastructure\Crypto\Cryptogropher;

use Infrastructure\Crypto\Cryptographer\AesHmacCryptographer;
use UnitTest\BaseTestObject;

/**
 * Class AesHmacCryptographerTest
 */
class AesHmacCryptographerTest extends BaseTestObject
{
    const AUTH_KEY = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';

    /**
     * @throws \Exception
     */
    public function testEncryptThenDecrypt()
    {
        $sut = $this->newSUT();
        $payload = bin2hex(random_bytes(256));
        $encrypted = $sut->encrypt($payload);

        $this->assertInternalType('string', $encrypted);

        $decrypted = $sut->decrypt($encrypted);

        $this->assertInternalType('string', $decrypted);
        $this->assertEquals($payload, $decrypted);
    }

    /**
     * @throws \Exception
     */
    public function testEncryptThenDecryptSerialized()
    {
        $sut = $this->newSUT();
        $payload = $_SERVER;
        $encrypted = $sut->encrypt($payload, true);

        $this->assertInternalType('string', $encrypted);

        $decrypted = $sut->decrypt($encrypted, true);

        $this->assertInternalType('array', $decrypted);
        $this->assertEquals($payload, $decrypted);
    }

    /**
     * @throws \Exception
     */
    public function testEncryptThenDecryptInvalidReturn()
    {
        $sut = $this->newSUT();
        $payload = bin2hex(random_bytes(256));
        $encrypted = $sut->encrypt($payload);

        $this->assertNull($sut->decrypt('extraInformationNotNeeded' . $encrypted));
    }

    /**
     * @throws \Exception
     */
    public function testInvalidHmacNullReturn()
    {
        $value = bin2hex(random_bytes(256));
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(AesHmacCryptographer::CIPHER_METHOD_AES_256_CBC));
        $salt = openssl_random_pseudo_bytes(16);
        $encrypted = openssl_encrypt(
            $value,
            AesHmacCryptographer::CIPHER_METHOD_AES_256_CBC,
            hex2bin(hash_pbkdf2('sha512', self::AUTH_KEY, $salt, 1, 256 / 4)),
            OPENSSL_RAW_DATA,
            $iv
        );
        $payload = base64_encode($encrypted);
        $hmac = hash_hmac(AesHmacCryptographer::HMAC_ALGO_SHA_256, $payload, self::AUTH_KEY);

        $request = base64_encode(bin2hex($iv) . bin2hex($salt) . str_repeat('a', 6) . substr($hmac, 6) . $payload);

        $this->assertNull($this->newSUT()->decrypt($request));
    }

    /**
     * @param string $key
     *
     * @return AesHmacCryptographer
     */
    private function newSUT($key = self::AUTH_KEY): AesHmacCryptographer
    {
        return new AesHmacCryptographer($key);
    }
}
