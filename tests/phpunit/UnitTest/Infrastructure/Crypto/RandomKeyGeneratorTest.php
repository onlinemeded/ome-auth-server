<?php
/**
 * RandomKeyGeneratorTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Infrastructure\Crypto;

use Infrastructure\Crypto\RandomKeyGenerator;
use UnitTest\BaseTestObject;

/**
 * Class RandomKeyGeneratorTest
 */
class RandomKeyGeneratorTest extends BaseTestObject
{
    /**
     *
     */
    public function testGenerateKey(): void
    {
        $length = 15;
        $result = RandomKeyGenerator::generateKey($length, RandomKeyGenerator::USE_LOWERCASE);

        $this->assertIsString($result);
        $this->assertEquals($length, strlen($result));
    }

    /**
     *
     */
    public function testGenerateKeyEmptyReturn(): void
    {
        $result = RandomKeyGenerator::generateKey(15);

        $this->assertEmpty($result);
    }
}
