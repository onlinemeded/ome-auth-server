<?php
/**
 * FeatureManagerTest.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Infrastructure\Feature;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use Infrastructure\Configuration\ConfigurationManager;
use Infrastructure\Feature\FeatureManager;
use Opensoft\Rollout\Storage\ArrayStorage;

/**
 * Class FeatureManagerTest
 */
class FeatureManagerTest extends BaseTestObject
{
    /**
     * @var array
     */
    private $config = [
        'auth'                   => [
            'name'    => 'Auth',
            'enabled' => true
        ],
        'platform.management'    => [
            'name'    => 'Platform Management',
            'enabled' => true,
            'acl'     => [
                'group' => ['admin']
            ]
        ],
        'platform.configuration' => [
            'name'    => 'Platform Configuration',
            'enabled' => true,
            'acl'     => [
                'group' => ['admin']
            ]
        ]
    ];

    /**
     *
     */
    public function testCreation()
    {
        $instance = FeatureManager::create(new ArrayStorage);

        $instance->defineGroup(
            'all',
            function () {
                return true;
            }
        );

        $instance->activate('core');

        foreach ($this->config as $feature => $settings) {
            if (true === $settings['enabled']) {
                $instance->activate($feature);
                continue;
            }

            if (true === array_key_exists('acl', $settings)) {
                foreach (['group', 'user'] as $permissionLevel) {
                    if (true === array_key_exists($permissionLevel, $settings['acl'])) {
                        foreach ($settings['acl'][$permissionLevel] as $level) {
                            $instance->{'activate' . ucfirst($permissionLevel)}($feature, $level);
                        }
                    }
                }
            }
        }

        $this->assertInstanceOf(FeatureManager::class, $instance);
    }
}
