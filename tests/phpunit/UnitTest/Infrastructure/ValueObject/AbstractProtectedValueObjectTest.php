<?php
/**
 * AbstractProtectedValueObjectTest.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Infrastructure\ValueObject;

use Infrastructure\ValueObject\AbstractProtectedValueObject;
use UnitTest\BaseTestObject;
use Infrastructure\Exception\Runtime\BadMethodCallException;
use Infrastructure\Exception\Runtime\InvalidArgumentException;
use Infrastructure\Exception\RuntimeException;

/**
 * Class AbstractProtectedValueObjectTest
 */
class AbstractProtectedValueObjectTest extends BaseTestObject
{
    const PROPERTY_1 = 'property1';
    const PROPERTY_2 = 'property2';
    const PROPERTY_3 = 'property3';
    const PROPERTY_4 = 'property4';

    /**
     *
     */
    public function testGetProperties()
    {
        $valueObject = $this->newSUT();

        foreach ($this->getPropertiesWithValues() as $property => $value) {
            $this->assertEquals($value, $valueObject->{$property});
        }
    }

    /**
     *
     */
    public function testGetInvalidPropertyException()
    {
        $this->assertThrows(
            function (AbstractProtectedValueObject $valueObject) {
                $valueObject->nonExistentProperty;
            },
            new InvalidArgumentException,
            $this->newSUT()
        );
    }

    /**
     *
     */
    public function testSetPropertyException()
    {
        $this->assertThrows(
            function (AbstractProtectedValueObject $valueObject) {
                $valueObject->{self::PROPERTY_1} = 'new value';
            },
            new RuntimeException,
            $this->newSUT()
        );
    }

    /**
     *
     */
    public function testBadFunctionCallException()
    {
        foreach (['some', 'random', 'methods'] as $method) {
            $this->assertThrows(
                function (AbstractProtectedValueObject $valueObject, $method) {
                    $valueObject->{$method}();
                },
                new BadMethodCallException,
                $this->newSUT(),
                $method
            );
        }
    }

    /**
     *
     */
    public function testToArrayConversion()
    {
        $valueObject = $this->newSUT();

        $this->assertEquals(
            $this->getPropertiesWithValues(),
            $valueObject->toArray()
        );
    }

    /**
     *
     */
    public function testToArrayConversionWithNestedValueObject()
    {
        $valueObject = $this->newSUT();
        $nestedValueObject = $this->newSUT();
        $properties = $this->getPropertiesWithValues();
        $randomProperty = rand(0, 3);
        $randomPropertyKey = array_keys($properties)[$randomProperty];

        /** @var AbstractProtectedValueObject $valueObject */
        $valueObject = $valueObject->{'with' . ucfirst($randomPropertyKey)}(
            $nestedValueObject
        );
        $properties[$randomPropertyKey] = $nestedValueObject->toArray();

        $this->assertEquals(
            $properties,
            $valueObject->toArray()
        );
    }

    /**
     * @return TestProtectedValueObject
     */
    private function newSUT()
    {
        return TestProtectedValueObject::create(
            self::PROPERTY_1,
            self::PROPERTY_2,
            self::PROPERTY_3,
            self::PROPERTY_4
        );
    }

    /**
     * @return array
     */
    private function getPropertiesWithValues()
    {
        return [
            'property1' => self::PROPERTY_1,
            'property2' => self::PROPERTY_2,
            'property3' => self::PROPERTY_3,
            'property4' => self::PROPERTY_4
        ];
    }
}
