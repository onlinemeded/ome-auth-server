<?php
/**
 * TestProtectedValueObject.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Infrastructure\ValueObject;

use Infrastructure\ValueObject\AbstractProtectedValueObject;

/**
 * Class TestProtectedValueObject
 *
 * @param mixed $property1
 * @param mixed $property2
 * @param mixed $property3
 * @param mixed $property4
 *
 * @method TestProtectedValueObject withProperty1($property1)
 * @method TestProtectedValueObject withProperty2($property2)
 * @method TestProtectedValueObject withProperty3($property3)
 * @method TestProtectedValueObject withProperty4($property4)
 */
class TestProtectedValueObject extends AbstractProtectedValueObject
{
    /**
     * @param mixed $property1
     * @param mixed $property2
     * @param mixed $property3
     * @param mixed $property4
     *
     * @return TestProtectedValueObject
     */
    final public static function create(
        $property1,
        $property2,
        $property3,
        $property4
    ) {
        return new self(
            [
                'property1' => $property1,
                'property2' => $property2,
                'property3' => $property3,
                'property4' => $property4
            ]
        );
    }
}
