<?php
/**
 * ConfigurationManagerTest.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Infrastructure\Configuration;

use UnitTest\BaseTestObject;
use Infrastructure\Configuration\ConfigurationManager;

/**
 * Class ConfigurationManagerTest
 */
class ConfigurationManagerTest extends BaseTestObject
{
    /**
     * @var array
     */
    private static $sutConfig = [
        'directive1' => [
            'property1' => 'propertyOne',
            'property2' => 'propertyTwo',
            'property3' => 'propertyThree',
            'property4' => 'propertyFour'
        ]
    ];
    
    /**
     *
     */
    public function testGetConfigurationMissingDirective()
    {
        $sut = $this->newSUT();
        
        foreach ($this->getPrimitiveTypesArray() as $defaultReturn) {
            $this->assertEquals(
                $defaultReturn,
                $sut->getDirective('', $defaultReturn)
            );
        }
    }
    
    /**
     *
     */
    public function testGetConfigurationDirective()
    {
        $sut = $this->newSUT();
        $sut->addConfigurationFromArray(self::$sutConfig);
        
        foreach (self::$sutConfig as $directive => $properties) {
            $this->assertEquals(
                $properties,
                $sut->getDirective($directive)
            );
        }
    }
    
    /**
     * @return ConfigurationManager
     */
    private function newSUT()
    {
        return new ConfigurationManager;
    }
}
