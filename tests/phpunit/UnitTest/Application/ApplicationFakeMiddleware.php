<?php
/**
 * ApplicationFakeMiddleware.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Application;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ApplicationFakeMiddleware
 */
class ApplicationFakeMiddleware
{
    /**
     * @var int
     */
    private $testValue = 0;

    /**
     * @param int $testValue
     */
    public function __construct(int $testValue)
    {
        $this->testValue = $testValue;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param callable $next
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        return $response->withJson(['unitTest' => $this->testValue]);
    }
}
