<?php
/**
 * ApplicationFactory.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Application;

use Application\Http\ApplicationInterface;
use Domain\Auth\Factory\RepositoryFactory;
use Domain\Auth\Factory\UseCaseFactory;
use Domain\Auth\Infrastructure\Entity\OAuthAccessTokenEntity;
use Domain\Auth\Infrastructure\Entity\OAuthAuthorizationCodeEntity;
use Domain\Auth\Infrastructure\Entity\OAuthAuthorizationCodeRepository;
use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;
use Domain\Auth\Infrastructure\Entity\OAuthClientRepository;
use Domain\Auth\Infrastructure\Entity\OAuthRefreshTokenEntity;
use Domain\Auth\Infrastructure\Entity\OAuthRefreshTokenRepository;
use Domain\Auth\Infrastructure\Entity\OAuthUserEntity;
use Domain\Auth\Infrastructure\Entity\OAuthUserRepository;
use Infrastructure\Configuration\ConfigurationManager;
use Infrastructure\DI\Container;
use Infrastructure\Entity\EntityManager;
use Infrastructure\Feature\FeatureManager;
use Infrastructure\Log\LoggingManager;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\Setup;
use OAuth2\GrantType\UserCredentials;
use Opensoft\Rollout\Storage\ArrayStorage;
use OAuth2\GrantType\ClientCredentials;
use OAuth2\GrantType\AuthorizationCode;
use OAuth2\GrantType\RefreshToken;
use OAuth2\Server as OAuthServer;
use Exception;

/**
 * Class ApplicationFactory
 */
class ApplicationFactory
{
    /**
     * @return Container
     */
    public static function getContainerStaticProxy(): Container
    {
        static $instance = null;

        if (null === $instance) {
            try {
                $instance = new Container(
                    [
                        ApplicationInterface::APP_CONFIG_MANAGER  => function () {
                            $configManager = new ConfigurationManager;
                            $configManager->addConfigurationFromArray(
                                require BASE_DIR . 'tests/phpunit/app.php'
                            );

                            return $configManager;
                        },
                        ApplicationInterface::APP_LOGGING_MANAGER => function (Container $container) {
                            $config = $container->offsetGet(ConfigurationManager::class)
                                ->getDirective('logging');

                            return LoggingManager::create($config['name'], $config['handlers']);
                        },
                        ApplicationInterface::APP_ENTITY_MANAGER  => function (Container $container) {
                            $config = $container->offsetGet(ConfigurationManager::class)
                                ->getDirective('database');

                            // Metadata
                            $doctrineConfig = Setup::createAnnotationMetadataConfiguration(
                                $config['path']['metadata'],
                                $config['devMode'],
                                $config['connection']['default']['proxy']['path'],
                                new $config['cache']['metadata']
                            );

                            $doctrineConfig->setMetadataDriverImpl(
                                new AnnotationDriver(
                                    new AnnotationReader,
                                    $config['path']['metadata']
                                )
                            );

                            // Cache
                            $doctrineConfig->setQueryCacheImpl(new $config['cache']['query']);
                            $doctrineConfig->setResultCacheImpl(new $config['cache']['result']);

                            // Proxies
                            $doctrineConfig->setAutoGenerateProxyClasses(false);
                            $doctrineConfig->setProxyNamespace(
                                $config['connection']['default']['proxy']['namespace']
                            );

                            return EntityManager::create(
                                $config['connection']['default'],
                                $doctrineConfig
                            );
                        },
                        ApplicationInterface::APP_FEATURE_MANAGER => function (Container $container) {
                            $featureManager = FeatureManager::create(new ArrayStorage);
                            $config = $container->offsetGet(ConfigurationManager::class)
                                ->getDirective('features');

                            $featureManager->defineGroup(
                                'all',
                                function () {
                                    return true;
                                }
                            );

                            foreach ($config as $feature => $settings) {
                                if (true === $settings['enabled']) {
                                    $featureManager->activate($feature);
                                    continue;
                                }

                                if (true === is_array($settings['enabled'])) {
                                    foreach (['group', 'user'] as $permissionLevel) {
                                        if (true === array_key_exists(
                                                $permissionLevel,
                                                $settings['enabled']
                                            )) {
                                            foreach ($settings['enabled'][$permissionLevel] as $level) {
                                                $featureManager->{'activate' . ucfirst(
                                                    $permissionLevel
                                                )}(
                                                    $feature,
                                                    $level
                                                );
                                            }
                                        }
                                    }
                                }
                            }

                            return $featureManager;
                        },
                        ApplicationInterface::APP_OAUTH_SERVER    => function (Container $container) {
                            $config = $container->offsetGet(ConfigurationManager::class)->getDirective(
                                'middleware'
                            )['oauth2'];

                            /**
                             * @var EntityManager                    $entityManager
                             * @var OAuthUserRepository              $userStorage
                             * @var OAuthClientRepository            $clientStorage
                             * @var OAuthAuthorizationCodeRepository $authorizationCodeStorage
                             * @var OAuthRefreshTokenRepository      $refreshTokenStorage
                             */
                            $entityManager = $container->offsetGet(ApplicationInterface::APP_ENTITY_MANAGER);
                            $userStorage = $entityManager->getRepository(OAuthUserEntity::class);
                            $clientStorage = $entityManager->getRepository(OAuthClientEntity::class);
                            $authorizationCodeStorage = $entityManager->getRepository(
                                OAuthAuthorizationCodeEntity::class
                            );
                            $refreshTokenStorage = $entityManager->getRepository(OAuthRefreshTokenEntity::class);

                            return new OAuthServer(
                                [
                                    'client_credentials' => $clientStorage,
                                    'user_credentials'   => $userStorage,
                                    'access_token'       => $entityManager->getRepository(
                                        OAuthAccessTokenEntity::class
                                    ),
                                    'authorization_code' => $authorizationCodeStorage,
                                    'refresh_token'      => $refreshTokenStorage
                                ],
                                $config,
                                [
                                    new UserCredentials($userStorage),
                                    new ClientCredentials($clientStorage),
                                    new AuthorizationCode($authorizationCodeStorage),
                                    new RefreshToken($refreshTokenStorage)
                                ]
                            );
                        },
                        RepositoryFactory::class                  => function (Container $container) {
                            return new RepositoryFactory(
                                $container->offsetGet(ApplicationInterface::APP_ENTITY_MANAGER),
                                $container->offsetGet(ApplicationInterface::APP_OAUTH_SERVER)
                            );
                        },
                        UseCaseFactory::class                     => function (Container $container) {
                            return new UseCaseFactory($container->offsetGet(RepositoryFactory::class));
                        }
                    ]
                );

                // load data fixtures
                /** @var EntityManager $em */
                $em = $instance->offsetGet(ApplicationInterface::APP_ENTITY_MANAGER);

                // create tables if missing
                if (false === $em->getConnection()->getSchemaManager()->tablesExist(['oauth_access_token'])) {
                    (new SchemaTool($em))->createSchema($em->getMetadataFactory()->getAllMetadata());
                }

                $em->getConnection()->exec('SET FOREIGN_KEY_CHECKS = 0;');

                $loader = new Loader;
                $loader->loadFromDirectory(__DIR__ . '/../Domain/Auth/Fixtures');

                $purger = new ORMPurger;
                $purger->setPurgeMode($purger::PURGE_MODE_TRUNCATE);

                $executor = new ORMExecutor($em, $purger);
                $executor->execute($loader->getFixtures());

                $em->getConnection()->exec('SET FOREIGN_KEY_CHECKS = 1;');
            } catch (Exception $e) {
                die('Cannot create container: ' . $e->getMessage());
            }
        }

        return $instance;
    }
}
