<?php
/**
 * ImportCommandTest.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace UnitTest\Application\Console\Command;

use UnitTest\Application\ApplicationFactory;
use Application\Console\Command\ImportCommand;
use Application\Http\ApplicationInterface;
use Infrastructure\DI\Container;
use Infrastructure\Entity\EntityManager;
use Infrastructure\Exception\Runtime\InvalidArgumentException;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Tester\CommandTester;
use Exception;
use PDO;

/**
 * Class ImportCommandTest
 */
class ImportCommandTest extends AbstractCommandTestObject
{
    /**
     * @var array
     */
    private static $emailAddresses = [];

    /**
     *
     */
    public static function tearDownAfterClass(): void
    {
        $config = ApplicationFactory::getContainerStaticProxy()
                      ->offsetGet(ApplicationInterface::APP_CONFIG_MANAGER)
                      ->getDirective('database')['connection']['default'];

        $db = new PDO(
            sprintf(
                '%s:host=%s;dbname=%s',
                str_replace('pdo_', '', $config['driver']),
                $config['host'],
                $config['dbname']
            ),
            $config['user'],
            $config['password'],
            [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']
        );

        $statement = $db->prepare(
            sprintf(
                'DELETE o, u FROM oauth_user o JOIN user u ON (o.id = u.oauth_user_id) WHERE email IN ("%s")',
                implode('","', self::$emailAddresses)
            )
        );

        // can't process
        if (false === $statement->execute()) {
            self::fail(
                sprintf('Query failed: %d::%s', $statement->errorCode(), implode(PHP_EOL, $statement->errorInfo()))
            );
        }

        parent::tearDownAfterClass();
    }

    /**
     * @throws Exception
     */
    public function testRun(): void
    {
        $tmpFile = tempnam(sys_get_temp_dir(), 'uTImportCommand');
        $tmp = fopen($tmpFile, 'w');
        fputcsv(
            $tmp,
            [
                'id',
                'email',
                'alternate_email',
                'password',
                'first_name',
                'last_name'
            ]
        );

        for ($i = 10; $i < 20; $i++) {
            self::$emailAddresses[] = 'testEmail' . $i . '@unit.test';
            fputcsv(
                $tmp,
                [
                    $i,
                    'testEmail' . $i . '@unit.test',
                    null,
                    password_hash('test' . $i, PASSWORD_BCRYPT, ['cost' => 11]),
                    $i . '-First',
                    'Last-' . $i
                ]
            );
        }

        fclose($tmp);

        $this->assertEquals(
            0,
            $this->newSUT()->run(
                new StringInput($tmpFile),
                new NullOutput()
            )
        );
    }

    /**
     *
     */
    public function testRunClientIdentifierIsArrayException(): void
    {
        if (false === $this->hasSttyAvailable()) {
            $this->markTestSkipped('`stty` is required to test autocomplete functionality');
        }

        $commandTester = new CommandTester($this->newSUT());

        // user prompt for client secret
        $commandTester->setInputs(['tmpPass', 'tmpPass']);

        $exception = $this->assertThrows(
            function (CommandTester $commandTester) {
                $commandTester->execute(
                    [
                        'file'    => ['array.csv', 'file.csv']
                    ]
                );
            },
            new InvalidArgumentException,
            $commandTester
        );

        $this->assertEquals('Input file is an array', $exception->getMessage());
    }

    /**
     *
     */
    public function testRunMissingFileException(): void
    {
        $this->assertThrows(
            function (ImportCommand $command) {
                $command->run(
                    new StringInput('missing.csv'),
                    new NullOutput()
                );
            },
            new InvalidArgumentException,
            $this->newSUT()
        );
    }

    /**
     * @return ImportCommand
     */
    private function newSUT(): ImportCommand
    {
        static $container = null;

        if (null === $container) {
            $container = new Container(
                [EntityManager::class => ApplicationFactory::getContainerStaticProxy()->offsetGet(EntityManager::class)]
            );
        }

        return new ImportCommand($container);
    }
}
