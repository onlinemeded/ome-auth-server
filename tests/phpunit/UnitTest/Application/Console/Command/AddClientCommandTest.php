<?php
/**
 * AddClientCommandTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Application\Console\Command;

use UnitTest\Application\ApplicationFactory;
use Application\Console\Command\AddClientCommand;
use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;
use Domain\Auth\Infrastructure\Entity\OAuthClientRepository;
use Infrastructure\DI\Container;
use Infrastructure\Entity\EntityManager;
use Infrastructure\Exception\Runtime\InvalidArgumentException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;
use Exception;

/**
 * Class AddClientCommandTest
 */
class AddClientCommandTest extends AbstractCommandTestObject
{
    const CLIENT_NAME = 'Unit Test Client';
    const CLIENT_IDENTIFIER = 'omeUnitTest';
    const CLIENT_REDIRECT_URL = 'https://somwhere.safe';
    const ALT_CLIENT_IDENTIFIER = 'omeAltUnitTest';

    /**
     * @var Container
     */
    private static $container = null;

    /**
     *
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$container = new Container(
            [EntityManager::class => ApplicationFactory::getContainerStaticProxy()->offsetGet(EntityManager::class)]
        );
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public static function tearDownAfterClass(): void
    {
        /**
         * @var EntityManager         $em
         * @var OAuthClientRepository $repo
         * @var OAuthClientEntity     $newClientEntity
         */
        $em = self::$container->offsetGet(EntityManager::class);
        $repo = $em->getRepository(OAuthClientEntity::class);
        $newClientEntity = $repo->findOneBy(['clientIdentifier' => self::CLIENT_IDENTIFIER]);

        if (!empty($newClientEntity)) {
            $em->remove($newClientEntity);
            $em->flush($newClientEntity);
        }

        parent::tearDownAfterClass();
    }

    /**
     * @throws Exception
     */
    public function testRun(): void
    {
        if (false === $this->hasSttyAvailable()) {
            $this->markTestSkipped('`stty` is required to test autocomplete functionality');
        }

        $command = $this->newSUT();
        $commandTester = new CommandTester($command);

        // user prompt for client secret
        $commandTester->setInputs(['tmpPass', 'tmpPass']);

        $result = $commandTester->execute(
            [
                'identifier'   => self::CLIENT_IDENTIFIER,
                'redirect-url' => self::CLIENT_REDIRECT_URL,
                'name'         => self::CLIENT_NAME,
                'command'      => $command->getName()
            ]
        );

        $this->assertEquals(0, $result);

        /**
         * @var EntityManager         $em
         * @var OAuthClientRepository $repo
         * @var OAuthClientEntity     $newClientEntity
         */
        $em = self::$container->offsetGet(EntityManager::class);
        $repo = $em->getRepository(OAuthClientEntity::class);
        $newClientEntity = $repo->findOneBy(['clientIdentifier' => self::CLIENT_IDENTIFIER]);

        $this->assertNotEmpty($newClientEntity);
        $this->assertIsInt($newClientEntity->getId());
        $this->assertEquals(self::CLIENT_NAME, $newClientEntity->getName());
    }

    /**
     *
     */
    public function testRunClientIdentifierIsArrayException(): void
    {
        if (false === $this->hasSttyAvailable()) {
            $this->markTestSkipped('`stty` is required to test autocomplete functionality');
        }

        $command = $this->newSUT();
        $commandTester = new CommandTester($command);

        // user prompt for client secret
        $commandTester->setInputs(['tmpPass', 'tmpPass']);

        $exception = $this->assertThrows(
            function (CommandTester $commandTester, Command $command) {
                $commandTester->execute(
                    [
                        'identifier'   => ['client', 'identifier'],
                        'redirect-url' => self::CLIENT_REDIRECT_URL,
                        'name'         => self::CLIENT_NAME,
                        'command'      => $command->getName()
                    ]
                );
            },
            new InvalidArgumentException,
            $commandTester,
            $command
        );

        $this->assertEquals('Input clientIdentifier is an array', $exception->getMessage());
    }

    /**
     *
     */
    public function testRunExistingClientIdentifierException(): void
    {
        if (false === $this->hasSttyAvailable()) {
            $this->markTestSkipped('`stty` is required to test autocomplete functionality');
        }

        $command = $this->newSUT();
        $commandTester = new CommandTester($command);

        // user prompt for client secret
        $commandTester->setInputs(['tmpPass', 'tmpPass']);

        $exception = $this->assertThrows(
            function (CommandTester $commandTester, Command $command) {
                $commandTester->execute(
                    [
                        'identifier'   => self::CLIENT_IDENTIFIER,
                        'redirect-url' => self::CLIENT_REDIRECT_URL,
                        'name'         => self::CLIENT_NAME,
                        'command'      => $command->getName()
                    ]
                );
            },
            new InvalidArgumentException,
            $commandTester,
            $command
        );

        $this->assertEquals(
            sprintf('A client for identifier "%s" already exists', self::CLIENT_IDENTIFIER),
            $exception->getMessage()
        );
    }

    /**
     *
     */
    public function testRunMisMatchedClientSecretException(): void
    {
        if (false === $this->hasSttyAvailable()) {
            $this->markTestSkipped('`stty` is required to test autocomplete functionality');
        }

        $command = $this->newSUT();
        $commandTester = new CommandTester($command);

        // user prompt for client secret
        $commandTester->setInputs(['tmpPass', 'tmpPass2']);

        $exception = $this->assertThrows(
            function (CommandTester $commandTester, Command $command) {
                $commandTester->execute(
                    [
                        'identifier'   => self::ALT_CLIENT_IDENTIFIER,
                        'redirect-url' => self::CLIENT_REDIRECT_URL,
                        'name'         => self::CLIENT_NAME,
                        'command'      => $command->getName()
                    ]
                );
            },
            new InvalidArgumentException,
            $commandTester,
            $command
        );

        $this->assertEquals('Client secret does not match', $exception->getMessage());
    }

    /**
     * @return AddClientCommand
     */
    private function newSUT(): AddClientCommand
    {
        $command = new AddClientCommand(self::$container);
        $command->setApplication(new Application);

        return $command;
    }
}
