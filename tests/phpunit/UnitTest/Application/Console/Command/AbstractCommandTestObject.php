<?php
/**
 * AbstractCommandTestObject.php
 *
 * Copyright 2020 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Application\Console\Command;

use UnitTest\BaseTestObject;

/**
 * Class AbstractCommandTestObject
 */
class AbstractCommandTestObject extends BaseTestObject
{
    /**
     * @return bool
     */
    protected function hasSttyAvailable(): bool
    {
        exec('stty 2>&1', $output, $exitCode);
        return 0 === $exitCode;
    }
}
