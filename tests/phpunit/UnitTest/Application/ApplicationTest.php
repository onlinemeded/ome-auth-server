<?php
/**
 * ApplicationTest.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Application;

use UnitTest\Application\Http\Controller\TestController;
use UnitTest\BaseTestObject;
use Application\Http\Application;
use Application\Http\StatusCode;
use Infrastructure\Exception\RuntimeException;

/**
 * Class ApplicationTest
 */
class ApplicationTest extends BaseTestObject
{
    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     * @throws \Slim\Exception\MethodNotAllowedException
     * @throws \Slim\Exception\NotFoundException
     */
    public function testRun()
    {
        $sut = $this->newSUT();
        $_SERVER['REQUEST_URI'] = '/test';
        $_SERVER['REQUEST_METHOD'] = 'get';

        $result = $this->runAndCapture($sut);

        $this->assertJsonStringEqualsJsonString(
            json_encode(TestController::TEST_ACTION_RETURN),
            $result
        );
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     * @throws \Slim\Exception\MethodNotAllowedException
     * @throws \Slim\Exception\NotFoundException
     */
    public function testRunWithMiddleware()
    {
        $middlewareReturn = rand(1, 500);
        $sut = new Application(
            ApplicationFactory::getContainerStaticProxy(),
            [new ApplicationFakeMiddleware($middlewareReturn)]
        );

        $_SERVER['REQUEST_URI'] = '/test';
        $_SERVER['REQUEST_METHOD'] = 'get';

        $result = json_decode($this->runAndCapture($sut), true);

        $this->assertArrayHasKey('unitTest', $result);
        $this->assertEquals($middlewareReturn, $result['unitTest']);
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     * @throws \Slim\Exception\MethodNotAllowedException
     * @throws \Slim\Exception\NotFoundException
     */
    public function testRunNotFoundHandler()
    {
        $_SERVER['REQUEST_URI'] = '/nonExistentRoute';
        $_SERVER['REQUEST_METHOD'] = 'get';

        $this->assertErrorHandlerReturn(
            $this->newSUT(),
            StatusCode::HTTP_NOT_FOUND
        );
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     * @throws \Slim\Exception\MethodNotAllowedException
     * @throws \Slim\Exception\NotFoundException
     */
    public function testRunNotAllowedHandler()
    {
        $_SERVER['REQUEST_URI'] = '/test';
        $_SERVER['REQUEST_METHOD'] = 'post';

        $this->assertErrorHandlerReturn(
            $this->newSUT(),
            StatusCode::HTTP_METHOD_NOT_ALLOWED
        );
    }

    /**
     * @param Application $application
     * @param int         $httpStatusCode
     *
     * @throws \Slim\Exception\MethodNotAllowedException
     * @throws \Slim\Exception\NotFoundException
     */
    private function assertErrorHandlerReturn(
        Application $application,
        int $httpStatusCode
    ) {
        $_SERVER['SERVER_NAME'] = 'localhost';

        $httpReason = StatusCode::getReasonPhrase($httpStatusCode);
        $result = json_decode($this->runAndCapture($application), true);

        $this->assertArrayHasKey('msg', $result);
        $this->assertEquals($httpReason, $result['msg']);
        $this->assertArrayHasKey('exception', $result);
        $this->assertArrayHasKey('code', $result['exception']);
        $this->assertEquals($httpStatusCode, $result['exception']['code']);
        $this->assertArrayHasKey('msg', $result['exception']);
        $this->assertEquals(
            sprintf(
                '%s: %s http://%s%s',
                $httpReason,
                strtoupper($_SERVER['REQUEST_METHOD']),
                $_SERVER['SERVER_NAME'],
                $_SERVER['REQUEST_URI']
            ),
            $result['exception']['msg']
        );
        $this->assertArrayHasKey('type', $result['exception']);
        $this->assertEquals(
            RuntimeException::class,
            $result['exception']['type']
        );
    }

    /**
     * @param Application $application
     *
     * @return false
     * @throws \Slim\Exception\MethodNotAllowedException
     * @throws \Slim\Exception\NotFoundException
     */
    private function runAndCapture(Application $application)
    {
        ob_start();
        $application->run();
        $result = ob_get_contents();
        ob_end_clean();

        return $result;
    }

    /**
     * @return \Application\Http\Application
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    private function newSUT()
    {
        return new Application(ApplicationFactory::getContainerStaticProxy());
    }
}
