<?php
/**
 * AbstractControllerTest.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Application\Http\Controller;

use UnitTest\Application\ApplicationFactory;
use Infrastructure\Configuration\ConfigurationManager;
use Infrastructure\Entity\EntityManager;
use Infrastructure\Log\LoggingManager;
use Infrastructure\Exception\Runtime\InvalidArgumentException;
use Infrastructure\Exception\RuntimeException;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class AbstractControllerTest
 */
class AbstractControllerTest extends AbstractControllerTestObject
{
    /**
     * @var array
     */
    private $parameters = [
        'param1' => 'paramOne',
        'param2' => 'paramTwo',
        'param3' => 'paramThree',
        'param4' => 'paramFour'
    ];

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     * @throws \Infrastructure\Exception\RuntimeException
     */
    public function testDispatch()
    {
        /**
         * @var Request  $request
         * @var Response $response
         */
        list($request, $response) = $this->createRequestAndResponse();
        $sut = $this->newSUT();
        $result = $sut->dispatch($request, $response, []);

        $this->assertInstanceOf(Response::class, $result);
        $this->assertEquals(
            StatusCode::HTTP_OK,
            $result->getStatusCode()
        );
        $this->assertJsonStringEqualsJsonString(
            json_encode(TestController::TEST_ACTION_RETURN),
            $result->getBody()->__toString()
        );
    }

    /**
     *
     */
    public function testDispatchNoActionException()
    {
        $this->assertThrows(
            function () {
                list($request, $response) = $this->createRequestAndResponse();
                (new TestController(
                    ApplicationFactory::getContainerStaticProxy(),
                    'nonExistentAction'
                ))->dispatch(
                    $request,
                    $response,
                    []
                );
            },
            new InvalidArgumentException
        );
    }

    /**
     *
     */
    public function testDispatchEncodingRuntimeException()
    {
        $this->assertThrows(
            function () {
                list($request, $response) = $this->createRequestAndResponse();
                (new TestController(
                    ApplicationFactory::getContainerStaticProxy(),
                    'malformedJSON'
                ))->dispatch(
                    $request,
                    $response,
                    []
                );
            },
            new RuntimeException
        );
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testGetAction()
    {
        foreach (['action1', 'action2', 'action3', 'action4'] as $action) {
            $sut = $this->newSUT($action);

            $this->assertEquals($action, $sut->getActionForSUT());
        }
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testGetHttpResponseCode()
    {
        $sut = $this->newSUT();

        $this->assertEquals(
            StatusCode::HTTP_OK,
            $sut->getHttpResponseCodeForSUT()
        );
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testSetHttpResponseCode()
    {
        $sut = $this->newSUT();

        foreach (range(200, 220) as $responseCode) {
            $sut->setHttpResponseCodeForSUT($responseCode);

            $this->assertEquals(
                $responseCode,
                $sut->getHttpResponseCodeForSUT()
            );
        }
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     * @throws \Infrastructure\Exception\RuntimeException
     */
    public function testGetParam()
    {
        /**
         * @var Request  $request
         * @var Response $response
         */
        list($request, $response) = $this->createRequestAndResponse();
        $sut = $this->newSUT();
        $sut->dispatch($request, $response, $this->parameters);

        foreach ($this->parameters as $parameter => $value) {
            $this->assertEquals(
                $value,
                $sut->getParamForSUT($parameter)
            );
        }
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testGetMissingParam()
    {
        $sut = $this->newSUT();

        foreach ($this->getPrimitiveTypesArray() as $defaultReturn) {
            $this->assertEquals(
                $defaultReturn,
                $sut->getParamForSUT('nonExistent', $defaultReturn)
            );
        }
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testSetParams()
    {
        $sut = $this->newSUT();

        $sut->setParamsForSUT($this->parameters);

        foreach ($this->parameters as $parameter => $value) {
            $this->assertEquals(
                $value,
                $sut->getParamForSUT($parameter)
            );
        }
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testGetParams()
    {
        $sut = $this->newSUT();
        $sut->setParamsForSUT($this->parameters);

        $this->assertEquals($this->parameters, $sut->getParamsForSUT());

        foreach ($this->parameters as $parameter => $value) {
            $this->assertEquals(
                $value,
                $sut->getParamForSUT($parameter)
            );
        }
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testGetFromServiceContainer()
    {
        $sut = $this->newSUT();
        $containers = [
            EntityManager::class,
            ConfigurationManager::class,
            LoggingManager::class
        ];

        foreach ($containers as $container) {
            $this->assertInstanceOf($container, $sut->getFromServiceContainerForSUT($container));
        }
    }

    /**
     * @param string $action
     *
     * @return \UnitTest\Application\Http\Controller\TestController
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    private function newSUT(string $action = 'test'): TestController
    {
        return $this->createController(TestController::class, $action);
    }
}
