<?php
/**
 * AbstractControllerTestObject.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Application\Http\Controller;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use Application\Http\Controller\AbstractController;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;
use ReflectionClass;
use ReflectionException;

/**
 * Class AbstractControllerTestObject
 */
class AbstractControllerTestObject extends BaseTestObject
{
    /**
     * @param AbstractController $controller
     * @param array              $params
     *
     * @return void
     * @throws ReflectionException
     */
    protected function setParams(AbstractController $controller, array $params): void
    {
        $reflection = new ReflectionClass($controller);
        $setParams = $reflection->getMethod('setParams');
        $setParams->setAccessible(true);
        $setParams->invoke($controller, $params);
    }

    /**
     * @param string $className
     * @param string $action
     *
     * @return mixed
     */
    protected function createController(string $className, string $action)
    {
        return new $className(ApplicationFactory::getContainerStaticProxy(), $action);
    }

    /**
     * @return array
     */
    protected function createRequestAndResponse(): array
    {
        return [Request::createFromEnvironment(Environment::mock()), new Response];
    }
}
