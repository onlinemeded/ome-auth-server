<?php
/**
 * ErrorControllerTest.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Application\Http\Controller;

use UnitTest\Application\ApplicationFactory;
use Application\Http\ApplicationInterface;
use Application\Http\Controller\ErrorController;
use Application\Http\StatusCode;
use Infrastructure\Configuration\ConfigurationManager;
use Infrastructure\Exception\Runtime\InvalidArgumentException;
use Infrastructure\Exception\RuntimeException;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ErrorControllerTest
 */
class ErrorControllerTest extends AbstractControllerTestObject
{
    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testDispatchNoActionException()
    {
        list($request, $response) = $this->createRequestAndResponse();
        $this->assertThrows(
            function (
                ErrorController $controller,
                Request $request,
                Response $response
            ) {
                $controller->dispatch($request, $response, []);
            },
            new InvalidArgumentException(),
            $this->newSUT('nonExistentAction'),
            $request,
            $response
        );
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testErrorHandler()
    {
        $this->assertHandlerReturn('error', 500, new RuntimeException('Testing error handler', 500));
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testPhpErrorHandler()
    {
        $this->assertHandlerReturn('phpError', 500, new RuntimeException('Testing Php error handler', 500));
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testNotFoundHandler()
    {
        $this->assertHandlerReturn('notFound', 404, null);
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testNotAllowedHandler()
    {
        $this->assertHandlerReturn('notAllowed', 405, ['PUT']);
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testProductionErrorReturn()
    {
        $action = 'error';
        $sut = $this->newSUT($action);
        list($request,) = $this->createRequestAndResponse();
        $config = ApplicationFactory::getContainerStaticProxy()->offsetGet(ConfigurationManager::class);
        $originalConfig = $config->getDirective('env');

        $config->addConfigurationFromArray(['env' => ApplicationInterface::MODE_PRODUCTION]);

        // Set exception to null to execute the 404 handler
        $sut->setParamsForSUT(['exception' => null]);

        $result = $sut->{$action}($request);

        $config->addConfigurationFromArray(['env' => $originalConfig]);

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('msg', $result);
        $this->assertInternalType('string', $result['msg']);
        $this->assertEquals(StatusCode::getReasonPhrase(StatusCode::HTTP_NOT_FOUND), $result['msg']);
    }

    /**
     * @param string $action
     * @param int    $httpStatusCode
     * @param        $exception
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    private function assertHandlerReturn(
        string $action,
        int $httpStatusCode,
        $exception
    ) {
        $sut = $this->newSUT($action);
        list($request,) = $this->createRequestAndResponse();
        $sut->setParamsForSUT(['exception' => $exception]);

        $result = $sut->{$action}($request);

        $this->assertArrayHasKey('msg', $result);
        $this->assertEquals(StatusCode::getReasonPhrase($httpStatusCode), $result['msg']);
        $this->assertArrayHasKey('exception', $result);
        $this->assertArrayHasKey('code', $result['exception']);
        $this->assertEquals($httpStatusCode, $result['exception']['code']);
        $this->assertArrayHasKey('msg', $result['exception']);
        $this->assertArrayHasKey('type', $result['exception']);
        $this->assertEquals(RuntimeException::class, $result['exception']['type']);
    }

    /**
     * @param string $action
     *
     * @return \UnitTest\Application\Http\Controller\TestErrorController
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    private function newSUT(string $action = 'test'): TestErrorController
    {
        return $this->createController(TestErrorController::class, $action);
    }
}
