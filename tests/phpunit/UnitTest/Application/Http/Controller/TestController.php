<?php
/**
 * TestController.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Application\Http\Controller;

use Application\Http\Controller\AbstractController;

/**
 * Class TestController
 */
class TestController extends AbstractController
{
    const TEST_ACTION_RETURN = [
        'primitives' => ['string', 1, -1, 1.1, 10000.00]
    ];

    /**
     * @return array
     */
    public function test()
    {
        return self::TEST_ACTION_RETURN;
    }

    /**
     *
     */
    public function action1()
    {
    }

    /**
     *
     */
    public function action2()
    {
    }

    /**
     *
     */
    public function action3()
    {
    }

    /**
     *
     */
    public function action4()
    {
    }

    /**
     * @return string
     */
    public function malformedJSON()
    {
        return "\x92";
    }

    /**
     * @return string
     */
    public function getActionForSUT()
    {
        return $this->getAction();
    }

    /**
     * @param int $code
     */
    public function setHttpResponseCodeForSUT(int $code)
    {
        return $this->setHttpResponseCode($code);
    }

    /**
     * @return int
     */
    public function getHttpResponseCodeForSUT()
    {
        return $this->getHttpResponseCode();
    }

    /**
     * @param array $params
     */
    public function setParamsForSUT(array $params)
    {
        return $this->setParams($params);
    }

    /**
     * @return array
     */
    public function getParamsForSUT()
    {
        return $this->getParams();
    }

    /**
     * @param string $name
     * @param null   $default
     *
     * @return mixed
     */
    public function getParamForSUT(string $name, $default = null)
    {
        return $this->getParam($name, $default);
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function getFromServiceContainerForSUT(string $name)
    {
        return $this->getFromServiceContainer($name);
    }
}
