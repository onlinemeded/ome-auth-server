<?php
/**
 * TestErrorController.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Application\Http\Controller;

use Application\Http\Controller\ErrorController;

/**
 * Class TestErrorController
 */
class TestErrorController extends ErrorController
{
    /**
     * @param array $params
     */
    public function setParamsForSUT(array $params)
    {
        return $this->setParams($params);
    }
}
