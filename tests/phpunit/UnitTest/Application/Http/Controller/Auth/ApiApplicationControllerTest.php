<?php
/**
 * ApiApplicationControllerTest.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace UnitTest\Application\Http\Controller\Auth;

use Application\Http\Controller\Auth\ApiApplicationController;
use UnitTest\Application\Http\Controller\AbstractControllerTest;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;

/**
 * Class ApiApplicationControllerTest
 */
class ApiApplicationControllerTest extends AbstractControllerTest
{
    /**
     *
     */
    public function testListAll(): void
    {
        $result = $this->newSUT('listAll')->listAll();

        $this->assertIsArray($result);
        $this->assertArrayHasKey(0, $result);
        $this->assertIsArray($result[0]);
        $this->assertEquals(OAuthDTOInterface::APP_NAME_VALID, $result[0]['name']);
        $this->assertEquals(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID, $result[0]['client_identifier']);
    }

    /**
     * @param string $action
     *
     * @return ApiApplicationController
     */
    private function newSUT(string $action = 'test'): ApiApplicationController
    {
        return $this->createController(ApiApplicationController::class, $action);
    }
}
