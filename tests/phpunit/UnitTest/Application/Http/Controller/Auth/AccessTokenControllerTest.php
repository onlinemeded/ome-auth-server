<?php
/**
 * AccessTokenControllerTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Application\Http\Controller\Auth;

use UnitTest\Application\Http\Controller\AbstractControllerTestObject;
use UnitTest\Application\Http\Request\RequestService;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Application\Http\Controller\Auth\AccessTokenController;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;

/**
 * Class AccessTokenControllerTest
 */
class AccessTokenControllerTest extends AbstractControllerTestObject
{
    /**
     *
     */
    public function testPostClientCredentials(): void
    {
        $result = $this->newSUT('post')
            ->post(
                RequestService::createSlimRequest(
                    'POST',
                    [
                        'HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded',
                        'PHP_AUTH_USER'     => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                        'PHP_AUTH_PW'       => OAuthDTOInterface::CLIENT_SECRET_VALID
                    ],
                    ['grant_type' => 'client_credentials']
                )
            );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('access_token', $result);
        $this->assertIsString($result['access_token']);
        $this->assertEquals(40, strlen($result['access_token']));
        $this->assertArrayHasKey('expires_in', $result);
        $this->assertIsInt($result['expires_in']);
        $this->assertArrayHasKey('token_type', $result);
        $this->assertIsString($result['token_type']);
        $this->assertArrayHasKey('scope', $result);
    }

    /**
     *
     */
    public function testPostUserCredentials(): void
    {
        $result = $this->newSUT('post')
            ->post(
                RequestService::createSlimRequest(
                    'POST',
                    [
                        'HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded',
                        'PHP_AUTH_USER'     => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                        'PHP_AUTH_PW'       => OAuthDTOInterface::CLIENT_SECRET_VALID
                    ],
                    [
                        'grant_type' => 'password',
                        'username'   => OAuthDTOInterface::USER_EMAIL_VALID,
                        'password'   => OAuthDTOInterface::USER_PASSWORD_VALID
                    ]
                )
            );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('access_token', $result);
        $this->assertIsString($result['access_token']);
        $this->assertEquals(40, strlen($result['access_token']));
        $this->assertArrayHasKey('expires_in', $result);
        $this->assertIsInt($result['expires_in']);
        $this->assertArrayHasKey('token_type', $result);
        $this->assertIsString($result['token_type']);
        $this->assertArrayHasKey('scope', $result);
        $this->assertArrayHasKey('refresh_token', $result);
        $this->assertIsString($result['refresh_token']);
    }

    /**
     *
     */
    public function testPostAuthenticationInvalidCredentialsException(): void
    {
        $exception = $this->assertThrows(
            function (AccessTokenController $controller) {
                $controller->post(
                    RequestService::createSlimRequest(
                        'POST',
                        [
                            'HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded',
                            'PHP_AUTH_USER'     => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                            'PHP_AUTH_PW'       => 'invalidSecret'
                        ],
                        ['grant_type' => 'client_credentials']
                    )
                );
            },
            new AuthenticationException,
            $this->newSUT('post')
        );

        $this->assertEquals('The client credentials are invalid', $exception->getMessage());
    }

    /**
     * @param string $action
     *
     * @return AccessTokenController
     */
    private function newSUT(string $action = 'test'): AccessTokenController
    {
        return $this->createController(AccessTokenController::class, $action);
    }
}
