<?php
/**
 * AuthorizationCodeControllerTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Application\Http\Controller\Auth;

use UnitTest\Application\Http\Controller\AbstractControllerTestObject;
use UnitTest\Application\Http\Request\RequestService;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Application\Http\Controller\Auth\AuthorizationCodeController;

/**
 * Class AuthorizationCodeControllerTest
 */
class AuthorizationCodeControllerTest extends AbstractControllerTestObject
{
    /**
     *
     */
    public function testGet(): void
    {
        $result = $this->newSUT('get')
            ->get(
                RequestService::createSlimRequest(
                    'GET',
                    [
                        'QUERY_STRING' => sprintf(
                            'client=%s&redirect=%s&state=%s',
                            OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                            OAuthDTOInterface::CLIENT_REDIRECT_URI_VALID,
                            sha1(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID . 'someSalt')
                        )
                    ]
                )
            );

        $this->assertIsString($result);
        $this->assertTrue(false !== strpos($result, OAuthDTOInterface::CLIENT_REDIRECT_URI_VALID));
    }

    /**
     * @param string $action
     *
     * @return AuthorizationCodeController
     */
    private function newSUT(string $action = 'test'): AuthorizationCodeController
    {
        return $this->createController(AuthorizationCodeController::class, $action);
    }
}
