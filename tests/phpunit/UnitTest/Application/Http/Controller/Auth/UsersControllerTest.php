<?php
/**
 * UsersControllerTest.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace UnitTest\Application\Http\Controller\Auth;

use UnitTest\Application\Http\Controller\AbstractControllerTest;
use UnitTest\Application\Http\Request\RequestService;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Application\Http\Controller\Auth\UsersController;

/**
 * Class UsersControllerTest
 */
class UsersControllerTest extends AbstractControllerTest
{
    /**
     *
     */
    public function testListAll(): void
    {
        $result = $this->newSUT('listAll')->listAll(
            RequestService::createSlimRequest('GET')->withAttribute(
                'client_identifier',
                OAuthDTOInterface::CLIENT_IDENTIFIER_VALID
            )
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey(0, $result);
        $this->assertIsArray($result[0]);
        $this->assertEquals(
            OAuthDTOInterface::USER_FIRST_NAME_VALID . ' ' . OAuthDTOInterface::USER_LAST_NAME_VALID,
            $result[0]['name']
        );
        $this->assertEquals(OAuthDTOInterface::USER_EMAIL_VALID, $result[0]['email']);
        $this->assertEquals(
            OAuthDTOInterface::USER_FIRST_NAME_VALID . ' ' . OAuthDTOInterface::USER_LAST_NAME_VALID,
            $result[0]['name']
        );
        $this->assertIsString($result[0]['title']);
    }

    /**
     *
     */
    public function testGet(): void
    {
        $result = $this->newSUT('get')->get(
            RequestService::createSlimRequest('GET')
                ->withAttribute('identifier', OAuthDTOInterface::USER_EMAIL_VALID)
                ->withAttribute('client_identifier', OAuthDTOInterface::CLIENT_IDENTIFIER_VALID)
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('id', $result);
        $this->assertArrayHasKey('email', $result);
        $this->assertArrayHasKey('name', $result);
        $this->assertArrayHasKey('user_name', $result);
        $this->assertArrayHasKey('title', $result);
        $this->assertEquals(
            [
                'id'        => OAuthDTOInterface::USER_ID_VALID,
                'email'     => OAuthDTOInterface::USER_EMAIL_VALID,
                'name'      => OAuthDTOInterface::USER_FIRST_NAME_VALID . ' ' . OAuthDTOInterface::USER_LAST_NAME_VALID,
                'user_name' => substr(
                    OAuthDTOInterface::USER_EMAIL_VALID,
                    0,
                    strpos(OAuthDTOInterface::USER_EMAIL_VALID, '@')
                ),
                'title' => 'Title'
            ],
            $result
        );
    }

    /**
     * @param string $action
     *
     * @return UsersController
     */
    private function newSUT(string $action = 'test'): UsersController
    {
        return $this->createController(UsersController::class, $action);
    }
}
