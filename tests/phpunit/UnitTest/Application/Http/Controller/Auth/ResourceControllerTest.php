<?php
/**
 * ResourceControllerTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Application\Http\Controller\Auth;

use UnitTest\Application\Http\Controller\AbstractControllerTestObject;
use UnitTest\Application\Http\Request\RequestService;
use UnitTest\Domain\Auth\Fixtures\OAuthAccessTokenFixtureLoader;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Application\Http\Controller\Auth\AccessTokenController;
use Application\Http\Controller\Auth\ResourceController;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;

/**
 * Class ResourceControllerTest
 */
class ResourceControllerTest extends AbstractControllerTestObject
{
    /**
     *
     */
    public function testPost(): void
    {
        /** @var AccessTokenController $tokenController */
        $tokenController = $this->createController(AccessTokenController::class, 'post');
        $accessToken = $tokenController->post(
            RequestService::createSlimRequest(
                'POST',
                [
                    'HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded',
                    'PHP_AUTH_USER'     => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                    'PHP_AUTH_PW'       => OAuthDTOInterface::CLIENT_SECRET_VALID
                ],
                ['grant_type' => 'client_credentials']
            )
        )['access_token'];

        $result = $this->newSUT('post')->post(
            RequestService::createSlimRequest(
                'POST',
                ['HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded'],
                ['access_token' => $accessToken]
            )
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('success', $result);
        $this->assertTrue($result['success']);
    }

    /**
     *
     */
    public function testPostInvalidTokenError(): void
    {
        $exception = $this->assertThrows(
            function (ResourceController $controller) {
                $controller->post(
                    RequestService::createSlimRequest(
                        'POST',
                        ['HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded'],
                        ['access_token' => 'invalidToken']
                    )
                );
            },
            new AuthenticationException,
            $this->newSUT('post')
        );

        $this->assertEquals('The access token provided is invalid', $exception->getMessage());
    }

    /**
     *
     */
    public function testPostExpiredTokenError(): void
    {
        $exception = $this->assertThrows(
            function (ResourceController $controller) {
                $controller->post(
                    RequestService::createSlimRequest(
                        'POST',
                        ['HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded'],
                        ['access_token' => OAuthAccessTokenFixtureLoader::getExpiredAccessToken()->getToken()]
                    )
                );
            },
            new AuthenticationException,
            $this->newSUT('post')
        );

        $this->assertEquals('The access token provided has expired', $exception->getMessage());
    }

    /**
     * @param string $action
     *
     * @return ResourceController
     */
    private function newSUT(string $action = 'test'): ResourceController
    {
        return $this->createController(ResourceController::class, $action);
    }
}
