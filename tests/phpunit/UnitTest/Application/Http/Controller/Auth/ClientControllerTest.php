<?php
/**
 * ClientControllerTest.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace UnitTest\Application\Http\Controller\Auth;

use UnitTest\Application\Http\Controller\AbstractControllerTest;
use UnitTest\Application\Http\Request\RequestService;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Application\Http\Controller\Auth\ClientController;

/**
 * Class ClientControllerTest
 */
class ClientControllerTest extends AbstractControllerTest
{
    /**
     *
     */
    public function testGet(): void
    {
        $result = $this->newSUT('get')
            ->get(
                RequestService::createSlimRequest(
                    'GET',
                    [
                        'QUERY_STRING' => 'identifier=' . OAuthDTOInterface::CLIENT_IDENTIFIER_VALID
                    ]
                )->withAttribute('identifier', OAuthDTOInterface::CLIENT_IDENTIFIER_VALID)
            );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('clientIdentifier', $result);
        $this->assertEquals(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID, $result['clientIdentifier']);
        $this->assertEquals(OAuthDTOInterface::CLIENT_REDIRECT_URI_VALID, $result['redirectUrl']);
    }

    /**
     *
     */
    public function testListAll(): void
    {
        $result = $this->newSUT('listAll')->listAll();

        $this->assertIsArray($result);
        $this->assertArrayHasKey(0, $result);
        $this->assertIsArray($result[0]);
        $this->assertEquals(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID, $result[0]['identifier']);
    }

    /**
     * @param string $action
     *
     * @return ClientController
     */
    private function newSUT(string $action = 'test'): ClientController
    {
        return $this->createController(ClientController::class, $action);
    }
}
