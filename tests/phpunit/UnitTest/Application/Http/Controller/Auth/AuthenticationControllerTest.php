<?php
/**
 * AuthenticationControllerTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Application\Http\Controller\Auth;

use UnitTest\Application\Http\Request\RequestService;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use UnitTest\Application\Http\Controller\AbstractControllerTest;
use Application\Http\Controller\Auth\AuthenticationController;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;

/**
 * Class AuthenticationControllerTest
 */
class AuthenticationControllerTest extends AbstractControllerTest
{
    /**
     *
     */
    public function testPost(): void
    {
        $result = $this->newSUT('post')
            ->post(
                RequestService::createSlimRequest(
                    'POST',
                    [
                        'HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded'
                    ],
                    [
                        'username' => OAuthDTOInterface::USER_EMAIL_VALID,
                        'password' => OAuthDTOInterface::USER_PASSWORD_VALID
                    ]
                )
            );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('success', $result);
        $this->assertTrue($result['success']);
    }

    /**
     *
     */
    public function testPostInvalidCredentialException(): void
    {
        $this->assertThrows(
            function (AuthenticationController $controller) {
                $controller->post(
                    RequestService::createSlimRequest(
                        'POST',
                        [
                            'HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded'
                        ],
                        [
                            'username' => OAuthDTOInterface::USER_EMAIL_INVALID,
                            'password' => ''
                        ]
                    )
                );
            },
            new AuthenticationException,
            $this->newSUT('post')
        );
    }

    /**
     * @param string $action
     *
     * @return AuthenticationController
     */
    private function newSUT(string $action = 'test'): AuthenticationController
    {
        return $this->createController(AuthenticationController::class, $action);
    }
}
