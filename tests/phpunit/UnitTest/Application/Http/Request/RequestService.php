<?php
/**
 * RequestService.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Application\Http\Request;

use Slim\Http\Environment;
use Slim\Http\Request;

/**
 * Class RequestService
 */
class RequestService
{
    /**
     * @param string $method
     * @param array  $data
     * @param array  $parameters
     *
     * @return Request
     */
    public static function createSlimRequest(string $method = 'GET', array $data = [], array $parameters = []): Request
    {
        // capture state
        $original = [
            'REQUEST_METHOD' => $_SERVER['REQUEST_METHOD']
        ];

        $_SERVER['REQUEST_METHOD'] = $data['REQUEST_METHOD'] = $method;

        (true === in_array(strtoupper($method), ['POST', 'PUT', 'DELETE']))
            ? $_POST = $parameters
            : $_REQUEST = $parameters;

        $request = Request::createFromEnvironment(Environment::mock($data));

        // revert state
        $_SERVER['REQUEST_METHOD'] = $original['REQUEST_METHOD'];

        return $request;
    }
}
