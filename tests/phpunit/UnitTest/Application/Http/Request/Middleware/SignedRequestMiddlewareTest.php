<?php
/**
 * SignedRequestMiddlewareTest.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Application\Http\Request\Middleware;

use UnitTest\BaseTestObject;
use Application\Http\Request\Middleware\SignedRequestMiddleware;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;
use Infrastructure\Crypto\Cryptographer\CryptographerInterface;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;
use Exception;

/**
 * Class SignedRequestMiddlewareTest
 */
class SignedRequestMiddlewareTest extends BaseTestObject
{
    const HEADER_NAME = 'OME-SIG';

    /**
     * @throws AuthenticationException
     * @throws Exception
     */
    public function testInvoke()
    {
        /** @var Environment $environment */
        list ($apiKey, $environment) = $this->prepareServerAndApiKey();
        $environment->set(
            'HTTP_' . self::HEADER_NAME,
            sprintf(
                '%s;%s',
                $apiKey,
                $this->hashRequest(
                    $apiKey,
                    $_SERVER['HTTP_HOST'],
                    $_SERVER['REQUEST_URI'],
                    $_SERVER['HTTP_OME_DATE']
                )
            )
        );

        $callable = function (Request $request, Response $response) {
            return $response;
        };
        $middleware = new SignedRequestMiddleware(['headerName' => self::HEADER_NAME]);
        $request = Request::createFromEnvironment($environment);
        $response = new Response;
        $result = $middleware->__invoke($request, $response, $callable);

        $this->assertInstanceOf(Response::class, $result);
        $this->assertSame($response, $result);
    }

    /**
     * @throws Exception
     */
    public function testInvokeException()
    {
        /** @var Environment $environment */
        list ($apiKey, $environment) = $this->prepareServerAndApiKey();
        $environment->set('HTTP_' . self::HEADER_NAME, sprintf('%s;%s', $apiKey, 'InvalidHash'));

        $this->assertThrows(
            function (SignedRequestMiddleware $middleware, Request $request, Response $response, callable $callable) {
                $middleware->__invoke($request, $response, $callable);
            },
            new AuthenticationException,
            new SignedRequestMiddleware(['headerName' => self::HEADER_NAME]),
            Request::createFromEnvironment($environment),
            new Response,
            function (Request $request, Response $response) {
                return $response;
            }
        );
    }

    /**
     * @throws Exception
     */
    public function testInvokeInvalidDateHeaderException()
    {
        /** @var Environment $environment */
        list ($apiKey, $environment) = $this->prepareServerAndApiKey(['HTTP_OME_DATE' => '']);
        $environment->set(
            'HTTP_' . self::HEADER_NAME,
            sprintf(
                '%s;%s',
                $apiKey,
                $this->hashRequest(
                    $apiKey,
                    $_SERVER['HTTP_HOST'],
                    $_SERVER['REQUEST_URI'],
                    $_SERVER['HTTP_OME_DATE']
                )
            )
        );

        $this->assertThrows(
            function (SignedRequestMiddleware $middleware, Request $request, Response $response, callable $callable) {
                $middleware->__invoke($request, $response, $callable);
            },
            new AuthenticationException,
            new SignedRequestMiddleware(['headerName' => self::HEADER_NAME]),
            Request::createFromEnvironment($environment),
            new Response,
            function (Request $request, Response $response) {
                return $response;
            }
        );
    }

    /**
     * @throws Exception
     */
    public function testInvokeInvalidTimeToLiveException()
    {
        $time = time();

        foreach ([$time - 31, $time + 31] as $interval) {
            /** @var Environment $environment */
            list ($apiKey, $environment) = $this->prepareServerAndApiKey(
                ['HTTP_OME_DATE' => gmdate('D, d M Y H:i:s', $interval)]
            );

            $environment->set(
                'HTTP_' . self::HEADER_NAME,
                sprintf(
                    '%s;%s',
                    $apiKey,
                    $this->hashRequest(
                        $apiKey,
                        $_SERVER['HTTP_HOST'],
                        $_SERVER['REQUEST_URI'],
                        $_SERVER['HTTP_OME_DATE']
                    )
                )
            );

            $this->assertThrows(
                function (
                    SignedRequestMiddleware $middleware,
                    Request $request,
                    Response $response,
                    callable $callable
                ) {
                    $middleware->__invoke($request, $response, $callable);
                },
                new AuthenticationException,
                new SignedRequestMiddleware(['headerName' => self::HEADER_NAME]),
                Request::createFromEnvironment($environment),
                new Response,
                function (Request $request, Response $response) {
                    return $response;
                }
            );
        }
    }

    /**
     * @throws Exception
     */
    public function testInvokeInvalidSignatureException()
    {
        /** @var Environment $environment */
        list ($apiKey, $environment) = $this->prepareServerAndApiKey();
        $environment->set(
            'HTTP_' . self::HEADER_NAME,
            $this->hashRequest(
                $apiKey,
                $_SERVER['HTTP_HOST'],
                $_SERVER['REQUEST_URI'],
                $_SERVER['HTTP_OME_DATE']
            )
        );

        $this->assertThrows(
            function (SignedRequestMiddleware $middleware, Request $request, Response $response, callable $callable) {
                $middleware->__invoke($request, $response, $callable);
            },
            new AuthenticationException,
            new SignedRequestMiddleware(['headerName' => self::HEADER_NAME]),
            Request::createFromEnvironment($environment),
            new Response,
            function (Request $request, Response $response) {
                return $response;
            }
        );
    }

    /**
     * @throws Exception
     */
    public function testInvokeBeforeValidationCallable()
    {
        /** @var Environment $environment */
        list ($apiKey, $environment) = $this->prepareServerAndApiKey();
        $environment->set(
            'HTTP_' . self::HEADER_NAME,
            sprintf(
                '%s;%s',
                $apiKey,
                $this->hashRequest(
                    $apiKey,
                    $_SERVER['HTTP_HOST'],
                    $_SERVER['REQUEST_URI'],
                    $_SERVER['HTTP_OME_DATE']
                )
            )
        );

        $callable = function (Request $request, Response $response) {
            return $response;
        };

        $middleware = new SignedRequestMiddleware(
            [
                'headerName'       => self::HEADER_NAME,
                'beforeValidation' => function ($key) use ($apiKey) {
                    $this->assertEquals($apiKey, $key);
                }
            ]
        );
        $request = Request::createFromEnvironment($environment);
        $response = new Response;
        $result = $middleware->__invoke($request, $response, $callable);

        $this->assertInstanceOf(Response::class, $result);
        $this->assertSame($response, $result);
    }

    /**
     * @throws AuthenticationException
     * @throws Exception
     */
    public function testOptionsMethodBypass()
    {
        /** @var Environment $environment */
        list ($apiKey, $environment) = $this->prepareServerAndApiKey();
        $environment->set('REQUEST_METHOD', 'OPTIONS');
        $callable = function (Request $request, Response $response) {
            return $response;
        };
        $middleware = new SignedRequestMiddleware(['headerName' => self::HEADER_NAME]);
        $request = Request::createFromEnvironment($environment);
        $response = new Response;
        $result = $middleware->__invoke($request, $response, $callable);

        $this->assertInstanceOf(Response::class, $result);
        $this->assertSame($response, $result);
    }

    /**
     * @param array $overrides
     *
     * @return array
     * @throws Exception
     */
    private function prepareServerAndApiKey(array $overrides = []): array
    {
        $_SERVER = array_merge(
            $_SERVER,
            [
                'HTTP_HOST'      => bin2hex(random_bytes(12)),
                'HTTP_OME_DATE'  => gmdate('D, d M Y H:i:s', time()),
                'REQUEST_URI'    => '/test',
                'REQUEST_METHOD' => 'get'
            ],
            $overrides
        );

        return [bin2hex(random_bytes(32)), new Environment($_SERVER)];
    }

    /**
     * @param string $apiKey
     * @param string $host
     * @param string $uri
     * @param string $time
     *
     * @return string
     */
    private function hashRequest(
        string $apiKey,
        string $host,
        string $uri,
        string $time
    ): string {
        return hash_hmac(
            CryptographerInterface::HMAC_ALGO_SHA_256,
            sprintf(
                '%s:%s:%s GMT',
                $host,
                $uri,
                $time
            ),
            $apiKey
        );
    }
}
