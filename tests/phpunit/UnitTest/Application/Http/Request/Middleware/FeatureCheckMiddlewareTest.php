<?php
/**
 * FeatureCheckMiddlewareTest.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Application\Http\Request\Middleware;

use UnitTest\BaseTestObject;
use Application\Http\Request\Middleware\FeatureCheckMiddleware;
use Infrastructure\Feature\FeatureManager;
use Infrastructure\InfrastructureException;
use Opensoft\Rollout\Storage\ArrayStorage;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Route;

/**
 * Class FeatureCheckMiddlewareTest
 */
class FeatureCheckMiddlewareTest extends BaseTestObject
{
    /**
     * @var FeatureManager
     */
    private static $featureManager;

    /**
     * @var array
     */
    private static $routeDetails = [
        'controller' => 'UnitTest\\Application\\Infrastructure\\Controller\\TestController',
        'method'     => 'get',
        'action'     => 'test',
        'feature'    => 'test'
    ];

    /**
     *
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        $featureManager = FeatureManager::create(new ArrayStorage);
        $featureManager->defineGroup(
            'all',
            function () {
                return true;
            }
        );

        foreach ([
                     'test' => [
                         'enabled' => true
                     ]
                 ] as $feature => $settings) {
            if (true === $settings['enabled']) {
                $featureManager->activate($feature);
                continue;
            }

            if (true === is_array($settings['enabled'])) {
                foreach (['group', 'user'] as $permissionLevel) {
                    if (true === array_key_exists(
                        $permissionLevel,
                        $settings['enabled']
                    )) {
                        foreach ($settings['enabled'][$permissionLevel] as $level) {
                            $featureManager->{
                            'activate' . ucfirst($permissionLevel)
                            }(
                                $feature,
                                $level
                            );
                        }
                    }
                }
            }
        }

        self::$featureManager = $featureManager;
    }

    /**
     * @throws \Infrastructure\InfrastructureException
     * @throws \ReflectionException
     */
    public function testInvoke()
    {
        $routeDetails = self::$routeDetails;
        $callable = function (
            Request $request,
            Response $response
        ) use ($routeDetails) {
            return $response;
        };
        $middleware = new FeatureCheckMiddleware(self::$featureManager);
        $request = Request::createFromEnvironment(
            Environment::mock(
                [
                    'REQUEST_URI'    => '/test',
                    'REQUEST_METHOD' => 'get'
                ]
            )
        );
        $response = new Response;

        $result = $middleware->__invoke(
            $request->withAttributes(
                [
                    'route' => new Route('get', '/test', $callable)
                ]
            ),
            $response,
            $callable
        );

        $this->assertInstanceOf(Response::class, $result);
        $this->assertSame($response, $result);
    }

    /**
     *
     */
    public function testInvokeException()
    {
        $this->assertThrows(
            function (FeatureManager $featureManager, array $routeDetails) {
                $callable = function (
                    Request $request,
                    Response $response
                ) use ($routeDetails) {
                    return $response;
                };
                $middleware = new FeatureCheckMiddleware($featureManager);
                $request = Request::createFromEnvironment(
                    Environment::mock(
                        [
                            'REQUEST_URI'    => '/test',
                            'REQUEST_METHOD' => 'get'
                        ]
                    )
                );
                $response = new Response;

                $middleware->__invoke(
                    $request->withAttributes(
                        [
                            'route' => new Route('get', '/test', $callable)
                        ]
                    ),
                    $response,
                    $callable
                );
            },
            new InfrastructureException,
            self::$featureManager,
            array_merge(self::$routeDetails, ['feature' => 'missingFeature'])
        );
    }
}
