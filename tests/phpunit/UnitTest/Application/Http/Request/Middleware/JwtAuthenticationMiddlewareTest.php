<?php
/**
 * JwtAuthenticationMiddlewareTest.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Application\Http\Request\Middleware;

use UnitTest\BaseTestObject;
use Application\Http\Request\Middleware\JwtAuthenticationMiddleware;
use Application\Http\StatusCode;
use Firebase\JWT\JWT;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class JwtAuthenticationMiddlewareTest
 */
class JwtAuthenticationMiddlewareTest extends BaseTestObject
{
    const TOKEN_SECRET = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';

    /**
     * @var array
     */
    private $config = [
        'secret'    => self::TOKEN_SECRET,
        'secure'    => false,
        'algorithm' => ['HS512'],
        'path'      => ['/v1'],
        'nbf'       => 10,
        'exp'       => 10
    ];

    /**
     * @throws \ReflectionException
     */
    public function testInvoke()
    {
        $payload = ['token' => ['test' => 'testOne']];
        $callable = function (Request $request, Response $response) {
            return $response->withJson(['token' => $request->getAttribute('token')]);
        };
        $middleware = new JwtAuthenticationMiddleware($this->config);
        $request = Request::createFromEnvironment(
            Environment::mock(
                [
                    'REQUEST_URI'        => '/v1/test',
                    'HTTP_Authorization' => 'Bearer ' . JWT::encode(
                        ['test' => 'testOne'],
                        self::TOKEN_SECRET,
                        $this->config['algorithm'][0]
                    )
                ]
            )
        );
        $response = new Response;
        $result = $middleware->__invoke($request, $response, $callable);

        $this->assertInstanceOf(Response::class, $result);
        $this->assertEquals(StatusCode::HTTP_OK, $result->getStatusCode());
        $this->assertEquals($payload, json_decode($result->getBody()->__toString(), true));
    }
}
