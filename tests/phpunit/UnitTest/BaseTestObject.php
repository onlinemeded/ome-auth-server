<?php
/**
 * BaseTestObject.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest;

use PHPUnit\Framework\TestCase;
use Exception;
use Throwable;

/**
 * Class BaseTestObject
 */
class BaseTestObject extends TestCase
{
    /**
     * @param callable  $callable
     * @param Throwable $throwable
     *
     * @return Exception|Throwable|null
     */
    public function assertThrows(callable $callable, Throwable $throwable)
    {
        try {
            call_user_func_array($callable, array_slice(func_get_args(), 2));
        } catch (Throwable $caughtThrowable) {
            $this->assertInstanceOf(get_class($throwable), $caughtThrowable, $caughtThrowable);
            return $caughtThrowable;
        }

        $this->fail(
            sprintf(
                'Expected throwable "%s" was not thrown',
                get_class($throwable)
            )
        );
    }

    /**
     * @return array
     */
    public function getPrimitiveTypesArray()
    {
        return ['string', 1, -1, 1.1, 10000.00];
    }
}
