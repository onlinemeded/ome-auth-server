<?php
/**
 * OAuthRequestServiceTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Service;

use UnitTest\BaseTestObject;
use Domain\Auth\Service\OAuthRequestService;
use OAuth2\Request as OAuthRequest;

/**
 * Class OAuthRequestServiceTest
 */
class OAuthRequestServiceTest extends BaseTestObject
{
    const CUSTOM_SERVER_VAR = 'CUSTOM_SERVER_VAR';
    const X_CUSTOM_HEADER = 'X_CUSTOM_HEADER';
    const PARAMETER_1 = 'parameter1';
    const REQUEST_1 = 'request1';
    const ATTRIBUTE_1 = 'attribute1';
    const COOKIE_1 = 'cookie1';

    /**
     *
     */
    public function testGetRequestFromHttp()
    {
        $method = 'PUT';
        $query = [self::PARAMETER_1 => 'one'];
        $request = [self::REQUEST_1 => 'one'];
        $attributes = [self::ATTRIBUTE_1 => 'one'];
        $cookies = [self::COOKIE_1 => 'one'];
        $server = [self::CUSTOM_SERVER_VAR => 'serverCustom'];
        $content = 'Content';
        $headers = [self::X_CUSTOM_HEADER => 'headerCustom'];

        $result = OAuthRequestService::getRequestFromHttp(
            $method,
            $query,
            $request,
            $attributes,
            $cookies,
            [],
            $server,
            $content,
            $headers
        );

        $this->assertInstanceOf(OAuthRequest::class, $result);
        $this->assertArrayHasKey(self::PARAMETER_1, $result->query);
        $this->assertArrayHasKey(self::REQUEST_1, $result->request);
        $this->assertArrayHasKey(self::ATTRIBUTE_1, $result->attributes);
        $this->assertArrayHasKey(self::COOKIE_1, $result->cookies);
        $this->assertEquals($server[self::CUSTOM_SERVER_VAR], $result->server(self::CUSTOM_SERVER_VAR));
        $this->assertEquals($content, $result->content);
        $this->assertEquals($headers[self::X_CUSTOM_HEADER], $result->headers(self::X_CUSTOM_HEADER));
    }

    /**
     *
     */
    public function testGetRequestFromHttpWithUrlEncodedDataType()
    {
        $content = sprintf('%s=%s', self::CUSTOM_SERVER_VAR, self::X_CUSTOM_HEADER);

        $result = OAuthRequestService::getRequestFromHttp(
            'PUT',
            [],
            [],
            [],
            [],
            [],
            ['CONTENT_TYPE' => 'application/x-www-form-urlencoded'],
            $content
        );

        $this->assertEquals([self::CUSTOM_SERVER_VAR => self::X_CUSTOM_HEADER], $result->request);
    }

    /**
     *
     */
    public function testGetRequestFromHttpWithJsonDataType()
    {
        $parsedBody = [self::CUSTOM_SERVER_VAR => self::X_CUSTOM_HEADER];
        $content = json_encode($parsedBody);

        $result = OAuthRequestService::getRequestFromHttp(
            'PUT',
            [],
            [],
            [],
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $content
        );

        $this->assertEquals($parsedBody, $result->request);
    }
}
