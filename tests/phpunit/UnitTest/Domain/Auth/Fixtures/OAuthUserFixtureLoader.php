<?php
/**
 * OAuthUserFixtureLoader.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Fixtures;

use Domain\Auth\Infrastructure\Entity\ApiApplicationEntity;
use Domain\Auth\Infrastructure\Entity\OAuthUserEntity;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class OAuthUserFixtureLoader
 */
class OAuthUserFixtureLoader implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $userEntity = new OAuthUserEntity;
        $userEntity->setEmail(OAuthDTOInterface::USER_EMAIL_VALID);
        $userEntity->setPassword(OAuthDTOInterface::USER_PASSWORD_VALID);
        $userEntity->setApiApplication(
            $manager->getRepository(ApiApplicationEntity::class)->find(OAuthDTOInterface::APP_ID_VALID)
        );

        $userAltEntity = new OAuthUserEntity;
        $userAltEntity->setEmail(OAuthDTOInterface::USER_ALT_EMAIL_VALID);
        $userAltEntity->setPassword(OAuthDTOInterface::USER_PASSWORD_VALID);
        $userAltEntity->setApiApplication(
            $manager->getRepository(ApiApplicationEntity::class)->find(OAuthDTOInterface::APP_ALT_ID_VALID)
        );

        $manager->persist($userEntity);
        $manager->persist($userAltEntity);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 11;
    }
}
