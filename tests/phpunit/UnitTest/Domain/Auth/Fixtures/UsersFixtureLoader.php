<?php
/**
 * UsersFixtureLoader.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace UnitTest\Domain\Auth\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\Auth\Infrastructure\Entity\OAuthUserEntity;
use Domain\Auth\Infrastructure\Entity\UserEntity;

/**
 * Class UsersFixtureLoader
 */
class UsersFixtureLoader implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $userEntity = new UserEntity;
        $userEntity->setFirstName(OAuthDTOInterface::USER_FIRST_NAME_VALID);
        $userEntity->setLastName(OAuthDTOInterface::USER_LAST_NAME_VALID);
        $userEntity->setOauthUser(
            $manager->getRepository(OAuthUserEntity::class)->find(OAuthDTOInterface::USER_ID_VALID)
        );

        $manager->persist($userEntity);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 11;
    }
}
