<?php
/**
 * ApiApplicationFixtureLoader.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace UnitTest\Domain\Auth\Fixtures;

use Domain\Auth\Infrastructure\Entity\ApiApplicationEntity;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;

/**
 * Class ApiApplicationFixtureLoader
 */
class ApiApplicationFixtureLoader implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $apiApplicationEntity = new ApiApplicationEntity;
        $apiApplicationEntity->setApplicationIdentifier(OAuthDTOInterface::APP_IDENTIFIER_VALID);
        $apiApplicationEntity->setName(OAuthDTOInterface::APP_NAME_VALID);
        $apiApplicationEntity->setOauthClient(
            $manager->getRepository(OAuthClientEntity::class)->find(OAuthDTOInterface::APP_OAUTH_CLIENT_ID_VALID)
        );

        $apiApplicationAltEntity = new ApiApplicationEntity;
        $apiApplicationAltEntity->setApplicationIdentifier(OAuthDTOInterface::APP_ALT_IDENTIFIER_VALID);
        $apiApplicationAltEntity->setName(OAuthDTOInterface::APP_ALT_NAME_VALID);
        $apiApplicationAltEntity->setOauthClient(
            $manager->getRepository(OAuthClientEntity::class)->find(OAuthDTOInterface::APP_ALT_OAUTH_CLIENT_ID_VALID)
        );

        $manager->persist($apiApplicationEntity);
        $manager->persist($apiApplicationAltEntity);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 11;
    }
}
