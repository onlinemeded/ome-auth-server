<?php
/**
 * OAuthDTOInterface.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace UnitTest\Domain\Auth\Fixtures;

/**
 * Interface OAuthDTOInterface
 */
interface OAuthDTOInterface
{
    // Client
    const CLIENT_ID_VALID = 1;

    const CLIENT_IDENTIFIER_VALID = 'testClient';
    const CLIENT_IDENTIFIER_MISSING = 'missingClient';

    const CLIENT_REDIRECT_URI_VALID = 'http://localhost/';
    const CLIENT_SECRET_VALID = 'testPass';
    const CLIENT_NAME_VALID = 'Test Client';

    const CLIENT_ALT_ID_VALID = 2;
    const CLIENT_ALT_IDENTIFIER_VALID = 'testClientAlt';
    const CLIENT_ALT_NAME_VALID = 'Alternate Test Client';


    // User
    const USER_ID_VALID = 1;

    const USER_EMAIL_VALID = 'valid.email@unit.test';
    const USER_EMAIL_INVALID = 'invalid.email_unit.test';

    const USER_PASSWORD_VALID = '$ampleP455word!!';
    const USER_FIRST_NAME_VALID = 'FirstName';
    const USER_LAST_NAME_VALID = 'LastName';

    const USER_ALT_ID_VALID = 2;
    const USER_ALT_EMAIL_VALID = 'alt.valid.email@unit.test';
    const USER_ALT_FIRST_NAME_VALID = 'FirstNameAlternate';
    const USER_ALT_LAST_NAME_VALID = 'LastNameAlternate';


    // ApiKey
    const API_KEY_ID_VALID = 1;
    const API_KEY_APP_ID_VALID = self::APP_ID_VALID;
    const API_KEY_VALUE_VALID = 's)bczf(~Xhg9hfTf(P%N=pD+`5Zs#O';

    const API_ALT_KEY_ID_VALID = 2;
    const API_ALT_KEY_APP_ID_VALID = self::APP_ALT_ID_VALID;


    // ApiApplication
    const APP_ID_VALID = 1;
    const APP_OAUTH_CLIENT_ID_VALID = self::CLIENT_ID_VALID;
    const APP_NAME_VALID = 'Test Application';
    const APP_IDENTIFIER_VALID = 'A9DA3B10A2';

    const APP_ALT_ID_VALID = 2;
    const APP_ALT_OAUTH_CLIENT_ID_VALID = self::CLIENT_ALT_ID_VALID;
    const APP_ALT_NAME_VALID = 'Alternate Test Application';
    const APP_ALT_IDENTIFIER_VALID = 'B9DA3B10A2';
}
