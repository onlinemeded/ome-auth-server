<?php
/**
 * OAuthAccessTokenFixtureLoader.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Fixtures;

use Domain\Auth\Infrastructure\Entity\OAuthAccessTokenEntity;
use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OAuth2\ResponseType\AccessToken;
use DateInterval;
use DateTime;

/**
 * Class OAuthAccessTokenFixtureLoader
 */
class OAuthAccessTokenFixtureLoader implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * @var OAuthAccessTokenEntity|null
     */
    private static $expiredAccessToken;

    /**
     * @param ObjectManager $manager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $reflectedAccessToken = new \ReflectionClass(AccessToken::class);
        $generateAccessToken = $reflectedAccessToken->getMethod('generateAccessToken');
        $generateAccessToken->setAccessible(true);

        $accessToken = new OAuthAccessTokenEntity;
        $accessToken->setClient(
            $manager->getRepository(OAuthClientEntity::class)->findOneBy(
                ['clientIdentifier' => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID]
            )
        );
        $accessToken->setExpires(self::getCustomDate());
        $accessToken->setToken($generateAccessToken->invoke($reflectedAccessToken->newInstanceWithoutConstructor()));

        $manager->persist($accessToken);

        $expiredAccessToken = new OAuthAccessTokenEntity;
        $expiredAccessToken->setClient(
            $manager->getRepository(OAuthClientEntity::class)->findOneBy(
                ['clientIdentifier' => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID]
            )
        );
        $expiredAccessToken->setExpires(self::getCustomDate(true));
        $expiredAccessToken->setToken(
            $generateAccessToken->invoke($reflectedAccessToken->newInstanceWithoutConstructor())
        );

        $manager->persist($expiredAccessToken);
        $manager->flush();

        self::$expiredAccessToken = $expiredAccessToken;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 10;
    }

    /**
     * @param bool   $subtract
     * @param string $intervalSpec
     *
     * @return DateTime
     * @throws \Exception
     */
    public static function getCustomDate(bool $subtract = false, string $intervalSpec = 'P1D'): DateTime
    {
        $date = new DateTime;
        $interval = new DateInterval($intervalSpec);
        $date->{((false === $subtract)
            ? 'add'
            : 'sub')}(
            $interval
        );

        return $date;
    }

    /**
     * @return OAuthAccessTokenEntity|null
     */
    public static function getExpiredAccessToken(): ?OAuthAccessTokenEntity
    {
        return self::$expiredAccessToken;
    }
}
