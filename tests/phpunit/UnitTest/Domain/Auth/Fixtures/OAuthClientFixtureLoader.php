<?php
/**
 * OAuthClientFixtureLoader.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Fixtures;

use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class OAuthClientFixtureLoader
 */
class OAuthClientFixtureLoader implements OrderedFixtureInterface, FixtureInterface, OAuthDTOInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $clientEntity = new OAuthClientEntity;
        $clientEntity->setClientIdentifier(self::CLIENT_IDENTIFIER_VALID);
        $clientEntity->setClientSecret(self::CLIENT_SECRET_VALID);
        $clientEntity->setRedirectUri(self::CLIENT_REDIRECT_URI_VALID);
        $clientEntity->setName(self::CLIENT_NAME_VALID);

        $clientAltEntity = new OAuthClientEntity;
        $clientAltEntity->setClientIdentifier(self::CLIENT_ALT_IDENTIFIER_VALID);
        $clientAltEntity->setClientSecret(self::CLIENT_SECRET_VALID);
        $clientAltEntity->setRedirectUri(self::CLIENT_REDIRECT_URI_VALID);
        $clientAltEntity->setName(self::CLIENT_ALT_NAME_VALID);

        $manager->persist($clientEntity);
        $manager->persist($clientAltEntity);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}
