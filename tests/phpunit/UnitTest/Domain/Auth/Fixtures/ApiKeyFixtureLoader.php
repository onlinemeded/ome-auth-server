<?php
/**
 * ApiKeyFixtureLoader.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Fixtures;

use Domain\Auth\Infrastructure\Entity\ApiApplicationEntity;
use Domain\Auth\Infrastructure\Entity\ApiKeyEntity;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class ApiKeyFixtureLoader
 */
class ApiKeyFixtureLoader implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $apiKeyEntity = new ApiKeyEntity;
        $apiKeyEntity->setValue(OAuthDTOInterface::API_KEY_VALUE_VALID);
        $apiKeyEntity->setApiApplication(
            $manager->getRepository(ApiApplicationEntity::class)->find(OAuthDTOInterface::API_KEY_APP_ID_VALID)
        );

        $apiKeyAltEntity = new ApiKeyEntity;
        $apiKeyAltEntity->setValue(OAuthDTOInterface::API_ALT_KEY_ID_VALID);
        $apiKeyAltEntity->setApiApplication(
            $manager->getRepository(ApiApplicationEntity::class)->find(OAuthDTOInterface::API_ALT_KEY_APP_ID_VALID)
        );

        $manager->persist($apiKeyEntity);
        $manager->persist($apiKeyAltEntity);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 10;
    }
}
