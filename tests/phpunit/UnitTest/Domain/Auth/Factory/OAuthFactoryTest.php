<?php
/**
 * OAuthFactoryTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Factory;

use UnitTest\BaseTestObject;
use Application\Http\Response\OAuthResponse;
use Application\Http\StatusCode;
use Domain\Auth\Factory\OAuthFactory;

/**
 * Class OAuthFactoryTest
 */
class OAuthFactoryTest extends BaseTestObject
{
    /**
     *
     */
    public function testNewResponse()
    {
        $parameters = [];
        $headers = [];
        $response = OAuthFactory::newResponse($parameters, StatusCode::HTTP_ACCEPTED, $headers);

        $this->assertInstanceOf(OAuthResponse::class, $response);
        $this->assertEquals($parameters, $response->getParameters());
        $this->assertEquals(StatusCode::HTTP_ACCEPTED, $response->getStatusCode());
        $this->assertEquals($headers, $response->getHttpHeaders());
    }
}
