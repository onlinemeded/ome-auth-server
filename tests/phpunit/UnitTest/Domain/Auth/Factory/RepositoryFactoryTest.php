<?php
/**
 * RepositoryFactoryTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Factory;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use Application\Http\ApplicationInterface;
use Domain\Auth\Factory\RepositoryFactory;
use Domain\Auth\Infrastructure\Entity\ApiApplicationRepository;
use Domain\Auth\Infrastructure\Entity\ApiKeyRepository;
use Domain\Auth\Infrastructure\Entity\OAuthClientRepository;
use Domain\Auth\Infrastructure\Entity\OAuthUserRepository;
use Domain\Auth\Infrastructure\Entity\UserRepository;
use Domain\Auth\Repository\OAuthRepository;
use OAuth2\Server as OAuthServer;

/**
 * Class RepositoryFactoryTest
 */
class RepositoryFactoryTest extends BaseTestObject
{
    /**
     *
     */
    public function testNewOAuthRepository(): void
    {
        $this->assertInstanceOf(OAuthRepository::class, $this->newSUT()->newOAuthRepository());
    }

    /**
     *
     */
    public function testNewApiApplicationRepository(): void
    {
        $this->assertInstanceOf(ApiApplicationRepository::class, $this->newSUT()->newApiApplicationRepository());
    }

    /**
     *
     */
    public function testNewApiKeyRepository(): void
    {
        $this->assertInstanceOf(ApiKeyRepository::class, $this->newSUT()->newApiKeyRepository());
    }

    /**
     *
     */
    public function testNewOAuthClientRepository(): void
    {
        $this->assertInstanceOf(OAuthClientRepository::class, $this->newSUT()->newOAuthClientRepository());
    }

    /**
     *
     */
    public function testNewOAuthUserRepository(): void
    {
        $this->assertInstanceOf(OAuthUserRepository::class, $this->newSUT()->newOAuthUserRepository());
    }

    /**
     *
     */
    public function testNewUserRepository(): void
    {
        $this->assertInstanceOf(UserRepository::class, $this->newSUT()->newUserRepository());
    }

    /**
     * @return RepositoryFactory
     */
    private function newSUT(): RepositoryFactory
    {
        /** @var OAuthServer $serverInstance */
        static $entityManager = null;
        static $serverInstance = null;

        if (null === $serverInstance) {
            $entityManager = ApplicationFactory::getContainerStaticProxy()->offsetGet(
                ApplicationInterface::APP_ENTITY_MANAGER
            );

            $serverInstance = ApplicationFactory::getContainerStaticProxy()->offsetGet(
                ApplicationInterface::APP_OAUTH_SERVER
            );
        }

        return new RepositoryFactory($entityManager, $serverInstance);
    }
}
