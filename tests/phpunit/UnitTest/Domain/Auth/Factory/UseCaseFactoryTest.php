<?php
/**
 * UseCaseFactoryTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Factory;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use Domain\Auth\Factory\UseCaseFactory;
use Domain\Auth\Factory\RepositoryFactory;
use Domain\Auth\UseCase\ApiApplicationUseCase;
use Domain\Auth\UseCase\OAuthAccessTokenUseCase;
use Domain\Auth\UseCase\OAuthAuthorizationCodeUseCase;
use Domain\Auth\UseCase\OAuthClientUseCase;
use Domain\Auth\UseCase\OAuthResourceUseCase;
use Domain\Auth\UseCase\OAuthUserUseCase;
use Domain\Auth\UseCase\UserUseCase;

/**
 * Class UseCaseFactoryTest
 */
class UseCaseFactoryTest extends BaseTestObject
{
    /**
     *
     */
    public function testNewApiApplicationUseCase(): void
    {
        $this->assertInstanceOf(ApiApplicationUseCase::class, $this->newSUT()->newApiApplicationUseCase());
    }

    /**
     *
     */
    public function testNewOAuthAccessTokenUseCase(): void
    {
        $this->assertInstanceOf(OAuthAccessTokenUseCase::class, $this->newSUT()->newOAuthAccessTokenUseCase());
    }

    /**
     *
     */
    public function testNewOAuthAuthorizationCodeUseCase(): void
    {
        $this->assertInstanceOf(
            OAuthAuthorizationCodeUseCase::class,
            $this->newSUT()->newOAuthAuthorizationCodeUseCase()
        );
    }

    /**
     *
     */
    public function testNewOAuthClientUseCase(): void
    {
        $this->assertInstanceOf(OAuthClientUseCase::class, $this->newSUT()->newOAuthClientUseCase());
    }

    /**
     *
     */
    public function testNewOAuthResourceUseCase(): void
    {
        $this->assertInstanceOf(OAuthResourceUseCase::class, $this->newSUT()->newOAuthResourceUseCase());
    }

    /**
     *
     */
    public function testNewOAuthUserUseCase(): void
    {
        $this->assertInstanceOf(OAuthUserUseCase::class, $this->newSUT()->newOAuthUserUseCase());
    }

    /**
     *
     */
    public function testNewUserUseCase(): void
    {
        $this->assertInstanceOf(UserUseCase::class, $this->newSUT()->newUserUseCase());
    }

    /**
     * @return UseCaseFactory
     */
    private function newSUT(): UseCaseFactory
    {
        /** @var RepositoryFactory $repoInstance */
        static $repoInstance = null;

        if (null === $repoInstance) {
            $repoInstance = ApplicationFactory::getContainerStaticProxy()->offsetGet(
                RepositoryFactory::class
            );
        }

        return new UseCaseFactory($repoInstance);
    }
}
