<?php
/**
 * OAuthRepositoryTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Repository;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Application\Http\ApplicationInterface;
use Application\Http\StatusCode;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;
use Domain\Auth\Repository\OAuthRepository;
use Domain\Auth\Service\OAuthRequestService;
use OAuth2\Server as OAuthServer;

/**
 * Class OAuthRepositoryTest
 */
class OAuthRepositoryTest extends BaseTestObject
{
    /**
     *
     */
    public function testAuthenticateWithClientCredentials(): void
    {
        $result = $this->newSUT()->authenticateWithClientCredentials(
            OAuthRequestService::getRequestFromHttp(
                'POST',
                [],
                ['grant_type' => 'client_credentials'],
                [],
                [],
                [],
                [
                    'PHP_AUTH_USER' => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                    'PHP_AUTH_PW'   => OAuthDTOInterface::CLIENT_SECRET_VALID
                ]
            )
        );
        $responseBody = $result->getResponseBody();

        $this->assertEquals(StatusCode::HTTP_OK, $result->getStatusCode());
        $this->assertIsArray($responseBody);
        $this->assertArrayHasKey('access_token', $responseBody);
        $this->assertIsString($responseBody['access_token']);
        $this->assertEquals(40, strlen($responseBody['access_token']));
        $this->assertArrayHasKey('expires_in', $responseBody);
        $this->assertIsInt($responseBody['expires_in']);
        $this->assertArrayHasKey('token_type', $responseBody);
        $this->assertIsString($responseBody['token_type']);
        $this->assertArrayHasKey('scope', $responseBody);

        $this->accessToken = $responseBody['access_token'];
    }

    /**
     *
     */
    public function testAuthenticateWithAccessToken(): void
    {
        $accessToken = $this->newSUT()->authenticateWithClientCredentials(
            OAuthRequestService::getRequestFromHttp(
                'POST',
                [],
                ['grant_type' => 'client_credentials'],
                [],
                [],
                [],
                [
                    'PHP_AUTH_USER' => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                    'PHP_AUTH_PW'   => OAuthDTOInterface::CLIENT_SECRET_VALID
                ]
            )
        );
        $accessToken = $accessToken->getResponseBody()['access_token'];

        $result = $this->newSUT()->authenticateWithAccessToken(
            OAuthRequestService::getRequestFromHttp('POST', [], ['access_token' => $accessToken])
        );

        $this->assertEquals(StatusCode::HTTP_OK, $result->getStatusCode());
        $this->assertTrue(empty($result->getResponseBody()));
    }

    /**
     * @throws AuthenticationException
     */
    public function testAuthenticateWithAuthorizationCode(): void
    {
        $result = $this->newSUT()->authenticateWithAuthorizationCode(
            OAuthRequestService::getRequestFromHttp(
                'GET',
                [
                    'client_id'     => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                    'redirect_uri'  => OAuthDTOInterface::CLIENT_REDIRECT_URI_VALID,
                    'response_type' => 'code',
                    'state'         => sha1(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID . 'someSalt')
                ]
            )
        );

        $this->assertEquals(StatusCode::HTTP_FOUND, $result->getStatusCode());
        $this->assertTrue(empty($result->getResponseBody()));
    }

    /**
     *
     */
    public function testAuthenticateWithAuthorizationCodeWithErrorException(): void
    {
        $this->assertThrows(
            function (OAuthRepository $repository) {
                // missing client_id
                $repository->authenticateWithAuthorizationCode(
                    OAuthRequestService::getRequestFromHttp(
                        'GET',
                        [
                            'redirect_uri'  => OAuthDTOInterface::CLIENT_REDIRECT_URI_VALID,
                            'response_type' => 'code',
                            'state'         => sha1(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID . 'someSalt')
                        ]
                    )
                );
            },
            new AuthenticationException,
            $this->newSUT()
        );
    }

    /**
     * @return OAuthRepository
     */
    private function newSUT(): OAuthRepository
    {
        /** @var OAuthServer $serverInstance */
        static $serverInstance = null;

        if (null === $serverInstance) {
            $serverInstance = ApplicationFactory::getContainerStaticProxy()->offsetGet(
                ApplicationInterface::APP_OAUTH_SERVER
            );
        }

        return new OAuthRepository($serverInstance);
    }
}
