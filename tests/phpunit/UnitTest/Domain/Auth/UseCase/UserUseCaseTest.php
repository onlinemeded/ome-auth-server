<?php
/**
 * UserUseCaseTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\UseCase;

use UnitTest\Application\ApplicationFactory;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use UnitTest\BaseTestObject;
use Domain\Auth\Factory\RepositoryFactory;
use Domain\Auth\UseCase\UserUseCase;

/**
 * Class UserUseCaseTest
 */
class UserUseCaseTest extends BaseTestObject
{
    /**
     *
     */
    public function testGetUsers(): void
    {
        $result = $this->newSUT()->getUsers(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID);

        $this->assertIsArray($result);
        $this->assertArrayHasKey(0, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $this->assertArrayHasKey('email', $result[0]);
        $this->assertArrayHasKey('name', $result[0]);
        $this->assertArrayHasKey('user_name', $result[0]);
        $this->assertArrayHasKey('title', $result[0]);
        $this->assertEquals(
            [
                'id'        => OAuthDTOInterface::USER_ID_VALID,
                'email'     => OAuthDTOInterface::USER_EMAIL_VALID,
                'name'      => OAuthDTOInterface::USER_FIRST_NAME_VALID . ' ' . OAuthDTOInterface::USER_LAST_NAME_VALID,
                'user_name' => substr(
                    OAuthDTOInterface::USER_EMAIL_VALID,
                    0,
                    strpos(OAuthDTOInterface::USER_EMAIL_VALID, '@')
                ),
                'title'     => 'Title'
            ],
            $result[0]
        );
    }

    /**
     *
     */
    public function testGetUserById(): void
    {
        $result = $this->newSUT()->getUserById(
            OAuthDTOInterface::USER_EMAIL_VALID,
            OAuthDTOInterface::CLIENT_IDENTIFIER_VALID
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('id', $result);
        $this->assertArrayHasKey('email', $result);
        $this->assertEquals(
            [
                'id'        => OAuthDTOInterface::USER_ID_VALID,
                'email'     => OAuthDTOInterface::USER_EMAIL_VALID,
                'name'      => OAuthDTOInterface::USER_FIRST_NAME_VALID . ' ' . OAuthDTOInterface::USER_LAST_NAME_VALID,
                'user_name' => substr(
                    OAuthDTOInterface::USER_EMAIL_VALID,
                    0,
                    strpos(OAuthDTOInterface::USER_EMAIL_VALID, '@')
                ),
                'title'     => 'Title'
            ],
            $result
        );
    }

    /**
     *
     */
    public function testGetUserByIdMissingUser(): void
    {
        $result = $this->newSUT()->getUserById(
            OAuthDTOInterface::USER_EMAIL_INVALID,
            OAuthDTOInterface::CLIENT_IDENTIFIER_VALID
        );

        $this->assertIsArray($result);
        $this->assertEquals([], $result);
    }

    /**
     * @return UserUseCase
     */
    private function newSUT(): UserUseCase
    {
        /** @var RepositoryFactory $repoInstance */
        static $repoInstance = null;

        if (null === $repoInstance) {
            $repoInstance = ApplicationFactory::getContainerStaticProxy()->offsetGet(RepositoryFactory::class);
        }

        return new UserUseCase($repoInstance->newUserRepository(), $repoInstance->newOAuthUserRepository());
    }
}
