<?php
/**
 * OAuthClientUseCaseTest.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace UnitTest\Domain\Auth\UseCase;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use Domain\Auth\Factory\RepositoryFactory;
use Domain\Auth\UseCase\OAuthClientUseCase;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;

/**
 * Class OAuthClientUseCaseTest
 */
class OAuthClientUseCaseTest extends BaseTestObject
{
    /**
     *
     */
    public function testGetClients(): void
    {
        $result = $this->newSUT()->getClients();

        $this->assertIsArray($result);
        $this->assertArrayHasKey(0, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $this->assertEquals(OAuthDTOInterface::CLIENT_ID_VALID, $result[0]['id']);
        $this->assertArrayHasKey('identifier', $result[0]);
        $this->assertEquals(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID, $result[0]['identifier']);
    }

    /**
     *
     */
    public function testGetClientByIdentifier(): void
    {
        $result = $this->newSUT()->getClientByIdentifier(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID);

        $this->assertArrayHasKey('clientIdentifier', $result);
        $this->assertEquals(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID, $result['clientIdentifier']);
        $this->assertArrayHasKey('redirectUrl', $result);
        $this->assertEquals(OAuthDTOInterface::CLIENT_REDIRECT_URI_VALID, $result['redirectUrl']);
    }

    /**
     *
     */
    public function testGetClientByIdentifierWithMissingClientIdentifier(): void
    {
        $result = $this->newSUT()->getClientByIdentifier(OAuthDTOInterface::CLIENT_IDENTIFIER_MISSING);

        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }

    /**
     * @return OAuthClientUseCase
     */
    private function newSUT(): OAuthClientUseCase
    {
        /** @var RepositoryFactory $repoInstance */
        static $repoInstance = null;

        if (null === $repoInstance) {
            $repoInstance = ApplicationFactory::getContainerStaticProxy()->offsetGet(RepositoryFactory::class);
        }

        return new OAuthClientUseCase($repoInstance->newOAuthClientRepository(), $repoInstance->newApiKeyRepository());
    }
}
