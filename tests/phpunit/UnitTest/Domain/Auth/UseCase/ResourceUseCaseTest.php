<?php
/**
 * ResourceUseCaseTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\UseCase;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use UnitTest\Domain\Auth\Fixtures\OAuthAccessTokenFixtureLoader;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Domain\Auth\Factory\RepositoryFactory;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;
use Domain\Auth\Service\OAuthRequestService;
use Domain\Auth\UseCase\OAuthResourceUseCase;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\Tools\ToolsException;

/**
 * Class ResourceUseCaseTest
 */
class ResourceUseCaseTest extends BaseTestObject
{
    /**
     * @var string
     */
    private static $accessToken;

    /**
     * @throws DBALException
     * @throws ToolsException
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        /** @var RepositoryFactory $repoInstance */
        $repoInstance = ApplicationFactory::getContainerStaticProxy()->offsetGet(RepositoryFactory::class);

        $accessToken = $repoInstance->newOAuthRepository()->authenticateWithClientCredentials(
            OAuthRequestService::getRequestFromHttp(
                'POST',
                [],
                ['grant_type' => 'client_credentials'],
                [],
                [],
                [],
                [
                    'PHP_AUTH_USER' => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                    'PHP_AUTH_PW'   => OAuthDTOInterface::CLIENT_SECRET_VALID
                ]
            )
        );
        self::$accessToken = $accessToken->getResponseBody()['access_token'];
    }

    /**
     * @throws AuthenticationException
     * @throws DBALException
     * @throws ToolsException
     */
    public function testAuthenticateWithAccessToken()
    {
        $result = $this->newSUT()->authenticateWithAccessToken(self::$accessToken);

        $this->assertIsArray($result);
        $this->assertArrayHasKey('success', $result);
        $this->assertTrue($result['success']);
    }

    /**
     * @throws DBALException
     * @throws ToolsException
     */
    public function testAuthenticateWithAccessTokenInvalidTokenException()
    {
        $exception = $this->assertThrows(
            function (OAuthResourceUseCase $useCase) {
                $useCase->authenticateWithAccessToken('invalidToken');
            },
            new AuthenticationException,
            $this->newSUT()
        );

        $this->assertEquals('The access token provided is invalid', $exception->getMessage());
    }

    /**
     * @throws DBALException
     * @throws ToolsException
     */
    public function testAuthenticateWithAccessTokenExpiredTokenException()
    {
        $exception = $this->assertThrows(
            function (OAuthResourceUseCase $useCase) {
                $useCase->authenticateWithAccessToken(
                    OAuthAccessTokenFixtureLoader::getExpiredAccessToken()->getToken()
                );
            },
            new AuthenticationException,
            $this->newSUT()
        );

        $this->assertEquals('The access token provided has expired', $exception->getMessage());
    }

    /**
     * @return OAuthResourceUseCase
     * @throws DBALException
     * @throws ToolsException
     */
    private function newSUT(): OAuthResourceUseCase
    {
        /** @var RepositoryFactory $repoInstance */
        static $repoInstance = null;

        if (null === $repoInstance) {
            $repoInstance = ApplicationFactory::getContainerStaticProxy()->offsetGet(RepositoryFactory::class);
        }

        return new OAuthResourceUseCase($repoInstance->newOAuthRepository());
    }
}
