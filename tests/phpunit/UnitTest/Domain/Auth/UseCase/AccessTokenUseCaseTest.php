<?php
/**
 * AccessTokenUseCaseTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\UseCase;

use UnitTest\Application\ApplicationFactory;
use UnitTest\Application\Http\Request\RequestService;
use UnitTest\BaseTestObject;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Domain\Auth\Factory\RepositoryFactory;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;
use Domain\Auth\UseCase\OAuthAccessTokenUseCase;

/**
 * Class AccessTokenUseCaseTest
 */
class AccessTokenUseCaseTest extends BaseTestObject
{
    /**
     * @throws AuthenticationException
     */
    public function testAuthenticateWithClientCredentials(): void
    {
        $result = $this->newSUT()->handleAuthenticateRequest(
            RequestService::createSlimRequest(
                'POST',
                [
                    'HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded',
                    'PHP_AUTH_USER'     => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                    'PHP_AUTH_PW'       => OAuthDTOInterface::CLIENT_SECRET_VALID
                ],
                ['grant_type' => 'client_credentials']
            )
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('access_token', $result);
        $this->assertIsString($result['access_token']);
        $this->assertEquals(40, strlen($result['access_token']));
        $this->assertArrayHasKey('expires_in', $result);
        $this->assertIsInt($result['expires_in']);
        $this->assertArrayHasKey('token_type', $result);
        $this->assertIsString($result['token_type']);
        $this->assertArrayHasKey('scope', $result);
    }

    /**
     * @throws AuthenticationException
     */
    public function testAuthenticateWithUserCredentials(): void
    {
        $result = $this->newSUT()->handleAuthenticateRequest(
            RequestService::createSlimRequest(
                'POST',
                [
                    'HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded',
                    'PHP_AUTH_USER'     => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                    'PHP_AUTH_PW'       => OAuthDTOInterface::CLIENT_SECRET_VALID
                ],
                [
                    'grant_type' => 'password',
                    'username'   => OAuthDTOInterface::USER_EMAIL_VALID,
                    'password'   => OAuthDTOInterface::USER_PASSWORD_VALID
                ]
            )
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('access_token', $result);
        $this->assertIsString($result['access_token']);
        $this->assertEquals(40, strlen($result['access_token']));
        $this->assertArrayHasKey('expires_in', $result);
        $this->assertIsInt($result['expires_in']);
        $this->assertArrayHasKey('token_type', $result);
        $this->assertIsString($result['token_type']);
        $this->assertArrayHasKey('scope', $result);
    }

    /**
     *
     */
    public function testAuthenticateWithInvalidCredentialsException(): void
    {
        $exception = $this->assertThrows(
            function (OAuthAccessTokenUseCase $useCase) {
                $useCase->handleAuthenticateRequest(
                    RequestService::createSlimRequest(
                        'POST',
                        [
                            'HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded',
                            'PHP_AUTH_USER'     => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                            'PHP_AUTH_PW'       => 'invalidSecret'
                        ],
                        ['grant_type' => 'client_credentials']
                    )
                );
            },
            new AuthenticationException,
            $this->newSUT()
        );

        $this->assertEquals('The client credentials are invalid', $exception->getMessage());
    }

    /**
     *
     */
    public function testAuthenticateWithInvalidUserClientRelationshipException(): void
    {
        $exception = $this->assertThrows(
            function (OAuthAccessTokenUseCase $useCase) {
                $useCase->handleAuthenticateRequest(
                    RequestService::createSlimRequest(
                        'POST',
                        [
                            'HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded',
                            'PHP_AUTH_USER'     => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                            'PHP_AUTH_PW'       => OAuthDTOInterface::CLIENT_SECRET_VALID
                        ],
                        [
                            'grant_type' => 'password',
                            'username'   => OAuthDTOInterface::USER_ALT_EMAIL_VALID,
                            'password'   => OAuthDTOInterface::USER_PASSWORD_VALID
                        ]
                    )
                );
            },
            new AuthenticationException,
            $this->newSUT()
        );

        $this->assertEquals('User does not belong to client', $exception->getMessage());
    }

    /**
     * @return OAuthAccessTokenUseCase
     */
    private function newSUT(): OAuthAccessTokenUseCase
    {
        /** @var RepositoryFactory $repoInstance */
        static $repoInstance = null;

        if (null === $repoInstance) {
            $repoInstance = ApplicationFactory::getContainerStaticProxy()->offsetGet(RepositoryFactory::class);
        }

        return new OAuthAccessTokenUseCase(
            $repoInstance->newOAuthRepository(), $repoInstance->newOAuthUserRepository()
        );
    }
}
