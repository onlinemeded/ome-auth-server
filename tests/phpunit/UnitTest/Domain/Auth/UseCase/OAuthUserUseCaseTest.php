<?php
/**
 * OAuthUserUseCaseTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\UseCase;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use Domain\Auth\Factory\RepositoryFactory;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;
use Domain\Auth\UseCase\OAuthUserUseCase;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;

/**
 * Class OAuthUserUseCaseTest
 */
class OAuthUserUseCaseTest extends BaseTestObject
{
    /**
     * @throws AuthenticationException
     */
    public function testAuthenticateUser(): void
    {
        $result = $this->newSUT()->authenticateUser(
            OAuthDTOInterface::USER_EMAIL_VALID,
            OAuthDTOInterface::USER_PASSWORD_VALID
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('success', $result);
        $this->assertTrue($result['success']);
    }

    /**
     *
     */
    public function testAuthenticateUserInvalidCredentialException(): void
    {
        $this->assertThrows(
            function (OAuthUserUseCase $useCase) {
                $useCase->authenticateUser(OAuthDTOInterface::USER_EMAIL_INVALID, '');
            },
            new AuthenticationException,
            $this->newSUT()
        );
    }

    /**
     * @return OAuthUserUseCase
     */
    private function newSUT(): OAuthUserUseCase
    {
        /** @var RepositoryFactory $repoInstance */
        static $repoInstance = null;

        if (null === $repoInstance) {
            $repoInstance = ApplicationFactory::getContainerStaticProxy()->offsetGet(RepositoryFactory::class);
        }

        return new OAuthUserUseCase($repoInstance->newOAuthUserRepository());
    }
}
