<?php
/**
 * AuthorizationCodeUseCaseTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\UseCase;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use Domain\Auth\Factory\RepositoryFactory;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;
use Domain\Auth\UseCase\OAuthAuthorizationCodeUseCase;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;

/**
 * Class AuthorizationCodeUseCaseTest
 */
class AuthorizationCodeUseCaseTest extends BaseTestObject
{
    /**
     * @throws AuthenticationException
     */
    public function testAuthenticationWithAuthorizationCode(): void
    {
        $result = $this->newSUT()->authenticateWithAuthorizationCode(
            OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
            OAuthDTOInterface::CLIENT_REDIRECT_URI_VALID,
            'code',
            sha1(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID . 'someSalt')
        );

        $this->assertIsString($result);
        $this->assertTrue(false !== strpos($result, OAuthDTOInterface::CLIENT_REDIRECT_URI_VALID));
    }

    /**
     *
     */
    public function testAuthenticationWithAuthorizationCodeErrorException(): void
    {
        $this->assertThrows(
            function (OAuthAuthorizationCodeUseCase $useCase) {
                // missing client_id
                $useCase->authenticateWithAuthorizationCode(
                    OAuthDTOInterface::CLIENT_IDENTIFIER_MISSING,
                    OAuthDTOInterface::CLIENT_REDIRECT_URI_VALID,
                    'code',
                    sha1(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID . 'someSalt')
                );
            },
            new AuthenticationException,
            $this->newSUT()
        );
    }

    /**
     * @return OAuthAuthorizationCodeUseCase
     */
    private function newSUT(): OAuthAuthorizationCodeUseCase
    {
        /** @var RepositoryFactory $repoInstance */
        static $repoInstance = null;

        if (null === $repoInstance) {
            $repoInstance = ApplicationFactory::getContainerStaticProxy()->offsetGet(RepositoryFactory::class);
        }

        return new OAuthAuthorizationCodeUseCase($repoInstance->newOAuthRepository());
    }
}
