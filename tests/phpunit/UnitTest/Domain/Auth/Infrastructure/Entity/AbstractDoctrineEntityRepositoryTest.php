<?php
/**
 * AbstractDoctrineEntityRepositoryTest.php
 *
 * @copyright 2019 Patrick Loven. All Rights Reserved.
 */

namespace UnitTest\Domain\Auth\Infrastructure\Entity;

use UnitTest\Application\ApplicationFactory;
use UnitTest\BaseTestObject;
use Application\Http\ApplicationInterface;
use Infrastructure\Entity\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Exception;

/**
 * Class AbstractDoctrineEntityRepositoryTest
 */
abstract class AbstractDoctrineEntityRepositoryTest extends BaseTestObject
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ClassMetadata
     */
    protected $classMetaData;

    /**
     * @param string $entityClassName
     */
    protected function init(string $entityClassName): void
    {
        try {
            $this->classMetaData = new ClassMetadata($entityClassName);
            $this->entityManager = ApplicationFactory::getContainerStaticProxy()->offsetGet(
                ApplicationInterface::APP_ENTITY_MANAGER
            );
        } catch (Exception $e) {
            $this->fail($e->getMessage());
        }
    }
}
