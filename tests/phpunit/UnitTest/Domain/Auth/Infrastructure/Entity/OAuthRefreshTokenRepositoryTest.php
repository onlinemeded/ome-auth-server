<?php
/**
 * OAuthRefreshTokenRepositoryTest.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace UnitTest\Domain\Auth\Infrastructure\Entity\OAuth;

use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use UnitTest\Infrastructure\Entity\EntityBackedTestObject;
use Domain\Auth\Infrastructure\Entity\OAuthRefreshTokenEntity;
use Domain\Auth\Infrastructure\Entity\OAuthRefreshTokenRepository;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use OAuth2\ResponseType\AccessToken;
use DateInterval;
use DateTime;
use ReflectionException;

/**
 * Class OAuthRefreshTokenRepositoryTest
 */
class OAuthRefreshTokenRepositoryTest extends EntityBackedTestObject
{
    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ReflectionException
     */
    public function testSetRefreshToken(): void
    {
        $expires = (new DateTime)->add(new DateInterval('P1D'));

        $reflectedAccessToken = new \ReflectionClass(AccessToken::class);
        $generateAccessToken = $reflectedAccessToken->getMethod('generateAccessToken');
        $generateAccessToken->setAccessible(true);
        $oAuthToken = $generateAccessToken->invoke($reflectedAccessToken->newInstanceWithoutConstructor());

        $sut = $this->newSUT();
        $sut->setRefreshToken(
            $oAuthToken,
            OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
            OAuthDTOInterface::USER_EMAIL_VALID,
            $expires->getTimestamp()
        );

        /** @var OAuthRefreshTokenEntity $refreshTokenEntity */
        $refreshTokenEntity = $sut->findOneBy(['refreshToken' => $oAuthToken]);

        $this->assertInstanceOf(OAuthRefreshTokenEntity::class, $refreshTokenEntity);
        $this->assertEquals($oAuthToken, $refreshTokenEntity->getRefreshToken());

        self::$entities[] = $refreshTokenEntity;
    }

    /**
     *
     */
    public function testGetAccessToken(): void
    {
        /** @var OAuthRefreshTokenEntity $refreshTokenEntity */
        $refreshTokenEntity = end(self::$entities);

        $result = $this->newSUT()->getRefreshToken($refreshTokenEntity->getRefreshToken());

        $this->assertIsArray($result);
        $this->assertArrayHasKey('refresh_token', $result);
        $this->assertEquals($refreshTokenEntity->getRefreshToken(), $result['refresh_token']);
        $this->assertArrayHasKey('client_id', $result);
        $this->assertEquals($refreshTokenEntity->getClient()->getClientIdentifier(), $result['client_id']);
        $this->assertArrayHasKey('user_id', $result);
        $this->assertEquals($refreshTokenEntity->getUser()->getId(), $result['user_id']);
        $this->assertArrayHasKey('expires', $result);
        $this->assertEquals($refreshTokenEntity->getExpires()->getTimestamp(), $result['expires']);
        $this->assertArrayHasKey('scope', $result);
        $this->assertEquals($refreshTokenEntity->getScope(), $result['scope']);
    }

    /**
     *
     */
    public function testGetAccessTokenWithInvalidToken(): void
    {
        $this->assertNull($this->newSUT()->getRefreshToken('invalidToken'));
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testUnsetRefreshToken(): void
    {
        /** @var OAuthRefreshTokenEntity $refreshTokenEntity */
        $refreshTokenEntity = array_pop(self::$entities);
        $repo = $this->newSUT();

        $repo->unsetRefreshToken($refreshTokenEntity->getRefreshToken());

        $this->assertNull($repo->findOneBy(['refreshToken' => $refreshTokenEntity->getRefreshToken()]));
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testUnsetRefreshTokenWithInvalidCode(): void
    {
        $this->assertNull($this->newSUT()->unsetRefreshToken('invalidCode'));
    }

    /**
     * @return OAuthRefreshTokenRepository
     */
    private function newSUT(): OAuthRefreshTokenRepository
    {
        return new OAuthRefreshTokenRepository(self::$em, self::$em->getClassMetadata(OAuthRefreshTokenEntity::class));
    }
}
