<?php
/**
 * OAuthUserRepositoryTest.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace UnitTest\Domain\Auth\Infrastructure\Entity\OAuth;

use UnitTest\BaseTestObject;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use UnitTest\Application\ApplicationFactory;
use Application\Http\Application;
use Domain\Auth\Infrastructure\Entity\OAuthUserEntity;
use Domain\Auth\Infrastructure\Entity\OAuthUserRepository;
use Infrastructure\Entity\EntityManager;

/**
 * Class OAuthUserRepositoryTest
 */
class OAuthUserRepositoryTest extends BaseTestObject
{
    /**
     * @var EntityManager
     */
    private static $em;

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$em = ApplicationFactory::getContainerStaticProxy()->offsetGet(Application::APP_ENTITY_MANAGER);
    }

    /**
     *
     */
    public function testCheckUserCredentialsForValidUser()
    {
        $this->assertTrue(
            $this->newSUT()->checkUserCredentials(
                OAuthDTOInterface::USER_EMAIL_VALID,
                OAuthDTOInterface::USER_PASSWORD_VALID
            )
        );
    }

    /**
     *
     */
    public function testCheckUserCredentialsForInvalidUser()
    {
        $this->assertFalse($this->newSUT()->checkUserCredentials(OAuthDTOInterface::CLIENT_IDENTIFIER_MISSING, ''));
    }

    /**
     *
     */
    public function testCheckUserCredentialsForValidUserNonVerifiedSecret()
    {
        $this->assertFalse(
            $this->newSUT()->checkUserCredentials(
                OAuthDTOInterface::USER_EMAIL_VALID,
                'nonVerifiedPassword'
            )
        );
    }

    /**
     *
     */
    public function testGetUserDetailsForValidUser()
    {
        $result = $this->newSUT()->getUserDetails(OAuthDTOInterface::USER_EMAIL_VALID);

        $this->assertIsArray($result);
        $this->assertArrayHasKey('user_id', $result);
        $this->assertEquals(OAuthDTOInterface::USER_ID_VALID, $result['user_id']);
        $this->assertArrayHasKey('scope', $result);
        $this->assertNull($result['scope']);
    }

    /**
     *
     */
    public function testGetUserDetailsForInvalidUser()
    {
        $this->assertNull($this->newSUT()->getUserDetails('invalidEmail@unit.test'));
    }

    /**
     * @return OAuthUserRepository
     */
    private function newSUT(): OAuthUserRepository
    {
        return new OAuthUserRepository(self::$em, self::$em->getClassMetadata(OAuthUserEntity::class));
    }
}
