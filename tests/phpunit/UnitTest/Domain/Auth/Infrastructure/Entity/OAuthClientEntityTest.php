<?php
/**
 * OAuthClientEntityTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Infrastructure\Entity\OAuth;

use UnitTest\BaseTestObject;
use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;

/**
 * Class OAuthClientEntityTest
 */
class OAuthClientEntityTest extends BaseTestObject
{
    /**
     *
     */
    public function testVerifyClientSecret()
    {
        $sut = new OAuthClientEntity;
        $clientSecret = 'clientSecret';
        $sut->setClientSecret($clientSecret);

        $this->assertTrue($sut->verifyClientSecret($clientSecret));
    }
}
