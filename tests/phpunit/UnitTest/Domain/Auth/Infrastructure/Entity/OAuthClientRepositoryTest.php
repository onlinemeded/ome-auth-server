<?php
/**
 * OAuthClientRepositoryTest.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace UnitTest\Domain\Auth\Infrastructure\Entity\OAuth;

use UnitTest\BaseTestObject;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use UnitTest\Application\ApplicationFactory;
use Application\Http\Application;
use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;
use Domain\Auth\Infrastructure\Entity\OAuthClientRepository;
use Infrastructure\Entity\EntityManager;

/**
 * Class OAuthClientRepositoryTest
 */
class OAuthClientRepositoryTest extends BaseTestObject
{
    /**
     * @var EntityManager
     */
    private static $em;

    /**
     *
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$em = ApplicationFactory::getContainerStaticProxy()->offsetGet(Application::APP_ENTITY_MANAGER);
    }

    /**
     *
     */
    public function testGetClientDetailsForValidClient()
    {
        $result = $this->newSUT()->getClientDetails(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID);

        $this->assertIsArray($result);
        $this->assertArrayHasKey('client_id', $result);
        $this->assertEquals(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID, $result['client_id']);
        $this->assertArrayHasKey('client_secret', $result);
        $this->assertTrue(password_verify(OAuthDTOInterface::CLIENT_SECRET_VALID, $result['client_secret']));
        $this->assertArrayHasKey('redirect_uri', $result);
        $this->assertNotNull($result['redirect_uri']);
    }

    /**
     *
     */
    public function testGetClientDetailsForInvalidClient()
    {
        $this->assertFalse($this->newSUT()->getClientDetails(OAuthDTOInterface::CLIENT_IDENTIFIER_MISSING));
    }

    /**
     *
     */
    public function testCheckClientCredentialsForValidClient()
    {
        $this->assertTrue(
            $this->newSUT()->checkClientCredentials(
                OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                OAuthDTOInterface::CLIENT_SECRET_VALID
            )
        );
    }

    /**
     *
     */
    public function testCheckClientCredentialsForValidClientNonVerifiedSecret()
    {
        $this->assertFalse(
            $this->newSUT()->checkClientCredentials(
                OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                'nonVerifiedSecret'
            )
        );
    }

    /**
     *
     */
    public function testCheckClientCredentialsForInvalidClient()
    {
        $this->assertFalse($this->newSUT()->checkClientCredentials(OAuthDTOInterface::CLIENT_IDENTIFIER_MISSING));
    }

    /**
     *
     */
    public function testCheckRestrictedGrantType()
    {
        $this->assertTrue($this->newSUT()->checkRestrictedGrantType(OAuthDTOInterface::CLIENT_ID_VALID, 'shrug'));
    }

    /**
     *
     */
    public function testIsPublicClient()
    {
        $this->assertFalse($this->newSUT()->isPublicClient(OAuthDTOInterface::CLIENT_ID_VALID));
    }

    /**
     *
     */
    public function testGetClientScope()
    {
        $this->assertNull($this->newSUT()->getClientScope(OAuthDTOInterface::CLIENT_ID_VALID));
    }

    /**
     * @return OAuthClientRepository
     */
    private function newSUT(): OAuthClientRepository
    {
        return new OAuthClientRepository(self::$em, self::$em->getClassMetadata(OAuthClientEntity::class));
    }
}
