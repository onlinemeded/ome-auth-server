<?php
/**
 * OAuthUserEntityTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Infrastructure\Entity\OAuth;

use UnitTest\BaseTestObject;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Domain\Auth\Infrastructure\Entity\OAuthUserEntity;

/**
 * Class OAuthUserEntityTest
 */
class OAuthUserEntityTest extends BaseTestObject
{
    /**
     *
     */
    public function testVerifyPassword()
    {
        $sut = new OAuthUserEntity;
        $password = OAuthDTOInterface::USER_PASSWORD_VALID;
        $sut->setPassword($password);

        $this->assertTrue($sut->verifyPassword($password));
        $this->assertTrue(password_verify($password, $sut->getPassword()));
    }
}
