<?php
/**
 * OAuthAccessTokenRepositoryTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Infrastructure\Entity\OAuth;

use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use UnitTest\Infrastructure\Entity\EntityBackedTestObject;
use Domain\Auth\Infrastructure\Entity\OAuthAccessTokenEntity;
use Domain\Auth\Infrastructure\Entity\OAuthAccessTokenRepository;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use OAuth2\ResponseType\AccessToken;
use DateInterval;
use DateTime;
use ReflectionClass;
use ReflectionException;

/**
 * Class OAuthAccessTokenRepositoryTest
 */
class OAuthAccessTokenRepositoryTest extends EntityBackedTestObject
{
    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ReflectionException
     */
    public function testSetAccessToken(): void
    {
        $reflectedAccessToken = new ReflectionClass(AccessToken::class);
        $generateAccessToken = $reflectedAccessToken->getMethod('generateAccessToken');
        $generateAccessToken->setAccessible(true);
        $oAuthToken = $generateAccessToken->invoke($reflectedAccessToken->newInstanceWithoutConstructor());
        $expires = (new DateTime)->add(new DateInterval('P1D'));

        $sut = $this->newSUT();
        $sut->setAccessToken(
            $oAuthToken,
            OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
            OAuthDTOInterface::USER_EMAIL_VALID,
            $expires->getTimestamp()
        );

        $accessTokenEntity = $sut->findOneBy(['token' => $oAuthToken]);

        $this->assertInstanceOf(OAuthAccessTokenEntity::class, $accessTokenEntity);

        self::$entities[] = $accessTokenEntity;
    }

    /**
     *
     */
    public function testGetAccessToken(): void
    {
        /** @var OAuthAccessTokenEntity $accessTokenEntity */
        $accessTokenEntity = array_pop(self::$entities);

        $result = $this->newSUT()->getAccessToken($accessTokenEntity->getToken());

        $this->assertIsArray($result);
        $this->assertArrayHasKey('token', $result);
        $this->assertEquals($accessTokenEntity->getToken(), $result['token']);
        $this->assertArrayHasKey('client_id', $result);
        $this->assertEquals($accessTokenEntity->getClient()->getClientIdentifier(), $result['client_id']);
        $this->assertArrayHasKey('user_id', $result);
        $this->assertEquals($accessTokenEntity->getUser()->getId(), $result['user_id']);
        $this->assertArrayHasKey('expires', $result);
        $this->assertEquals($accessTokenEntity->getExpires()->getTimestamp(), $result['expires']);
        $this->assertArrayHasKey('scope', $result);
        $this->assertEquals($accessTokenEntity->getScope(), $result['scope']);
    }

    /**
     *
     */
    public function testGetAccessTokenWithInvalidToken(): void
    {
        $this->assertNull($this->newSUT()->getAccessToken('invalidToken'));
    }

    /**
     * @return OAuthAccessTokenRepository
     */
    private function newSUT(): OAuthAccessTokenRepository
    {
        return new OAuthAccessTokenRepository(self::$em, self::$em->getClassMetadata(OAuthAccessTokenEntity::class));
    }
}
