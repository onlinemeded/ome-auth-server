<?php
/**
 * OAuthAuthorizationCodeRepositoryTest.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace UnitTest\Domain\Auth\Infrastructure\Entity\OAuth;

use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use UnitTest\Infrastructure\Entity\EntityBackedTestObject;
use Domain\Auth\Infrastructure\Entity\OAuthAuthorizationCodeEntity;
use Domain\Auth\Infrastructure\Entity\OAuthAuthorizationCodeRepository;
use OAuth2\ResponseType\AuthorizationCode;
use DateInterval;
use DateTime;

/**
 * Class OAuthAuthorizationCodeRepositoryTest
 */
class OAuthAuthorizationCodeRepositoryTest extends EntityBackedTestObject
{
    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ReflectionException
     */
    public function testSetAuthorizationCode()
    {
        $expires = (new DateTime)->add(new DateInterval('P1D'));

        $reflectedAccessToken = new \ReflectionClass(AuthorizationCode::class);
        $generateAuthorizationCode = $reflectedAccessToken->getMethod('generateAuthorizationCode');
        $generateAuthorizationCode->setAccessible(true);
        $oAuthCode = $generateAuthorizationCode->invoke($reflectedAccessToken->newInstanceWithoutConstructor());

        $sut = $this->newSUT();
        $sut->setAuthorizationCode(
            $oAuthCode,
            OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
            OAuthDTOInterface::USER_EMAIL_VALID,
            OAuthDTOInterface::CLIENT_REDIRECT_URI_VALID,
            $expires->getTimestamp()
        );

        /** @var OAuthAuthorizationCodeEntity $authorizationCodeEntity */
        $authorizationCodeEntity = $sut->findOneBy(['code' => $oAuthCode]);

        $this->assertInstanceOf(OAuthAuthorizationCodeEntity::class, $authorizationCodeEntity);
        $this->assertEquals($oAuthCode, $authorizationCodeEntity->getCode());

        self::$entities[] = $authorizationCodeEntity;
    }

    /**
     *
     */
    public function testGetAuthorizationCode()
    {
        /** @var OAuthAuthorizationCodeEntity $authorizationCodeEntity */
        $authorizationCodeEntity = end(self::$entities);

        $result = $this->newSUT()->getAuthorizationCode($authorizationCodeEntity->getCode());

        $this->assertIsArray($result);
        $this->assertArrayHasKey('code', $result);
        $this->assertEquals($authorizationCodeEntity->getCode(), $result['code']);
        $this->assertArrayHasKey('client_id', $result);
        $this->assertEquals($authorizationCodeEntity->getClient()->getClientIdentifier(), $result['client_id']);
        $this->assertArrayHasKey('user_id', $result);
        $this->assertEquals($authorizationCodeEntity->getUser()->getId(), $result['user_id']);
        $this->assertArrayHasKey('expires', $result);
        $this->assertEquals($authorizationCodeEntity->getExpires()->getTimestamp(), $result['expires']);
        $this->assertArrayHasKey('scope', $result);
        $this->assertEquals($authorizationCodeEntity->getScope(), $result['scope']);
    }

    /**
     *
     */
    public function testGetAuthorizationCodeFromInvalidCode()
    {
        $this->assertNull($this->newSUT()->getAuthorizationCode(''));
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testExpireAuthorizationCode()
    {
        /** @var OAuthAuthorizationCodeEntity $authorizationCodeEntity */
        $authorizationCodeEntity = array_pop(self::$entities);
        $repo = $this->newSUT();

        $repo->expireAuthorizationCode($authorizationCodeEntity->getCode());

        $this->assertNull($repo->findOneBy(['code' => $authorizationCodeEntity->getCode()]));
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testExpireAuthorizationCodeWithInvalidCode()
    {
        $this->assertNull($this->newSUT()->expireAuthorizationCode('invalidCode'));
    }

    /**
     * @return OAuthAuthorizationCodeRepository
     */
    private function newSUT(): OAuthAuthorizationCodeRepository
    {
        return new OAuthAuthorizationCodeRepository(
            self::$em,
            self::$em->getClassMetadata(OAuthAuthorizationCodeEntity::class)
        );
    }
}
