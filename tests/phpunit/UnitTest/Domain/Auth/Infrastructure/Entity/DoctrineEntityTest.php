<?php
/**
 * DoctrineEntityTest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace UnitTest\Domain\Auth\Infrastructure\Entity;

use UnitTest\BaseTestObject;
use Infrastructure\Conversion\ArrayConvertibleInterface;
use Infrastructure\Entity\AbstractEntity;
use DateTime;
use Exception;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RecursiveRegexIterator;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use RegexIterator;

/**
 * Class DoctrineEntityTest
 */
class DoctrineEntityTest extends BaseTestObject
{
    /**
     * @throws ReflectionException
     */
    public function testAllEntities()
    {
        foreach (new RegexIterator(
                     new RecursiveIteratorIterator(
                         new RecursiveDirectoryIterator(
                             realpath(__DIR__ . '/../../../../../../../src/Domain/Auth/Infrastructure/Entity')
                         )
                     ),
                     '/^.+\.php$/i',
                     RecursiveRegexIterator::GET_MATCH
                 ) as $fileInfo) {

            $className = '\\' . str_replace(
                    '/',
                    '\\',
                    substr(substr($fileInfo[0], strpos($fileInfo[0], '/src/') + 5), 0, -4)
                );

            if (false !== strstr($className, 'Abstract') || 0 === preg_match('/Entity$/', $className)) {
                continue;
            }

            $entity = new $className;

            $this->assertPropertyGettersAndSetters($entity);

            if (true === $entity instanceof ArrayConvertibleInterface) {
                $this->assertArrayConvertible($entity);
            }
        }
    }

    /**
     * @param ArrayConvertibleInterface $entity
     */
    protected function assertArrayConvertible(ArrayConvertibleInterface $entity)
    {
        $this->assertIsArray($entity->toArray());
        $this->assertInstanceOf(get_class($entity), $entity::fromArray(['id' => 1]));
    }

    /**
     * @param AbstractEntity $entity
     *
     * @throws ReflectionException
     * @throws Exception
     */
    protected function assertPropertyGettersAndSetters(AbstractEntity $entity)
    {
        $reflectedEntity = new ReflectionClass($entity);
        $entityProperties = $reflectedEntity->getProperties();
        $reflectedInstance = $reflectedEntity->newInstanceArgs();

        // load encryption method
        $reflectedVerifyEncryptedFieldValue = $reflectedEntity->getMethod('verifyEncryptedFieldValue');
        $reflectedVerifyEncryptedFieldValue->setAccessible(true);

        foreach ($entityProperties as $property) {
            // This is a mixin for hashing passwords
            if ('hashOptions' === $property->name) {
                continue;
            }

            $propertySetter = 'set' . $this->makeCamelCase($property->name);

            if ('id' === $property->name) {
                $this->assertFalse(
                    method_exists($entity, $propertySetter),
                    sprintf('Id on entity %s should not have a setter', get_class($entity))
                );

                $id = rand(1000, 10000);
                $property->setAccessible(true);
                $property->setValue($reflectedInstance, $id);

                $this->assertEquals(
                    $id,
                    $reflectedEntity->getMethod(
                        'get' . $this->makeCamelCase($property->name)
                    )->invoke($reflectedInstance)
                );
            } else {
                $invokeArgs = [];
                $reflectedSetMethod = $reflectedEntity->getMethod($propertySetter);
                $tags = $this->getMethodParameterTypesFromDocBlock($reflectedSetMethod);

                foreach ($reflectedSetMethod->getParameters() as $parameter) {
                    $parameterName = '$' . $parameter->name;
                    $parameterType = [$parameter->getType()];

                    if (true === array_key_exists($parameterName, $tags['@param'])) {
                        $parameterType = $tags['@param'][$parameterName];
                    }

                    $invokeArgs[] = $this->getNewArgumentFromType($parameterType[0]);
                }

                $reflectedSetMethod->invokeArgs($reflectedInstance, $invokeArgs);
                $reflectedGetMethod = $reflectedEntity->getMethod('get' . $this->makeCamelCase($property->name));

                $result = $reflectedGetMethod->invoke($reflectedInstance);

                foreach ($invokeArgs as $arg) {
                    if (true === in_array($property->name, ['password', 'clientSecret'])) {
                        $this->assertTrue(
                            $reflectedVerifyEncryptedFieldValue->invoke($reflectedInstance, $result, $arg)
                        );
                    } else {
                        $this->assertEquals(
                            $arg,
                            $result,
                            sprintf('The getter return for %s did not equal it\'s setter', $property->name)
                        );

                        // pass back to the "real" entity
                        $entity->{$propertySetter}($arg);
                    }
                }
            }
        }
    }

    /**
     * @param ReflectionMethod $reflectionMethod
     *
     * @return array
     */
    protected function getMethodParameterTypesFromDocBlock(ReflectionMethod $reflectionMethod): array
    {
        $returnTag = '@return';
        $tags = [
            '@param'   => [],
            $returnTag => null
        ];

        preg_match_all(
            "/(@[a-zA-Z]+)\s+([^ ]+)(?:\s+([a-zA-Z\\\\$]+)$)?/m",
            $reflectionMethod->getDocComment(),
            $matches
        );

        foreach ($matches[1] as $i => $match) {
            if ($returnTag === $match) {
                $tags[$returnTag] = $matches[2][$i];
            } else {
                $tags[$match][$matches[3][$i]] = explode('|', $matches[2][$i]);
            }
        }

        return $tags;
    }

    /**
     * @param string $type
     *
     * @return DateTime|int|string|null
     * @throws Exception
     */
    protected function getNewArgumentFromType(string $type)
    {
        if (true === in_array($type, ['string', 'int', 'DateTime'])) {
            switch ($type) {
                case 'string':
                    return 'string';
                case 'int':
                    return rand(1, 1000);
                case 'DateTime':
                    return new DateTime;
                default:
                    return null;
            }
        }

        $className = '\\Domain\\Auth\\Infrastructure\\Entity\\' . $type;
        if (true === class_exists($className)) {
            $reflectedEntity = new $className;

            // setup id's for dummy relationships
            $reflectionClass = new ReflectionClass($reflectedEntity);
            $this->setReflectedValue($reflectionClass, $reflectedEntity, 'id', rand(1, 999));

            if (true === method_exists($reflectedEntity, 'setClientIdentifier')) {
                $this->setReflectedValue($reflectionClass, $reflectedEntity, 'clientIdentifier', 'clientIdentifier');
            }

            return $reflectedEntity;
        }

        return null;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    protected function makeCamelCase(string $value): string
    {
        return lcfirst(str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $value))));
    }

    /**
     * @param ReflectionClass $reflector
     * @param AbstractEntity  $entity
     * @param string          $property
     * @param mixed           $value
     *
     * @throws ReflectionException
     */
    private function setReflectedValue(
        ReflectionClass $reflector,
        AbstractEntity $entity,
        string $property,
        $value
    ): void {
        $id = $reflector->getProperty($property);
        $id->setAccessible(true);
        $id->setValue($entity, $value);
        $id->setAccessible(false);
    }
}
