<?php
/**
 * HealthControllerTest.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace UnitTest\Domain\Platform\Controller;

use UnitTest\Application\Http\Controller\AbstractControllerTestObject;
use Domain\Platform\Controller\HealthController;

/**
 * Class HealthControllerTest
 */
class HealthControllerTest extends AbstractControllerTestObject
{
    /**\
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function testHeartbeatAction()
    {
        $sut = $this->newSUT('heartbeat');

        $this->assertNull($sut->heartbeat());
    }

    /**
     * @param string $action
     *
     * @return \Domain\Platform\Controller\HealthController
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    private function newSUT(string $action = 'test'): HealthController
    {
        return $this->createController(HealthController::class, $action);
    }
}
