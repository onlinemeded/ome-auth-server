<?php
/**
 * routes.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */
return [
    'get@/test' => [
        'controller' => 'UnitTest\\Application\\Http\\Controller\\TestController',
        'action'     => 'test',
        'feature'    => 'test'
    ]
];
