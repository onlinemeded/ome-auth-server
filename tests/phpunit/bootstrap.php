<?php
/**
 * bootstrap.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */
// defines
define('BASE_DIR', __DIR__ . '/../../');

require BASE_DIR . 'vendor/autoload.php';

use UnitTest\Application\ApplicationFactory;
use Application\Http\Application;

$container = ApplicationFactory::getContainerStaticProxy();

(new Application($container));
