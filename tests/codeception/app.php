<?php
/**
 * app.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */

use Application\Http\ApplicationInterface;
use Monolog\Logger;

$originalConfig = require BASE_DIR . '/config/app.php';

// Add custom routes for error testing
$originalConfig['logging'] = array_merge(
    $originalConfig['logging'],
    [
        'name'     => 'system',
        'handlers' => [
            'Monolog\\Handler\\StreamHandler' => [
                BASE_DIR . 'storage/logs/test_system.log',
                Logger::ERROR
            ]
        ]
    ]
);

// Add custom routes for error testing
$originalConfig['routing']['routes'] = array_merge(
    $originalConfig['routing']['routes'],
    [
        'get@/notAllowed'    => [
            'controller' => 'Application\\Http\\Controller\\ErrorController',
            'action'     => 'notAllowed',
            'feature'    => 'test'
        ],
        'get@/missingAction' => [
            'controller' => 'Application\\Http\\Controller\\ErrorController',
            'action'     => 'index',
            'feature'    => 'test'
        ]
    ]
);

// Use testing db info
$testConfig = require BASE_DIR . '/tests/phpunit/app.php';
$originalConfig['database'] = array_merge($originalConfig['database'], $testConfig['database']);

return array_merge($originalConfig, ['env' => ApplicationInterface::MODE_PRODUCTION]);
