<?php
/**
 * PlatformCest.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

use Codeception\Util\HttpCode;

/**
 * Class PlatformCest
 */
class PlatformCest
{
    /**
     * @param ApiTester $I
     */
    public function testHeartbeat(ApiTester $I): void
    {
        $I->sendGET('/v1/heartbeat');
        $I->seeResponseCodeIs(HttpCode::NO_CONTENT);
    }
}
