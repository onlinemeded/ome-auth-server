<?php
/**
 * AccessTokenGrantCest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Application\Http\StatusCode;
use Codeception\Util\HttpCode;

/**
 * Class AccessTokenGrantCest
 */
class AccessTokenGrantCest
{
    /**
     * @param ApiTester $I
     */
    public function testClientCredentialsTokenAuthentication(ApiTester $I): void
    {
        $I->haveHttpHeader(
            'Authorization',
            $I->generateAuthHeader(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID, OAuthDTOInterface::CLIENT_SECRET_VALID)
        );
        $I->sendPOST('/v1/sso/token', ['grant_type' => 'client_credentials']);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
    }

    /**
     * @param ApiTester $I
     */
    public function testClientCredentialsTokenAuthenticationInvalidCredentials(ApiTester $I): void
    {
        $I->haveHttpHeader(
            'Authorization',
            $I->generateAuthHeader(OAuthDTOInterface::CLIENT_IDENTIFIER_VALID, 'invalidSecret')
        );
        $I->sendPOST('/v1/sso/token', ['grant_type' => 'client_credentials']);
        $I->seeResponseCodeIs(HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(
            [
                'msg' => StatusCode::getReasonPhrase(StatusCode::HTTP_FORBIDDEN)
            ]
        );
    }
}
