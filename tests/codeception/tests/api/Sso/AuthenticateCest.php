<?php
/**
 * AuthenticateCest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Codeception\Util\HttpCode;

/**
 * Class AuthenticateCest
 */
class AuthenticateCest
{
    /**
     * @param ApiTester $I
     */
    public function testUserAuthentication(ApiTester $I): void
    {
        $I->sendPOST(
            '/v1/sso/authenticate',
            [
                'username' => OAuthDTOInterface::USER_EMAIL_VALID,
                'password' => OAuthDTOInterface::USER_PASSWORD_VALID
            ]
        );
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => true]);
    }
}
