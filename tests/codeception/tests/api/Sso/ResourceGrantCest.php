<?php
/**
 * ResourceGrantCest.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Codeception\Util\HttpCode;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;

/**
 * Class ResourceGrantCest
 */
class ResourceGrantCest
{
    /**
     * @param ApiTester $I
     */
    public function testResourceAuth(ApiTester $I): void
    {
        $this->getValidAccessToken($I);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();

        $response = json_decode($I->grabResponse(), true);

        $I->deleteHeader('Authorization');
        $I->sendPOST('/v1/sso/resource', ['access_token' => $response['access_token']]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => true]);
    }

    /**
     * @param ApiTester $I
     */
    public function testResourceAuthInvalidTokenError(ApiTester $I): void
    {
        $I->sendPOST('/v1/sso/resource', ['access_token' => 'expiredToken']);
        $I->seeResponseCodeIs(HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['msg' => 'Forbidden']);
    }

    /**
     * @param ApiTester $I
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ReflectionException
     */
    public function testResourceAuthExpiredTokenError(ApiTester $I): void
    {
        $I->sendPOST('/v1/sso/resource', ['access_token' => $I->getExpiredToken()->getToken()]);
        $I->seeResponseCodeIs(HttpCode::FORBIDDEN);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['msg' => 'Forbidden']);
    }

    /**
     * @param ApiTester $I
     */
    private function getValidAccessToken(ApiTester $I): void
    {
        $I->haveHttpHeader(
            'Authorization',
            $I->generateAuthHeader(
                OAuthDTOInterface::CLIENT_IDENTIFIER_VALID,
                OAuthDTOInterface::CLIENT_SECRET_VALID
            )
        );

        $I->sendPOST('/v1/sso/token', ['grant_type' => 'client_credentials']);
    }
}
