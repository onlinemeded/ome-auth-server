<?php
/**
 * ErrorControllerCest.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

use Codeception\Util\HttpCode;

/**
 * Class ErrorControllerCest
 */
class ErrorControllerCest
{
    /**
     * @param ApiTester $I
     */
    public function verifyNotFoundHandler(ApiTester $I): void
    {
        $I->sendGET('/');
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['msg' => 'Not Found']);
    }

    /**
     * @param ApiTester $I
     */
    public function verifyNotAllowedHandler(ApiTester $I): void
    {
        $I->sendPOST('/notAllowed');
        $I->seeResponseCodeIs(HttpCode::METHOD_NOT_ALLOWED);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['msg' => 'Method Not Allowed']);
    }

    /**
     * @param ApiTester $I
     */
    public function verifyMissingActionHandler(ApiTester $I): void
    {
        $I->sendGET('/missingAction');
        $I->seeResponseCodeIs(HttpCode::INTERNAL_SERVER_ERROR);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['msg' => 'Internal Server Error']);
    }
}
