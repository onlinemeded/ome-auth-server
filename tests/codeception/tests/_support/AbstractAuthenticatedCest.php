<?php
/**
 * AbstractAuthenticatedCest.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

/**
 * Class AbstractAuthenticatedCest
 *
 * @method mixed sendHEAD($I, $url, $params = null)
 * @method mixed sendGET($I, $url, $params = null)
 * @method mixed sendOPTIONS($url, $params = null)
 * @method mixed sendPOST($I, $url, $params = null, $files = null)
 * @method mixed sendPUT($I, $url, $params = null, $files = null)
 * @method mixed sendPATCH($I, $url, $params = null, $files = null)
 * @method mixed sendDELETE($I, $url, $params = null, $files = null)
 * @method mixed sendLINK($I, $url, $linkEntries)
 * @method mixed sendUNLINK($I, $url, $linkEntries)
 */
abstract class AbstractAuthenticatedCest
{
    const API_KEY = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
    const IDENTITY = ['id' => 1, 'email' => 'developer@onlinemeded.org'];

    /**
     * @var \Helper\Api
     */
    private $apiHelper;

    /**
     * @param \Helper\Api $apiHelper
     */
    public function _inject(\Helper\Api $apiHelper)
    {
        $this->apiHelper = $apiHelper;
    }

    /**
     * @param string $name
     * @param mixed  $arguments
     *
     * @return mixed
     */
    public function __call(string $name, $arguments)
    {
        if ('send' === substr($name, 0, 4)) {
            /** @var ApiTester $I */
            $I = array_shift($arguments);
            $I->haveHttpHeader('Authorization', 'Bearer ' . $this->generateToken());


            return call_user_func_array([$I, $name], $arguments);
        }

        throw new \BadMethodCallException(sprintf('The methhod "%s" could not be found', $name));
    }

    /**
     * @return string
     */
    protected function generateToken(): string
    {
        return (new \Application\Infrastructure\Crypto\Cryptographer\AesHmacCryptographer(self::API_KEY))->encrypt(
            json_encode(self::IDENTITY)
        );
    }
}
