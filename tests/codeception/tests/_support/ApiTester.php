<?php

use Codeception\Actor;
use Codeception\Scenario;
use UnitTest\Domain\Auth\Fixtures\OAuthDTOInterface;
use Domain\Auth\Infrastructure\Entity\OAuthAccessTokenEntity;
use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;
use Infrastructure\Configuration\ConfigurationManager;
use Infrastructure\Entity\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\Tools\Setup;
use OAuth2\ResponseType\AccessToken;


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
 */
class ApiTester extends Actor
{
    use _generated\ApiTesterActions;

    /**
     * @var OAuthAccessTokenEntity
     */
    private $expiredToken;

    /**
     * @param Scenario $scenario
     */
    public function __construct(Scenario $scenario)
    {
        parent::__construct($scenario);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __destruct()
    {
        if (null !== $this->expiredToken) {
            $em = $this->getEntityManager();
            $em->remove($this->expiredToken);
            $em->flush($this->expiredToken);
        }
    }

    /**
     * @param string $id
     * @param string $secret
     * @param string $type
     *
     * @return string
     */
    public function generateAuthHeader(string $id, string $secret, string $type = 'Basic'): string
    {
        return $type . ' ' . base64_encode($id . ':' . $secret);
    }

    /**
     * @return OAuthAccessTokenEntity
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ReflectionException
     */
    public function getExpiredToken(): OAuthAccessTokenEntity
    {
        if (null === $this->expiredToken) {
            $em = $this->getEntityManager();
            $reflectedAccessToken = new \ReflectionClass(AccessToken::class);
            $generateAccessToken = $reflectedAccessToken->getMethod('generateAccessToken');
            $generateAccessToken->setAccessible(true);

            $date = new DateTime;
            $date->sub(new DateInterval('P1D'));
            $accessToken = new OAuthAccessTokenEntity;
            $accessToken->setClient(
                $em->getRepository(OAuthClientEntity::class)->findOneBy(
                    ['clientIdentifier' => OAuthDTOInterface::CLIENT_IDENTIFIER_VALID]
                )
            );
            $accessToken->setExpires($date);
            $accessToken->setToken(
                $generateAccessToken->invoke($reflectedAccessToken->newInstanceWithoutConstructor())
            );

            $em->persist($accessToken);
            $em->flush();

            $this->expiredToken = $accessToken;
        }

        return $this->expiredToken;
    }

    /**
     * @return EntityManager
     * @throws ORMException
     */
    private function getEntityManager(): EntityManager
    {
        static $instance = false;

        if (false === $instance) {
            if (false === defined('BASE_DIR')) {
                define('BASE_DIR', __DIR__ . '/../../../../');
            }

            $configManager = new ConfigurationManager;
            $configManager->addConfigurationFromArray(
                array_merge(
                    require __DIR__ . '/../../app.php',
                    [
                        'features' => [
                            'test' => [
                                'enabled' => true
                            ]
                        ]
                    ]
                )
            );
            $config = $configManager->getDirective('database');

            // Metadata
            $doctrineConfig = Setup::createAnnotationMetadataConfiguration(
                $config['path']['metadata'],
                $config['devMode'],
                $config['connection']['default']['proxy']['path'],
                new $config['cache']['metadata'],
                false
            );

            // Cache
            $doctrineConfig->setQueryCacheImpl(new $config['cache']['query']);
            $doctrineConfig->setResultCacheImpl(new $config['cache']['result']);

            // Proxies
            $doctrineConfig->setAutoGenerateProxyClasses(false);
            $doctrineConfig->setProxyNamespace($config['connection']['default']['proxy']['namespace']);

            $instance = EntityManager::create($config['connection']['default'], $doctrineConfig);
        }

        return $instance;
    }

}
