<?php
/**
 * bootstrap.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */
// defines
define('BASE_DIR', __DIR__ . '/../../');

require BASE_DIR . 'vendor/autoload.php';

use Application\Http\Application;
use Application\Http\ApplicationInterface;
use Domain\Auth\Factory\RepositoryFactory;
use Domain\Auth\Factory\UseCaseFactory;
use Domain\Auth\Infrastructure\Entity\OAuthAccessTokenEntity;
use Domain\Auth\Infrastructure\Entity\OAuthAuthorizationCodeEntity;
use Domain\Auth\Infrastructure\Entity\OAuthAuthorizationCodeRepository;
use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;
use Domain\Auth\Infrastructure\Entity\OAuthClientRepository;
use Domain\Auth\Infrastructure\Entity\OAuthRefreshTokenEntity;
use Domain\Auth\Infrastructure\Entity\OAuthRefreshTokenRepository;
use Domain\Auth\Infrastructure\Entity\OAuthUserEntity;
use Infrastructure\Configuration\ConfigurationManager;
use Infrastructure\DI\Container;
use Infrastructure\Entity\EntityManager;
use Infrastructure\Feature\FeatureManager;
use Infrastructure\Log\LoggingManager;
use Doctrine\ORM\Tools\Setup;
use OAuth2\GrantType\ClientCredentials;
use OAuth2\GrantType\AuthorizationCode;
use OAuth2\GrantType\RefreshToken;
use OAuth2\Server as OAuthServer;
use Opensoft\Rollout\Storage\ArrayStorage;

$container = new Container(
    [
        ConfigurationManager::class            => function () {
            $configManager = new ConfigurationManager;
            $configManager->addConfigurationFromArray(
                array_merge(
                    require __DIR__ . '/app.php',
                    [
                        'features' => [
                            'test' => [
                                'enabled' => true
                            ]
                        ]
                    ]
                )
            );

            return $configManager;
        },
        LoggingManager::class                  => function (Container $container) {
            $config = $container->offsetGet(ConfigurationManager::class)->getDirective('logging');

            return LoggingManager::create($config['name'], $config['handlers']);
        },
        EntityManager::class                   => function (Container $container) {
            $config = $container->offsetGet(ConfigurationManager::class)->getDirective('database');

            // Metadata
            $doctrineConfig = Setup::createAnnotationMetadataConfiguration(
                $config['path']['metadata'],
                $config['devMode'],
                $config['connection']['default']['proxy']['path'],
                new $config['cache']['metadata'],
                false
            );

            // Cache
            $doctrineConfig->setQueryCacheImpl(new $config['cache']['query']);
            $doctrineConfig->setResultCacheImpl(new $config['cache']['result']);

            // Proxies
            $doctrineConfig->setAutoGenerateProxyClasses(false);
            $doctrineConfig->setProxyNamespace($config['connection']['default']['proxy']['namespace']);

            return EntityManager::create($config['connection']['default'], $doctrineConfig);
        },
        FeatureManager::class                  => function (Container $container) {
            $featureManager = FeatureManager::create(new ArrayStorage);
            $config = $container->offsetGet(ConfigurationManager::class)->getDirective('features');

            $featureManager->defineGroup(
                'all',
                function () {
                    return true;
                }
            );

            $featureManager->activate('core');

            foreach ($config as $feature => $settings) {
                if (true === $settings['enabled']) {
                    $featureManager->activate($feature);
                    continue;
                }

                if (true === array_key_exists('acl', $settings)) {
                    foreach (['group', 'user'] as $permissionLevel) {
                        if (true === array_key_exists($permissionLevel, $settings['acl'])) {
                            foreach ($settings['acl'][$permissionLevel] as $level) {
                                $featureManager->{'activate' . ucfirst($permissionLevel)}($feature, $level);
                            }
                        }
                    }
                }
            }

            return $featureManager;
        },
        ApplicationInterface::APP_OAUTH_SERVER => function (Container $container) {
            $config = $container->offsetGet(ConfigurationManager::class)->getDirective('middleware')['oauth2'];

            /**
             * @var EntityManager                    $entityManager
             * @var OAuthClientRepository            $clientStorage
             * @var OAuthAuthorizationCodeRepository $authorizationCodeStorage
             * @var OAuthRefreshTokenRepository      $refreshTokenStorage
             */
            $entityManager = $container->offsetGet(ApplicationInterface::APP_ENTITY_MANAGER);
            $clientStorage = $entityManager->getRepository(OAuthClientEntity::class);
            $userStorage = $entityManager->getRepository(OAuthUserEntity::class);
            $authorizationCodeStorage = $entityManager->getRepository(OAuthAuthorizationCodeEntity::class);
            $refreshTokenStorage = $entityManager->getRepository(OAuthRefreshTokenEntity::class);

            return new OAuthServer(
                [
                    'client_credentials' => $clientStorage,
                    'user_credentials'   => $userStorage,
                    'access_token'       => $entityManager->getRepository(OAuthAccessTokenEntity::class),
                    'authorization_code' => $authorizationCodeStorage,
                    'refresh_token'      => $refreshTokenStorage
                ],
                $config,
                [
                    new ClientCredentials($clientStorage),
                    new AuthorizationCode($authorizationCodeStorage),
                    new RefreshToken($refreshTokenStorage)
                ]
            );
        },
        RepositoryFactory::class               => function (Container $container) {
            return new RepositoryFactory(
                $container->offsetGet(ApplicationInterface::APP_ENTITY_MANAGER),
                $container->offsetGet(ApplicationInterface::APP_OAUTH_SERVER)
            );
        },
        UseCaseFactory::class                  => function (Container $container) {
            return new UseCaseFactory($container->offsetGet(RepositoryFactory::class));
        }
    ]
);
(new Application($container))->run();
