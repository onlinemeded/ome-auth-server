<?php
/**
 * index.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
define('BASE_DIR', realpath(__DIR__ . '/../') . '/');

require BASE_DIR . 'vendor/autoload.php';

use Application\Http\Application;
use Application\Http\ApplicationInterface;
use Application\Http\Request\Middleware\FeatureCheckMiddleware;
use Application\Http\Request\Middleware\JwtAuthenticationMiddleware;
use Application\Http\Request\Middleware\SignedRequestMiddleware;
use Application\Http\StatusCode;
use Domain\Auth\Factory\RepositoryFactory;
use Domain\Auth\Factory\UseCaseFactory;
use Domain\Auth\Infrastructure\Entity\OAuthAccessTokenEntity;
use Domain\Auth\Infrastructure\Entity\ApiKeyEntity;
use Domain\Auth\Infrastructure\Entity\OAuthAuthorizationCodeEntity;
use Domain\Auth\Infrastructure\Entity\OAuthAuthorizationCodeRepository;
use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;
use Domain\Auth\Infrastructure\Entity\OAuthClientRepository;
use Domain\Auth\Infrastructure\Entity\OAuthRefreshTokenEntity;
use Domain\Auth\Infrastructure\Entity\OAuthRefreshTokenRepository;
use Domain\Auth\Infrastructure\Entity\OAuthUserEntity;
use Domain\Auth\Infrastructure\Entity\OAuthUserRepository;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;
use Infrastructure\Configuration\ConfigurationManager;
use Infrastructure\Crypto\Cryptographer\AesHmacCryptographer;
use Infrastructure\DI\Container;
use Infrastructure\Entity\EntityManager;
use Infrastructure\Feature\FeatureManager;
use Infrastructure\Log\LoggingManager;
use Doctrine\ORM\Tools\Setup;
use OAuth2\GrantType\ClientCredentials;
use OAuth2\GrantType\AuthorizationCode;
use OAuth2\GrantType\RefreshToken;
use OAuth2\GrantType\UserCredentials;
use OAuth2\Server as OAuthServer;
use Opensoft\Rollout\Storage\ArrayStorage;
use Slim\Http\Request;
use Tuupola\Middleware\CorsMiddleware;

try {
    $container = new Container(
        [
            ApplicationInterface::APP_CONFIG_MANAGER  => function () {
                $configManager = new ConfigurationManager;
                $configManager->addConfigurationFromArray(require BASE_DIR . 'config/app.php');

                return $configManager;
            },
            ApplicationInterface::APP_LOGGING_MANAGER => function (Container $container) {
                $config = $container->offsetGet(ApplicationInterface::APP_CONFIG_MANAGER)->getDirective('logging');

                return LoggingManager::create($config['name'], $config['handlers']);
            },
            ApplicationInterface::APP_ENTITY_MANAGER  => function (Container $container) {
                $config = $container->offsetGet(ApplicationInterface::APP_CONFIG_MANAGER)->getDirective('database');

                // Metadata
                $doctrineConfig = Setup::createAnnotationMetadataConfiguration(
                    $config['path']['metadata'],
                    $config['devMode'],
                    $config['connection']['default']['proxy']['path'],
                    new $config['cache']['metadata'],
                    false
                );

                // Cache
                $doctrineConfig->setQueryCacheImpl(new $config['cache']['query']);
                $doctrineConfig->setResultCacheImpl(new $config['cache']['result']);

                // Proxies
                $doctrineConfig->setAutoGenerateProxyClasses(false);
                $doctrineConfig->setProxyNamespace($config['connection']['default']['proxy']['namespace']);

                return EntityManager::create($config['connection']['default'], $doctrineConfig);
            },
            ApplicationInterface::APP_OAUTH_SERVER    => function (Container $container) {
                $config = $container->offsetGet(ConfigurationManager::class)->getDirective('middleware')['oauth2'];

                /**
                 * @var EntityManager                    $entityManager
                 * @var OAuthClientRepository            $clientStorage
                 * @var OAuthUserRepository              $userStorage
                 * @var OAuthAuthorizationCodeRepository $authorizationCodeStorage
                 * @var OAuthRefreshTokenRepository      $refreshTokenStorage
                 */
                $entityManager = $container->offsetGet(ApplicationInterface::APP_ENTITY_MANAGER);
                $clientStorage = $entityManager->getRepository(OAuthClientEntity::class);
                $userStorage = $entityManager->getRepository(OAuthUserEntity::class);
                $authorizationCodeStorage = $entityManager->getRepository(OAuthAuthorizationCodeEntity::class);
                $refreshTokenStorage = $entityManager->getRepository(OAuthRefreshTokenEntity::class);

                return new OAuthServer(
                    [
                        'client_credentials' => $clientStorage,
                        'user_credentials'   => $userStorage,
                        'access_token'       => $entityManager->getRepository(OAuthAccessTokenEntity::class),
                        'authorization_code' => $authorizationCodeStorage,
                        'refresh_token'      => $refreshTokenStorage
                    ],
                    $config,
                    [
                        new UserCredentials($userStorage),
                        new ClientCredentials($clientStorage),
                        new AuthorizationCode($authorizationCodeStorage),
                        new RefreshToken($refreshTokenStorage)
                    ]
                );
            },
            RepositoryFactory::class                  => function (Container $container) {
                return new RepositoryFactory(
                    $container->offsetGet(ApplicationInterface::APP_ENTITY_MANAGER),
                    $container->offsetGet(ApplicationInterface::APP_OAUTH_SERVER)
                );
            },
            UseCaseFactory::class                     => function (Container $container) {
                return new UseCaseFactory($container->offsetGet(RepositoryFactory::class));
            },
            ApplicationInterface::APP_FEATURE_MANAGER => function (Container $container) {
                $featureManager = FeatureManager::create(new ArrayStorage);
                $config = $container->offsetGet(ApplicationInterface::APP_CONFIG_MANAGER)->getDirective('features');

                $featureManager->defineGroup(
                    'all',
                    function () {
                        return true;
                    }
                );

                $featureManager->activate('core');

                foreach ($config as $feature => $settings) {
                    if (true === $settings['enabled']) {
                        $featureManager->activate($feature);
                        continue;
                    }

                    if (true === array_key_exists('acl', $settings)) {
                        foreach (['group', 'user'] as $permissionLevel) {
                            if (true === array_key_exists($permissionLevel, $settings['acl'])) {
                                foreach ($settings['acl'][$permissionLevel] as $level) {
                                    $featureManager->{'activate' . ucfirst($permissionLevel)}($feature, $level);
                                }
                            }
                        }
                    }
                }

                return $featureManager;
            }
        ]
    );

    $config = $container->offsetGet(ApplicationInterface::APP_CONFIG_MANAGER);
    $logger = $container->offsetGet(ApplicationInterface::APP_LOGGING_MANAGER);
    $entityManager = $container->offsetGet(ApplicationInterface::APP_ENTITY_MANAGER);
    $middleware = $config->getDirective('middleware');

    (new Application(
        $container,
        [
            new FeatureCheckMiddleware($container->offsetGet(ApplicationInterface::APP_FEATURE_MANAGER)),
            new JwtAuthenticationMiddleware(
                array_merge(
                    [
                        'logger' => $logger,
                        'error'  => function ($response, $arguments) {
                            throw new AuthenticationException($arguments['message'], StatusCode::HTTP_UNAUTHORIZED);
                        },
                        'before' => function (Request $request, $token) {
                            return $request->withAttribute(
                                'authToken',
                                (new AesHmacCryptographer(
                                    $request->getAttribute(SignedRequestMiddleware::ATTRIBUTE_ID)
                                ))->decrypt($token['decoded']['payload'])
                            );
                        }
                    ],
                    $middleware['jwt']
                )
            ),
            new SignedRequestMiddleware(
                array_merge(
                    [
                        'beforeValidation' => function ($apiKey) use ($entityManager) {
                            if (null ===
                                $entityManager->getRepository(ApiKeyEntity::class)->findOneBy(['value' => $apiKey])
                            ) {
                                throw new AuthenticationException('Invalid api key', StatusCode::HTTP_UNAUTHORIZED);
                            }
                        }
                    ],
                    $middleware['signedRequest']
                )
            ),
            new CorsMiddleware(
                array_merge(
                    [
                        'logger' => $logger,
                        'error'  => function ($request, $response, $arguments) {
                            throw new AuthenticationException($arguments['message'], StatusCode::HTTP_UNAUTHORIZED);
                        }
                    ],
                    $middleware['cors']
                )
            )
        ]
    ))->run();
} catch (Exception $e) {
    echo sprintf('Could not run web app. %s', $e->getMessage());
}
