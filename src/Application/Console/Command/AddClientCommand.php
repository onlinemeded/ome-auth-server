<?php
/**
 * AddClientCommand.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Console\Command;

use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;
use Domain\Auth\Infrastructure\Entity\OAuthClientRepository;
use Infrastructure\DI\Container;
use Infrastructure\Entity\EntityManager;
use Infrastructure\Exception\Runtime\InvalidArgumentException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * Class AddClientCommand
 */
class AddClientCommand extends AbstractCommand
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @param Container   $container
     * @param string|null $name
     */
    public function __construct(Container $container, ?string $name = null)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var EntityManager         $entityManager
         * @var OAuthClientRepository $clientRepo
         */
        $entityManager = $this->container->offsetGet(EntityManager::class);
        $clientRepo = $entityManager->getRepository(OAuthClientEntity::class);
        $clientIdentifier = $input->getArgument('identifier') ?? '';

        if (true === is_array($clientIdentifier)) {
            throw new InvalidArgumentException('Input clientIdentifier is an array');
        }

        $duplicate = $clientRepo->findOneBy(['clientIdentifier' => $clientIdentifier]);

        if (null !== $duplicate) {
            throw new InvalidArgumentException(
                sprintf('A client for identifier "%s" already exists', $clientIdentifier)
            );
        }

        $client = OAuthClientEntity::fromArray(
            [
                'clientIdentifier' => $clientIdentifier,
                'redirectUri'      => $input->getArgument('redirect-url'),
                'name'             => $input->getArgument('name'),
                'description'      => $input->getArgument('description')
            ]
        );

        $makeSecretQuestion = function (string $text): Question {
            $question = new Question($text);
            $question->setHidden(true);
            $question->setHiddenFallback(false);
            $question->setNormalizer(
                function (string $value) {
                    return false === empty($value)
                        ? trim($value)
                        : '';
                }
            );

            return $question;
        };

        /** @var QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');
        $secretQuestion = $makeSecretQuestion('Please enter client secret');
        $secretQuestionConfirmation = $makeSecretQuestion('Please re-enter client secret');
        $clientSecretAnswer = $questionHelper->ask($input, $output, $secretQuestion);
        $clientSecretConfirmationAnswer = $questionHelper->ask($input, $output, $secretQuestionConfirmation);

        if ($clientSecretAnswer !== $clientSecretConfirmationAnswer) {
            throw new InvalidArgumentException('Client secret does not match');
        }

        $client->setClientSecret($clientSecretAnswer);

        $entityManager->persist($client);
        $entityManager->flush($client);

        $output->writeln(PHP_EOL);
        $output->writeln($this->addComment('Added client'));
        (new Table($output))
            ->setRows(
                [
                    ['Id', $client->getId()],
                    ['Identifier', $clientIdentifier],
                    ['Name', $client->getName()],
                    ['Redirect Url', $client->getRedirectUri()],
                    ['Description', $client->getDescription()]
                ]
            )
            ->render();
        $output->writeln(PHP_EOL);
    }

    /**
     * @return void
     */
    public function configure(): void
    {
        $this->setName('client:add')
            ->setDescription('Add new client to application')
            ->setHelp('Creates and adds a new client to the application')
            ->addArgument('identifier', InputArgument::REQUIRED, 'Client identifier')
            ->addArgument('redirect-url', InputArgument::REQUIRED, 'Application re-direct url')
            ->addArgument('name', InputArgument::REQUIRED, 'Client name')
            ->addArgument('description', InputArgument::OPTIONAL, 'Client description');
    }
}
