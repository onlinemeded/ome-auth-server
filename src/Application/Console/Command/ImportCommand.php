<?php
/**
 * ImportCommand.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace Application\Console\Command;

use Domain\Auth\Infrastructure\Entity\OAuthUserEntity;
use Domain\Auth\Infrastructure\Entity\UserEntity;
use Infrastructure\DI\Container;
use Infrastructure\Entity\EntityManager;
use Infrastructure\Exception\Runtime\InvalidArgumentException;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ImportCommand
 */
class ImportCommand extends AbstractCommand
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var int
     */
    private $batchSize = 50;

    /**
     * @param Container   $container
     * @param string|null $name
     */
    public function __construct(Container $container, ?string $name = null)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    /**
     * @return void
     */
    public function configure(): void
    {
        $this->setName('import:file')
            ->setDescription('Convert files into DB records')
            ->setHelp('This command allows you to convert users files to DB records')
            ->addArgument('file', InputArgument::REQUIRED, 'The record input file');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws MappingException
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        /** @var EntityManager $entityManager */
        $entityManager = $this->container->offsetGet(EntityManager::class);
        $file = $input->getArgument('file') ?? '';

        if (true === is_array($file)) {
            throw new InvalidArgumentException('Input file is an array');
        }

        if (false === is_file($file)) {
            throw new InvalidArgumentException(sprintf('Could not locate input file: %s', $file));
        }

        $totalLines = intval(exec("wc -l '{$file}'"));
        $progressBar = new ProgressBar($output, $totalLines - 1);
        $i = 0;
        $cutoff = $totalLines + 1;
        $in = fopen('php://memory', 'r+');
        fputs($in, file_get_contents($file));
        rewind($in);
        fgetcsv($in);

        $output->writeln($this->addComment(sprintf('Attempting to add %d users', $totalLines - 1)));
        $progressBar->start();

        while ($i < $cutoff && false !== ($line = fgetcsv($in))) {
            $oAuthUserEntity = OAuthUserEntity::fromArray(
                [
                    'id'       => $line[0],
                    'email'    => $line[1],
                    'password' => $line[3]
                ]
            );

            $entityManager->persist($oAuthUserEntity);
            $entityManager->persist(
                UserEntity::fromArray(
                    [
                        'firstName' => $line[4],
                        'lastName'  => $line[5],
                        'oauthUser' => $oAuthUserEntity
                    ]
                )
            );

            if (0 === $i % $this->batchSize) {
                $entityManager->flush();
                $entityManager->clear();
            }

            $progressBar->advance();
            $i++;
        }

        $entityManager->flush();
        $entityManager->clear();
        $progressBar->finish();
        $output->writeln(PHP_EOL);
        fclose($in);
    }
}
