<?php
/**
 * AbstractCommand.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Console\Command;

use Symfony\Component\Console\Command\Command;

/**
 * Class AbstractCommand
 */
abstract class AbstractCommand extends Command
{
    /**
     * @param string $string
     *
     * @return string
     */
    protected function addComment(string $string): string
    {
        return $this->outputLine($string, 'comment');
    }

    /**
     * @param string $string
     * @param string $type
     * @param int    $indent
     *
     * @return string
     */
    protected function outputLine(string $string, string $type = 'info', $indent = 0): string
    {
        return sprintf('<%s>%s%s</%s>', $type, str_repeat(' ', $indent), $string, $type);
    }
}
