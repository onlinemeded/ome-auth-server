<?php
/**
 * SignedRequestMiddleware.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http\Request\Middleware;

use Application\Http\StatusCode;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;
use Infrastructure\Crypto\Cryptographer\CryptographerInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Environment;
use Slim\Http\Request;
use Tuupola\Middleware\JwtAuthentication\RequestPathRule;

/**
 * Class SignedRequestMiddleware
 */
class SignedRequestMiddleware
{
    const ATTRIBUTE_ID = 'apiKey';

    /**
     * @var array
     */
    private $options = [
        'attributeId' => self::ATTRIBUTE_ID,
        'headerName'  => '',
        'ignore'      => [],
        'timeToLive'  => 30
    ];

    /**
     * @var string
     */
    private $apiKey = '';

    /**
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->options = array_merge($this->options, $options);
    }

    /**
     * @param Request           $request
     * @param ResponseInterface $response
     * @param callable          $next
     *
     * @return ResponseInterface
     * @throws AuthenticationException
     */
    public function __invoke(Request $request, ResponseInterface $response, callable $next): ResponseInterface
    {
        if ('OPTIONS' === $request->getMethod()
            || false === (new RequestPathRule(['ignore' => (array)$this->options['ignore']]))->__invoke($request)
        ) {
            return $next($request, $response);
        }

        $header = $request->getHeader($this->options['headerName']);

        if (false === empty($header)) {
            if (true === $this->validateRequestSignature(
                new Environment($_SERVER),
                $header[0],
                $this->options['timeToLive']
            )) {
                return $next($request->withAttribute($this->options['attributeId'], $this->apiKey), $response);
            }
        }

        throw new AuthenticationException('Invalid request signature', StatusCode::HTTP_UNAUTHORIZED);
    }

    /**
     * @param Environment $environment
     * @param string      $signature
     * @param int         $timeToLive
     *
     * @return bool
     * @throws AuthenticationException
     */
    protected function validateRequestSignature(Environment $environment, string $signature, int $timeToLive): bool
    {
        $requestTime = strtotime($environment->get('HTTP_OME_DATE'));

        if (false === $requestTime) {
            throw new AuthenticationException('Could not parse date header', StatusCode::HTTP_UNAUTHORIZED);
        }

        $currentTime = time();

        if ($currentTime > $requestTime + $timeToLive || $currentTime < $requestTime - $timeToLive) {
            throw new AuthenticationException(
                sprintf(
                    'Timestamp is <> %s seconds. %s - %s',
                    $timeToLive,
                    date('D, d M Y H:i:s', $currentTime),
                    date('D, d M Y H:i:s', $requestTime)
                ),
                StatusCode::HTTP_UNAUTHORIZED
            );
        }

        $signatureParts = explode(';', $signature);

        if (2 !== count($signatureParts)) {
            throw new AuthenticationException('Invalid number of signature segments', StatusCode::HTTP_UNAUTHORIZED);
        }

        $this->apiKey = $signatureParts[0];

        if ((true === array_key_exists('beforeValidation', $this->options))
            && (true === is_callable($this->options['beforeValidation']))
        ) {
            $this->options['beforeValidation']($this->apiKey);
        }

        return (
            $this->generateRequestSignature(
                $this->apiKey,
                (string)$environment->get('HTTP_HOST'),
                (string)$environment->get('REQUEST_URI'),
                $requestTime
            ) === $signatureParts[1]
        );
    }

    /**
     * @param string $apiKey
     * @param string $host
     * @param string $uri
     * @param int    $time
     *
     * @return string
     */
    protected function generateRequestSignature(
        string $apiKey,
        string $host,
        string $uri,
        int $time
    ): string {
        return hash_hmac(
            CryptographerInterface::HMAC_ALGO_SHA_256,
            sprintf('%s:%s:%s GMT', $host, $uri, gmdate('D, d M Y H:i:s', $time)),
            $apiKey
        );
    }
}
