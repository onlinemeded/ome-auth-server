<?php
/**
 * FeatureCheckMiddleware.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http\Request\Middleware;

use Application\Http\StatusCode;
use Infrastructure\Feature\FeatureManager;
use Infrastructure\InfrastructureException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionException;
use ReflectionFunction;
use Slim\Route;

/**
 * Class FeatureCheckMiddleware
 */
class FeatureCheckMiddleware
{
    /**
     * @var FeatureManager
     */
    private $featureManager;

    /**
     * @param FeatureManager $featureManager
     */
    public function __construct(FeatureManager $featureManager)
    {
        $this->featureManager = $featureManager;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param callable               $next
     *
     * @return ResponseInterface
     * @throws InfrastructureException
     * @throws ReflectionException
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        $route = $request->getAttribute('route');

        if ((false === empty($route) && 'OPTIONS' !== $request->getMethod())
            && (false === $this->featureManager
                    ->isActive(
                        (new ReflectionFunction($route->getCallable()))->getStaticVariables()['routeDetails']['feature']
                    ))
        ) {
            throw new InfrastructureException('Feature unavailable', StatusCode::HTTP_NOT_FOUND);
        }

        return $next($request, $response);
    }
}
