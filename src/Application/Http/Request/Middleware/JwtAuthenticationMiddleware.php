<?php
/**
 * JwtAuthenticationMiddleware.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http\Request\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Tuupola\Middleware\CallableHandler;
use Tuupola\Middleware\JwtAuthentication;
use ReflectionException;
use ReflectionProperty;

/**
 * Class JwtAuthenticationMiddleware
 */
class JwtAuthenticationMiddleware implements MiddlewareInterface
{
    /**
     * @var JwtAuthentication
     */
    private $middleware;

    /**
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->middleware = new JwtAuthentication($options);
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     * @throws ReflectionException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $options = new ReflectionProperty($this->middleware, 'options');
        $options->setAccessible(true);
        $options->setValue(
            $this->middleware,
            array_merge(
                ['secret' => $request->getAttribute(SignedRequestMiddleware::ATTRIBUTE_ID)],
                $options->getValue($this->middleware)
            )
        );

        return $this->middleware->process($request, $handler);
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param callable               $next
     *
     * @return ResponseInterface
     * @throws ReflectionException
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        callable $next
    ): ResponseInterface {
        return $this->process($request, new CallableHandler($next, $response));
    }
}
