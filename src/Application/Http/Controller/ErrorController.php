<?php
/**
 * ErrorController.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http\Controller;

use Application\Http\Application;
use Application\Http\ApplicationInterface;
use Application\Http\StatusCode;
use Infrastructure\Exception\Runtime\InvalidArgumentException;
use Infrastructure\Exception\RuntimeException;
use Slim\Http\Request;
use Slim\Http\Response;
use Exception;

/**
 * Class ErrorController
 */
class ErrorController extends AbstractController
{
    /**
     * @var array
     */
    private $actionList = [
        'error',
        'phpError',
        'notFound',
        'notAllowed'
    ];

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $arguments
     *
     * @return Response
     */
    final public function dispatch(Request $request, Response $response, array $arguments): Response
    {
        $this->setParams(array_merge((array)$request->getParams(), $arguments));

        return $response->withJson(
            call_user_func_array([$this, $this->getAction()], [$request, $response]),
            $this->getHttpResponseCode()
        );
    }

    /**
     * @param string $name
     * @param mixed  $arguments
     *
     * @return mixed
     */
    public function __call(string $name, $arguments)
    {
        if (false === in_array($name, $this->actionList)) {
            throw new InvalidArgumentException(
                sprintf('No action "%s" found for "%s"', $name, static::class),
                StatusCode::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        /**
         * @var Exception|array|null $exception
         * @var Request        $request
         */
        $exception = $this->getParam('exception');
        [$request] = $arguments;

        // Create exception for non-exception handlers
        // This means it was not found
        if (null === $exception) {
            $exception = new RuntimeException(
                sprintf(
                    '%s: %s %s',
                    (string)StatusCode::getReasonPhrase(StatusCode::HTTP_NOT_FOUND),
                    $request->getMethod(),
                    $request->getUri()
                ),
                StatusCode::HTTP_NOT_FOUND
            );
        }

        // This means it was an invalid method
        if (true === is_array($exception)) {
            $exception = new RuntimeException(
                sprintf(
                    '%s: %s %s',
                    (string)StatusCode::getReasonPhrase(StatusCode::HTTP_METHOD_NOT_ALLOWED),
                    $request->getMethod(),
                    $request->getUri()
                ),
                StatusCode::HTTP_METHOD_NOT_ALLOWED
            );
        }

        $httpStatusCode = (int)$exception->getCode() ?: StatusCode::HTTP_INTERNAL_SERVER_ERROR;
        $this->setHttpResponseCode($httpStatusCode);
        $trace = explode(PHP_EOL, $exception->getTraceAsString());
        $type = get_class($exception);

        $this->getFromServiceContainer(ApplicationInterface::APP_LOGGING_MANAGER)
            ->error($exception->getMessage(), [$httpStatusCode, $name, $type, json_encode($trace)]);

        if (Application::MODE_PRODUCTION ===
            $this->getFromServiceContainer(ApplicationInterface::APP_CONFIG_MANAGER)->getDirective('env')) {
            return ['msg' => StatusCode::getReasonPhrase($httpStatusCode)];
        }

        $return = [
            'msg'       => StatusCode::getReasonPhrase($httpStatusCode),
            'exception' => [
                'code'  => $httpStatusCode,
                'msg'   => $exception->getMessage(),
                'file'  => $exception->getFile(),
                'line'  => $exception->getLine(),
                'type'  => $type,
                'trace' => $trace
            ]
        ];

        return $return;
    }
}
