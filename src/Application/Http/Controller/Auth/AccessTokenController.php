<?php
/**
 * AccessTokenController.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http\Controller\Auth;

use Application\Http\Controller\AbstractController;
use Domain\Auth\Factory\UseCaseFactory;
use Slim\Http\Request;

/**
 * Class AccessTokenController
 */
class AccessTokenController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function post(Request $request): array
    {
        return $this->getFromServiceContainer(UseCaseFactory::class)
            ->newOAuthAccessTokenUseCase()->handleAuthenticateRequest($request);
    }
}
