<?php
/**
 * AuthorizationCodeController.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http\Controller\Auth;

use Application\Http\Controller\AbstractController;
use Application\Http\StatusCode;
use Domain\Auth\Factory\UseCaseFactory;
use Slim\Http\Request;

/**
 * Class AuthorizationCodeController
 */
class AuthorizationCodeController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function get(Request $request)
    {
        $this->setHttpResponseCode(StatusCode::HTTP_FOUND);

        return $this->getFromServiceContainer(UseCaseFactory::class)
            ->newOAuthAuthorizationCodeUseCase()
            ->authenticateWithAuthorizationCode(
                $request->getParam('client', 'invalidClientId'),
                $request->getParam('redirect', ''),
                'code',
                $request->getParam('state', '')
            );
    }
}
