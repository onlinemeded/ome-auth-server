<?php
/**
 * UsersController.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http\Controller\Auth;

use Application\Http\Controller\AbstractController;
use Domain\Auth\Factory\UseCaseFactory;
use Slim\Http\Request;

/**
 * Class UsersController
 */
class UsersController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function listAll(Request $request): array
    {
        return $this->getFromServiceContainer(UseCaseFactory::class)->newUserUseCase()->getUsers(
            $request->getAttribute('client_identifier', 'invalidClient'),
            (int)$request->getParam('l', 20),
            (int)$request->getParam('o', 0)
        );
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function get(Request $request): array
    {
        return $this->getFromServiceContainer(UseCaseFactory::class)->newUserUseCase()->getUserById(
            $request->getAttribute('identifier', 'invalidEmail'),
            $request->getAttribute('client_identifier', 'invalidClient')
        );
    }
}
