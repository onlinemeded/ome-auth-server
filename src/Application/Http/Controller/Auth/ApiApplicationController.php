<?php
/**
 * ApiApplicationController.php
 *
 * @copyright 2019 Patrick Loven. All Rights Reserved.
 */

namespace Application\Http\Controller\Auth;

use Application\Http\Controller\AbstractController;
use Domain\Auth\Factory\UseCaseFactory;

/**
 * Class ApiApplicationController
 */
class ApiApplicationController extends AbstractController
{
    /**
     * @return array
     */
    public function listAll(): array
    {
        return $this->getFromServiceContainer(UseCaseFactory::class)->newApiApplicationUseCase()->getApps(
            (int)$this->getParam('l', 20),
            (int)$this->getParam('o', 0)
        );
    }
}
