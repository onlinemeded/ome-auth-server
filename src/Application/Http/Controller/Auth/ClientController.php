<?php
/**
 * ClientController.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http\Controller\Auth;

use Application\Http\Controller\AbstractController;
use Domain\Auth\Factory\UseCaseFactory;
use Slim\Http\Request;

/**
 * Class ClientController
 */
class ClientController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function get(Request $request): array
    {
        return $this->getFromServiceContainer(UseCaseFactory::class)->newOAuthClientUseCase()->getClientByIdentifier(
            $request->getAttribute('identifier', 'invalidClient')
        );
    }

    /**
     * @return array
     */
    public function listAll(): array
    {
        return $this->getFromServiceContainer(UseCaseFactory::class)->newOAuthClientUseCase()->getClients();
    }
}
