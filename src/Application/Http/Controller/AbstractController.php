<?php
/**
 * AbstractController.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http\Controller;

use Application\Http\StatusCode;
use Infrastructure\DI\Container as ServiceContainer;
use Infrastructure\Exception\Runtime\InvalidArgumentException;
use Infrastructure\Exception\RuntimeException;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AbstractController
 */
abstract class AbstractController implements ControllerInterface
{
    /**
     * @var ServiceContainer
     */
    private $serviceContainer;

    /**
     * @var array
     */
    private $params = [];

    /**
     * @var string
     */
    private $action;

    /**
     * @var int
     */
    private $httpResponseCode = StatusCode::HTTP_OK;

    /**
     * @param ServiceContainer $serviceContainer
     * @param string           $action
     */
    public function __construct(ServiceContainer $serviceContainer, string $action)
    {
        $this->serviceContainer = $serviceContainer;
        $this->action = $action;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $arguments
     *
     * @return Response
     * @throws RuntimeException
     */
    public function dispatch(Request $request, Response $response, array $arguments): Response
    {
        if (false === method_exists($this, $this->action)) {
            throw new InvalidArgumentException(
                sprintf(
                    'No action "%s" found for "%s"',
                    $this->action,
                    static::class
                ),
                StatusCode::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        $this->setParams(array_merge((array)$request->getParams(), $arguments));

        try {
            return $response->withJson(
                call_user_func_array([$this, $this->getAction()], [$request, $response]),
                $this->getHttpResponseCode()
            );
        } catch (\RuntimeException $exception) {
            throw new RuntimeException(
                'There is an encoding issue. ' . $exception->getMessage(),
                StatusCode::HTTP_INTERNAL_SERVER_ERROR,
                $exception
            );
        }
    }

    /**
     * @return string
     */
    final protected function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return int
     */
    final protected function getHttpResponseCode(): int
    {
        return $this->httpResponseCode;
    }

    /**
     * @param int $code
     */
    final protected function setHttpResponseCode(int $code): void
    {
        $this->httpResponseCode = $code;
    }

    /**
     * @param string $name
     * @param null   $default
     *
     * @return mixed
     */
    final protected function getParam(string $name, $default = null)
    {
        if (true === array_key_exists($name, $this->params)) {
            return $this->params[$name];
        }

        return $default;
    }

    /**
     * @param array $params
     */
    final protected function setParams(array $params): void
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    final protected function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    final protected function getFromServiceContainer(string $name)
    {
        return $this->serviceContainer->offsetGet($name);
    }
}
