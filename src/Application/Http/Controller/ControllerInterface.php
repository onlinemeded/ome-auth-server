<?php
/**
 * ControllerInterface.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http\Controller;

use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class ControllerInterface
 */
interface ControllerInterface
{
    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $arguments
     *
     * @return Response
     */
    public function dispatch(Request $request, Response $response, array $arguments): Response;
}
