<?php
/**
 * OAuthResponse.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http\Response;

use OAuth2\Response;

/**
 * Class OAuthResponse
 */
class OAuthResponse extends Response
{
    /**
     * @param string $format
     *
     * @return array|mixed|string
     */
    public function getResponseBody($format = 'json')
    {
        return $this->parameters
            ? $this->parameters
            : '';
    }
}
