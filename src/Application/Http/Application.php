<?php
/**
 * Application.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http;

use Infrastructure\DI\Container as ServiceContainer;
use Slim\App;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use Closure;
use Throwable;

/**
 * Class Application
 */
final class Application implements ApplicationInterface
{
    /**
     * @var ServiceContainer
     */
    private $serviceContainer;

    /**
     * @var mixed[]
     */
    private $middleware = [];

    /**
     * @param ServiceContainer $serviceContainer
     * @param array            $middleware
     */
    public function __construct(ServiceContainer $serviceContainer, array $middleware = [])
    {
        $this->serviceContainer = $serviceContainer;
        $this->middleware = $middleware;
    }

    /**
     * @throws Throwable
     */
    public function run(): void
    {
        $config = $this->serviceContainer->offsetGet(self::APP_CONFIG_MANAGER);
        $container = new Container($config->getDirective('slim'));
        $this->addErrorHandlers($container, $config->getDirective('errorHandling')['handlers']);

        $slim = new App($container);

        // Add middleware
        foreach ($this->middleware as $middleware) {
            $slim->add($middleware);
        }

        // Add routes
        $this->addRoutes($slim, $config->getDirective('routing')['routes']);

        $slim->run();
    }

    /**
     * @param App   $application
     * @param array $config
     */
    private function addRoutes(App $application, array $config): void
    {
        $serviceContainer = $this->serviceContainer;

        foreach ($config as $route => $routeDetails) {
            [$method, $uri] = explode('@', $route);
            $application->{$method}(
                $uri,
                function (
                    Request $request,
                    Response $response,
                    array $arguments
                ) use (
                    $serviceContainer,
                    $routeDetails
                ): Response {
                    return (new $routeDetails['controller']($serviceContainer, $routeDetails['action']))->dispatch(
                        $request,
                        $response,
                        $arguments
                    );
                }
            );
        }
    }

    /**
     * @param Container $containers
     * @param array     $config
     */
    private function addErrorHandlers(Container $containers, array $config): void
    {
        $serviceContainer = $this->serviceContainer;

        // @see https://www.slimframework.com/docs/v3/handlers/error.html
        foreach ($config as $handler => $handlerConfig) {
            $containers[$handler] = function () use ($serviceContainer, $handlerConfig): Closure {
                return function (
                    Request $request,
                    Response $response,
                    $exception = null
                ) use (
                    $serviceContainer,
                    $handlerConfig
                ): Response {
                    return (new $handlerConfig['controller']($serviceContainer, $handlerConfig['action']))->dispatch(
                        $request,
                        $response,
                        ['exception' => $exception]
                    );
                };
            };
        }
    }
}
