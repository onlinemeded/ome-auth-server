<?php
/**
 * ApplicationInterface.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Application\Http;

use Infrastructure\Configuration\ConfigurationManager;
use Infrastructure\Entity\EntityManager;
use Infrastructure\Feature\FeatureManager;
use Infrastructure\Log\LoggingManager;
use OAuth2\Server as OAuth2Server;

/**
 * Interface ApplicationInterface
 */
interface ApplicationInterface
{
    const MODE_DEVELOPMENT = 'DEV';
    const MODE_PRODUCTION = 'PROD';
    const MODE_TEST = 'TEST';

    const APP_CONFIG_MANAGER = ConfigurationManager::class;
    const APP_ENTITY_MANAGER = EntityManager::class;
    const APP_FEATURE_MANAGER = FeatureManager::class;
    const APP_LOGGING_MANAGER = LoggingManager::class;
    const APP_OAUTH_SERVER = OAuth2Server::class;
}
