<?php
/**
 * LoggingManager.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Log;

use Application\Http\StatusCode;
use Infrastructure\InfrastructureException;
use Monolog\Handler\HandlerInterface;
use Monolog\Logger;
use Exception;
use ReflectionClass;

/**
 * Class LoggingManager
 */
class LoggingManager extends Logger
{
    /**
     * @param string $name
     * @param array  $handlers
     *
     * @return LoggingManager
     * @throws InfrastructureException
     */
    public static function create(string $name, array $handlers = []): LoggingManager
    {
        try {
            $instance = new self($name);

            foreach ($handlers as $class => $properties) {
                /** @var HandlerInterface $handler */
                $handler = (new ReflectionClass($class))->newInstanceArgs($properties);
                $instance->pushHandler($handler);
            }

            return $instance;
        } catch (Exception $exception) {
            throw new InfrastructureException(
                'Could not create log handler',
                StatusCode::HTTP_INTERNAL_SERVER_ERROR,
                $exception
            );
        }
    }
}
