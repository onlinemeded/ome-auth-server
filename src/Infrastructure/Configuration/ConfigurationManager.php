<?php
/**
 * ConfigurationManager.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Configuration;

/**
 * Class ConfigurationManager
 */
class ConfigurationManager implements ConfigurationManagerInterface
{
    /**
     * @var array
     */
    private $configuration = [];

    /**
     * @param array $config
     */
    final public function addConfigurationFromArray(array $config): void
    {
        $this->configuration = array_merge($this->configuration, $config);
    }

    /**
     * @param string $directive
     * @param null   $default
     *
     * @return mixed
     */
    final public function getDirective(string $directive, $default = null)
    {
        if (true === array_key_exists($directive, $this->configuration)) {
            return $this->configuration[$directive];
        }

        return $default;
    }
}
