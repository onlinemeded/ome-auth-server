<?php
/**
 * ConfigurationManagerInterface.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Configuration;

/**
 * Interface ConfigurationManagerInterface
 */
interface ConfigurationManagerInterface
{
    /**
     * @param string $directive
     * @param null   $default
     *
     * @return mixed
     */
    public function getDirective(string $directive, $default = null);
}
