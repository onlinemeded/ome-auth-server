<?php
/**
 * FeatureManager.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Feature;

use Opensoft\Rollout\Rollout;
use Opensoft\Rollout\Storage\StorageInterface;

/**
 * Class FeatureManager
 */
class FeatureManager extends Rollout
{
    /**
     * @param StorageInterface $storage
     *
     * @return FeatureManager
     */
    public static function create(StorageInterface $storage): FeatureManager
    {
        return new self($storage);
    }
}
