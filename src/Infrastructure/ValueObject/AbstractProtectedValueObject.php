<?php
/**
 * AbstractProtectedValueObject.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\ValueObject;

use Infrastructure\Exception\Runtime\BadMethodCallException;
use Infrastructure\Exception\Runtime\InvalidArgumentException;
use Infrastructure\Exception\RuntimeException;

/**
 * Class AbstractProtectedValueObject
 */
abstract class AbstractProtectedValueObject
{
    /**
     * @var mixed[]
     */
    private $properties = [];

    /**
     * @param mixed[] $properties
     */
    final protected function __construct(array $properties)
    {
        $this->properties = $properties;
    }

    /**
     * @param string $name
     *
     * @return mixed
     * @throws InvalidArgumentException
     */
    final public function &__get(string $name)
    {
        if (true === array_key_exists($name, $this->properties)) {
            return $this->properties[$name];
        }

        throw new InvalidArgumentException(sprintf('No property "%s"', $name));
    }

    /**
     * @param string $name
     * @param mixed  $value
     *
     * @throws RuntimeException
     */
    final public function &__set(string $name, $value): void
    {
        throw new RuntimeException(
            sprintf('Cannot set property "%s". Object "%s" is not writable', $name, static::class)
        );
    }

    /**
     * @param string $name
     * @param mixed  $arguments
     *
     * @return self
     * @throws BadMethodCallException
     */
    final public function __call(string $name, $arguments)
    {
        if (substr($name, 0, 4) === 'with') {
            $name = lcfirst(substr($name, 4));

            if (true === array_key_exists($name, $this->properties)) {
                $clone = clone $this;
                $clone->properties[$name] = $arguments[0];

                return $clone;
            }
        }

        throw new BadMethodCallException(sprintf('No method "%s" for "%s"', $name, static::class));
    }

    /**
     * @return array
     */
    final public function toArray(): array
    {
        $array = [];

        foreach ($this->properties as $property => $value) {
            $array[$property] = (false === $value instanceof self)
                ? $value
                : $value->toArray();
        }

        return $array;
    }
}
