<?php
/**
 * AesHmacCryptographer.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Crypto\Cryptographer;

use Exception;

/**
 * Class AesHmacCryptographer
 */
class AesHmacCryptographer implements CryptographerInterface
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $cipherMethod;

    /**
     * @var string
     */
    private $hmacAlgorithm;

    /**
     * @var int
     */
    private $iterations = 1;

    /**
     * @param string $key
     * @param string $cipherMethod
     * @param string $hmacAlgorithm
     */
    public function __construct(
        string $key,
        string $cipherMethod = self::CIPHER_METHOD_AES_256_CBC,
        string $hmacAlgorithm = self::HMAC_ALGO_SHA_512
    ) {
        $this->key = $key;
        $this->cipherMethod = $cipherMethod;
        $this->hmacAlgorithm = $hmacAlgorithm;
    }

    /**
     * @param mixed       $value
     * @param bool        $serialize
     * @param string|null $iv
     *
     * @return string
     */
    public function encrypt($value, $serialize = false, string $iv = null): string
    {
        $iv = $iv
            ?: openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->cipherMethod));
        $salt = openssl_random_pseudo_bytes(16);
        $encrypted = openssl_encrypt(
            (false === $serialize
                ? $value
                : serialize($value)),
            $this->cipherMethod,
            hex2bin(
                hash_pbkdf2('sha512', $this->key, $salt, $this->iterations, (int)($this->getCipherMethodLength() / 4))
            ),
            OPENSSL_RAW_DATA,
            $iv
        );
        $payload = base64_encode($encrypted);
        $hmac = hash_hmac($this->hmacAlgorithm, $payload, $this->key);

        return base64_encode(bin2hex($iv) . bin2hex($salt) . $hmac . $payload);
    }

    /**
     * @param string $text
     * @param bool   $unserialize
     *
     * @return mixed
     */
    public function decrypt(string $text, $unserialize = false)
    {
        $payload = base64_decode($text);

        try {
            $iv = hex2bin(mb_substr($payload, 0, 32, '8bit'));
            $salt = hex2bin(mb_substr($payload, 32, 32, '8bit'));
        } catch (Exception $e) {
            return null;
        }

        $hmac = mb_substr($payload, 64, 128, '8bit');
        $encrypted = mb_substr($payload, 192, null, '8bit');
        $hmacValidation = hash_hmac($this->hmacAlgorithm, $encrypted, $this->key);

        if (true !== hash_equals($hmac, $hmacValidation)) {
            return null;
        }

        $decrypted = openssl_decrypt(
            base64_decode($encrypted),
            $this->cipherMethod,
            hex2bin(
                hash_pbkdf2('sha512', $this->key, $salt, $this->iterations, (int)($this->getCipherMethodLength() / 4))
            ),
            OPENSSL_RAW_DATA,
            $iv
        );

        return (false === $unserialize)
            ? $decrypted
            : unserialize($decrypted);
    }

    /**
     * @return int
     */
    private function getCipherMethodLength(): int
    {
        return (int)abs(filter_var($this->cipherMethod, FILTER_SANITIZE_NUMBER_INT));
    }
}
