<?php
/**
 * CryptographerInterface.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Crypto\Cryptographer;

/**
 * Interface CryptographerInterface
 */
interface CryptographerInterface
{
    /* AES */
    const CIPHER_METHOD_AES_256_CBC = 'AES-256-CBC';
    const CIPHER_METHOD_AES_256_CTR = 'AES-256-CTR';

    /* HMAC */
    const HMAC_ALGO_SHA_256 = 'sha256';
    const HMAC_ALGO_SHA_512 = 'sha512';

    /**
     * @param mixed       $value
     * @param bool        $serialize
     * @param string|null $iv
     *
     * @return string
     */
    public function encrypt($value, $serialize = false, string $iv = null): string;

    /**
     * @param string $text
     * @param bool   $unserialize
     *
     * @return mixed
     */
    public function decrypt(string $text, $unserialize = false);
}
