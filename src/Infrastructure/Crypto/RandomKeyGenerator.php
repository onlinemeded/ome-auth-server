<?php
/**
 * RandomKeyGenerator.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Crypto;

/**
 * Class RandomKeyGenerator
 */
class RandomKeyGenerator
{
    const CHAR_LOWERCASE = 'abcdefghijklmnopqrstuvwxyz';
    const CHAR_UPPERCASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const CHAR_NUMBERS = '1234567890';
    const CHAR_SPECIAL = '`~!@#$%^&*()-=_+[]{}|;\':",./<>?';
    const CHAR_HEX = '123456789ABCDEF';

    const USE_LOWERCASE = 1;
    const USE_UPPERCASE = 2;
    const USE_NUMBERS = 4;
    const USE_SPECIAL = 8;
    const USE_HEX = 16;

    /**
     * @param int $length
     * @param int $flags
     *
     * @return string
     */
    public static function generateKey(int $length, int $flags = 0): string
    {
        $chars = $key = '';

        foreach ([
                     self::USE_LOWERCASE => self::CHAR_LOWERCASE,
                     self::USE_UPPERCASE => self::CHAR_UPPERCASE,
                     self::USE_NUMBERS   => self::CHAR_NUMBERS,
                     self::USE_SPECIAL   => self::CHAR_SPECIAL,
                     self::USE_HEX       => self::CHAR_HEX
                 ] as $doInclude => $include) {
            if ($doInclude & $flags) {
                $chars .= $include;
            }
        }

        $chars = str_split($chars);
        $charsLength = count($chars) - 1;

        if (0 < $charsLength) {
            for ($i = 0; $i < $length; $i++) {
                $key .= $chars[(int)floor(rand(0, $charsLength))];
            }
        }

        return $key;
    }
}
