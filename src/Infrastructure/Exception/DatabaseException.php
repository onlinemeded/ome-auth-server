<?php
/**
 * DatabaseException.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Exception;

use Infrastructure\InfrastructureException;

/**
 * Class DatabaseException
 */
class DatabaseException extends InfrastructureException
{
}
