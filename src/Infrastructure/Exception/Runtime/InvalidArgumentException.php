<?php
/**
 * InvalidArgumentException.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Exception\Runtime;

/**
 * Class InvalidArgumentException
 */
class InvalidArgumentException extends \InvalidArgumentException
{
}
