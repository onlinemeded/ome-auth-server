<?php
/**
 * BadMethodCallException.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Exception\Runtime;

use Infrastructure\Exception\RuntimeException;

/**
 * Class BadMethodCallException
 */
class BadMethodCallException extends RuntimeException
{
}
