<?php
/**
 * Container.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\DI;

/**
 * Class Container
 */
class Container extends \Pimple\Container
{
}
