<?php
/**
 * ArrayConvertibleInterface.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Conversion;

/**
 * Interface ArrayConvertibleInterface
 */
interface ArrayConvertibleInterface
{
    /**
     * @param array $params
     *
     * @return mixed
     */
    public static function fromArray(array $params);

    /**
     * @return array
     */
    public function toArray(): array;
}
