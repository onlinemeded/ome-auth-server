<?php
/**
 * InfrastructureException.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure;

use Exception;

/**
 * Class InfrastructureException
 */
class InfrastructureException extends Exception
{
}
