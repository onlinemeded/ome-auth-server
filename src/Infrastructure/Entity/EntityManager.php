<?php
/**
 * EntityManager.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Entity;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\ORMException;

/**
 * Class EntityManager
 */
class EntityManager extends \Doctrine\ORM\EntityManager implements EntityManagerInterface
{
    /**
     * @param Connection    $conn
     * @param Configuration $config
     * @param EventManager  $eventManager
     */
    protected function __construct(Connection $conn, Configuration $config, EventManager $eventManager)
    {
        parent::__construct($conn, $config, $eventManager);
    }

    /**
     * @param array|Connection  $connection
     * @param Configuration     $config
     * @param EventManager|null $eventManager
     *
     * @return EntityManager
     * @throws ORMException
     */
    public static function create($connection, Configuration $config, EventManager $eventManager = null): EntityManager
    {
        $connection = static::createConnection($connection, $config, $eventManager);

        return new static($connection, $config, $connection->getEventManager());
    }
}
