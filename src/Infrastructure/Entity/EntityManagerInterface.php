<?php
/**
 * EntityManagerInterface.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */

namespace Infrastructure\Entity;

/**
 * Interface EntityManagerInterface
 */
interface EntityManagerInterface extends \Doctrine\ORM\EntityManagerInterface
{
}
