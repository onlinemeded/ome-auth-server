<?php
/**
 * AbstractEntity.php
 *
 * Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Infrastructure\Entity;

/**
 * Class AbstractEntity
 */
abstract class AbstractEntity
{
    const HASH_ALGO = PASSWORD_BCRYPT;

    /**
     * @var array
     */
    protected $hashOptions = ['cost' => 11];

    /**
     * @param string $value
     *
     * @return bool|string|null
     */
    protected function encryptField(string $value)
    {
        return password_hash($value, self::HASH_ALGO, $this->hashOptions);
    }

    /**
     * @param string $encryptedValue
     * @param string $value
     *
     * @return bool
     */
    protected function verifyEncryptedFieldValue($encryptedValue, $value): bool
    {
        return password_verify($value, $encryptedValue);
    }
}
