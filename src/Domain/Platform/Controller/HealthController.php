<?php
/**
 * HealthController.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Platform\Controller;

use Application\Http\Controller\AbstractController;
use Application\Http\StatusCode;

/**
 * Class HealthController
 */
class HealthController extends AbstractController
{
    /**
     *
     */
    public function heartbeat(): void
    {
        $this->setHttpResponseCode(StatusCode::HTTP_NO_CONTENT);
    }
}
