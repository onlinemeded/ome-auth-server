<?php
/**
 * OAuthRepository.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Repository;

use Application\Http\Response\OAuthResponse;
use Application\Http\StatusCode;
use Domain\Auth\Factory\OAuthFactory;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;
use OAuth2\Request as OAuthRequest;
use OAuth2\Server as OAuthServer;

/**
 * Class OAuthRepository
 */
class OAuthRepository
{
    /**
     * @var OAuthServer
     */
    private $server;

    /**
     * @param OAuthServer $server
     */
    public function __construct(OAuthServer $server)
    {
        $this->server = $server;
    }

    /**
     * @param OAuthRequest $request
     *
     * @return OAuthResponse
     */
    public function authenticateWithAccessToken(OAuthRequest $request): OAuthResponse
    {
        $response = OAuthFactory::newResponse();
        $this->server->verifyResourceRequest($request, $response);

        return $response;
    }

    /**
     * @param OAuthRequest $request
     *
     * @return OAuthResponse
     */
    public function authenticateWithClientCredentials(OAuthRequest $request): OAuthResponse
    {
        $response = OAuthFactory::newResponse();
        $this->server->handleTokenRequest($request, $response);

        return $response;
    }

    /**
     * @param OAuthRequest $request
     *
     * @return OAuthResponse
     * @throws AuthenticationException
     */
    public function authenticateWithAuthorizationCode(OAuthRequest $request): OAuthResponse
    {
        $response = OAuthFactory::newResponse();
        $this->server->handleAuthorizeRequest($request, $response, true);

        if (null !== $response->getParameter('error', null)) {
            throw new AuthenticationException(
                $response->getParameter('error_description'),
                StatusCode::HTTP_BAD_REQUEST
            );
        }

        return $response;
    }
}
