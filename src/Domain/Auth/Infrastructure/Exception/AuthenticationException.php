<?php
/**
 * AuthenticationException.php
 *
 * @Copyright 2018-2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Exception;

/**
 * Class AuthenticationException
 */
class AuthenticationException extends AuthException
{
}
