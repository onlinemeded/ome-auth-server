<?php
/**
 * OAuthUserEntity.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Infrastructure\Conversion\ArrayConvertibleInterface;
use Infrastructure\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * OAuthUserEntity
 *
 * @ORM\Table(name="oauth_user", uniqueConstraints={@ORM\UniqueConstraint(name="uq_email_client",
 *      columns={"email", "api_application_id"})})
 * @ORM\Entity(repositoryClass="Domain\Auth\Infrastructure\Entity\OAuthUserRepository")
 */
class OAuthUserEntity extends AbstractEntity implements ArrayConvertibleInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string")
     */
    private $password;

    /**
     * @var ApiApplicationEntity
     *
     * @ORM\ManyToOne(targetEntity="Domain\Auth\Infrastructure\Entity\ApiApplicationEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="api_application_id", referencedColumnName="id")
     * })
     */
    private $apiApplication;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $email
     *
     * @return OAuthUserEntity
     */
    public function setEmail(string $email): OAuthUserEntity
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $password
     *
     * @return OAuthUserEntity
     */
    public function setPassword(string $password): OAuthUserEntity
    {
        $this->password = $this->encryptField($password);

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Set apiApplication.
     *
     * @param ApiApplicationEntity|null $apiApplication
     *
     * @return OAuthUserEntity
     */
    public function setApiApplication(ApiApplicationEntity $apiApplication = null)
    {
        $this->apiApplication = $apiApplication;

        return $this;
    }

    /**
     * Get apiApplication.
     *
     * @return ApiApplicationEntity|null
     */
    public function getApiApplication()
    {
        return $this->apiApplication;
    }

    /**
     * @param string $password
     *
     * @return bool
     */
    public function verifyPassword(string $password): bool
    {
        return $this->verifyEncryptedFieldValue($this->getPassword(), $password);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'user_id' => $this->id,
            'scope'   => null
        ];
    }

    /**
     * @param array $params
     *
     * @return OAuthUserEntity
     */
    public static function fromArray(array $params): OAuthUserEntity
    {
        $user = new self();

        foreach ($params as $property => $value) {
            $user->$property = $value;
        }

        return $user;
    }
}
