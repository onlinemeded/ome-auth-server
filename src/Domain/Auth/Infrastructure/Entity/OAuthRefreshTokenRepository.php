<?php
/**
 * OAuthRefreshTokenRepository.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use OAuth2\Storage\RefreshTokenInterface;
use DateTime;
use Exception;

/**
 * Class OAuthRefreshTokenRepository
 */
class OAuthRefreshTokenRepository extends EntityRepository implements RefreshTokenInterface
{
    /**
     * @param string $refreshToken
     *
     * @return array|null
     */
    public function getRefreshToken($refreshToken): ?array
    {
        /** @var OAuthRefreshTokenEntity $refreshToken */
        $refreshToken = $this->findOneBy(['refreshToken' => $refreshToken]);

        if (null !== $refreshToken) {
            $refreshToken = $refreshToken->toArray();
            $refreshToken['expires'] = $refreshToken['expires']->getTimestamp();

            return $refreshToken;
        }

        return null;
    }

    /**
     * @param string $refreshToken
     * @param string $clientIdentifier
     * @param string $userEmail
     * @param string $expires
     * @param null   $scope
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function setRefreshToken($refreshToken, $clientIdentifier, $userEmail, $expires, $scope = null): void
    {
        $refreshToken = OAuthRefreshTokenEntity::fromArray(
            [
                'refreshToken' => $refreshToken,
                'client'       => $this->_em->getRepository(OAuthClientEntity::class)->findOneBy(
                    ['clientIdentifier' => $clientIdentifier]
                ),
                'user'         => $this->_em->getRepository(OAuthUserEntity::class)->findOneBy(
                    ['email' => $userEmail]
                ),
                'expires'      => (new DateTime())->setTimestamp($expires),
                'scope'        => $scope,
            ]
        );

        $this->_em->persist($refreshToken);
        $this->_em->flush();
    }

    /**
     * @param string $refreshToken
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function unsetRefreshToken($refreshToken): void
    {
        $refreshToken = $this->findOneBy(['refreshToken' => $refreshToken]);

        if (null !== $refreshToken) {
            $this->_em->remove($refreshToken);
            $this->_em->flush();
        }
    }
}
