<?php
/**
 * OAuthClientEntity.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Infrastructure\Conversion\ArrayConvertibleInterface;
use Infrastructure\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * OAuthClientEntity
 *
 * @ORM\Table(name="oauth_client")
 * @ORM\Entity(repositoryClass="Domain\Auth\Infrastructure\Entity\OAuthClientRepository")
 */
class OAuthClientEntity extends AbstractEntity implements ArrayConvertibleInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client_identifier", type="string", unique=true)
     */
    private $clientIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(name="client_secret", type="string")
     */
    private $clientSecret;

    /**
     * @var string
     *
     * @ORM\Column(name="redirect_uri", type="string")
     */
    private $redirectUri;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $clientIdentifier
     *
     * @return OAuthClientEntity
     */
    public function setClientIdentifier(string $clientIdentifier): OAuthClientEntity
    {
        $this->clientIdentifier = $clientIdentifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientIdentifier(): string
    {
        return $this->clientIdentifier;
    }

    /**
     * @param string $clientSecret
     *
     * @return OAuthClientEntity
     */
    public function setClientSecret(string $clientSecret): OAuthClientEntity
    {
        $this->clientSecret = $this->encryptField($clientSecret);

        return $this;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * @param string $redirectUri
     *
     * @return OAuthClientEntity
     */
    public function setRedirectUri(string $redirectUri): OAuthClientEntity
    {
        $this->redirectUri = $redirectUri;

        return $this;
    }

    /**
     * @return string
     */
    public function getRedirectUri(): string
    {
        return $this->redirectUri;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return OAuthClientEntity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return OAuthClientEntity
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $clientSecret
     *
     * @return bool
     */
    public function verifyClientSecret(string $clientSecret): bool
    {
        return $this->verifyEncryptedFieldValue($this->getClientSecret(), $clientSecret);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'client_id'     => $this->clientIdentifier,
            'client_secret' => $this->clientSecret,
            'redirect_uri'  => $this->redirectUri
        ];
    }

    /**
     * @param array $params
     *
     * @return OAuthClientEntity
     */
    public static function fromArray(array $params): OAuthClientEntity
    {
        $client = new self();

        foreach ($params as $property => $value) {
            $client->$property = $value;
        }

        return $client;
    }
}
