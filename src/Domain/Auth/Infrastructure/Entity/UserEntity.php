<?php
/**
 * UserEntity.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Infrastructure\Conversion\ArrayConvertibleInterface;
use Infrastructure\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserEntity
 *
 * @ORM\Table(name="user", indexes={@ORM\Index(name="fk_users_oauth_users_idx", columns={"oauth_user_id"})})
 * @ORM\Entity(repositoryClass="Domain\Auth\Infrastructure\Entity\UserRepository")
 */
class UserEntity extends AbstractEntity implements ArrayConvertibleInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="first_name", type="string", length=45, nullable=true)
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_name", type="string", length=45, nullable=true)
     */
    private $lastName;

    /**
     * @var OAuthUserEntity
     *
     * @ORM\ManyToOne(targetEntity="OAuthUserEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="oauth_user_id", referencedColumnName="id")
     * })
     */
    private $oauthUser;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName.
     *
     * @param string|null $firstName
     *
     * @return UserEntity
     */
    public function setFirstName($firstName = null)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string|null $lastName
     *
     * @return UserEntity
     */
    public function setLastName($lastName = null)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string|null
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set oauthUser.
     *
     * @param OAuthUserEntity|null $oauthUser
     *
     * @return UserEntity
     */
    public function setOauthUser(OAuthUserEntity $oauthUser = null)
    {
        $this->oauthUser = $oauthUser;

        return $this;
    }

    /**
     * Get oauthUser.
     *
     * @return OAuthUserEntity|null
     */
    public function getOauthUser()
    {
        return $this->oauthUser;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'          => $this->id,
            'firstName'   => $this->firstName,
            'lastName'    => $this->lastName,
            'oAuthUserId' => $this->oauthUser->getId()
        ];
    }

    /**
     * @param array $params
     *
     * @return UserEntity
     */
    public static function fromArray(array $params): UserEntity
    {
        $user = new self();

        foreach ($params as $property => $value) {
            $user->$property = $value;
        }

        return $user;
    }
}
