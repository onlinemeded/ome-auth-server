<?php
/**
 * OAuthAccessTokenRepository.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use OAuth2\Storage\AccessTokenInterface;
use DateTime;
use Exception;

/**
 * Class OAuthAccessTokenRepository
 */
class OAuthAccessTokenRepository extends EntityRepository implements AccessTokenInterface
{
    /**
     * @param string $oAuthToken
     *
     * @return array|null
     */
    public function getAccessToken($oAuthToken): ?array
    {
        /** @var OAuthAccessTokenEntity $token */
        $token = $this->findOneBy(['token' => $oAuthToken]);

        if (null !== $token) {
            $accessToken = $token->toArray();
            $accessToken['expires'] = $token->getExpires()->getTimestamp();
            $accessToken['client_id'] = $token->getClient()->getClientIdentifier();

            return $accessToken;
        }

        return null;
    }

    /**
     * @param string $oAuthToken
     * @param mixed  $clientIdentifier
     * @param mixed  $userEmail
     * @param int    $expires
     * @param null   $scope
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function setAccessToken($oAuthToken, $clientIdentifier, $userEmail, $expires, $scope = null): void
    {
        $token = OAuthAccessTokenEntity::fromArray(
            [
                'token'   => $oAuthToken,
                'client'  => $this->_em->getRepository(OAuthClientEntity::class)->findOneBy(
                    ['clientIdentifier' => $clientIdentifier]
                ),
                'user'    => $this->_em->getRepository(OAuthUserEntity::class)->findOneBy(['email' => $userEmail]),
                'expires' => (new DateTime)->setTimestamp($expires),
                'scope'   => $scope,
            ]
        );

        $this->_em->persist($token);
        $this->_em->flush();
    }
}
