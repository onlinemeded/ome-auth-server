<?php
/**
 * OAuthRefreshTokenEntity.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Infrastructure\Conversion\ArrayConvertibleInterface;
use Infrastructure\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * OAuthRefreshTokenEntity
 *
 * @ORM\Table(name="oauth_refresh_tokens")
 * @ORM\Entity(repositoryClass="Domain\Auth\Infrastructure\Entity\OAuthRefreshTokenRepository")
 */
class OAuthRefreshTokenEntity extends AbstractEntity implements ArrayConvertibleInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="refresh_token", type="string", unique=true)
     */
    private $refreshToken;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="expires", type="datetime")
     */
    private $expires;

    /**
     * @var string|null
     *
     * @ORM\Column(name="scope", type="string", nullable=true)
     */
    private $scope;

    /**
     * @var OAuthClientEntity
     *
     * @ORM\ManyToOne(targetEntity="Domain\Auth\Infrastructure\Entity\OAuthClientEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var OAuthUserEntity
     *
     * @ORM\ManyToOne(targetEntity="Domain\Auth\Infrastructure\Entity\OAuthUserEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $refreshToken
     *
     * @return OAuthRefreshTokenEntity
     */
    public function setRefreshToken(string $refreshToken): OAuthRefreshTokenEntity
    {
        $this->refreshToken = $refreshToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    /**
     * @param DateTime $expires
     *
     * @return OAuthRefreshTokenEntity
     */
    public function setExpires(DateTime $expires): OAuthRefreshTokenEntity
    {
        $this->expires = $expires;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getExpires(): DateTime
    {
        return $this->expires;
    }

    /**
     * @param string|null $scope
     *
     * @return OAuthRefreshTokenEntity
     */
    public function setScope(string $scope = null): OAuthRefreshTokenEntity
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getScope(): ?string
    {
        return $this->scope;
    }

    /**
     * @param OAuthClientEntity $client
     *
     * @return OAuthRefreshTokenEntity
     */
    public function setClient(OAuthClientEntity $client): OAuthRefreshTokenEntity
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return OAuthClientEntity
     */
    public function getClient(): OAuthClientEntity
    {
        return $this->client;
    }

    /**
     * @param OAuthUserEntity $user
     *
     * @return OAuthRefreshTokenEntity
     */
    public function setUser(OAuthUserEntity $user): OAuthRefreshTokenEntity
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return OAuthUserEntity
     */
    public function getUser(): OAuthUserEntity
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'refresh_token' => $this->refreshToken,
            'client_id'     => $this->client->getClientIdentifier(),
            'user_id'       => (null !== $this->user)
                ? $this->user->getId()
                : null,
            'expires'       => $this->expires,
            'scope'         => $this->scope
        ];
    }

    /**
     * @param array $params
     *
     * @return OAuthRefreshTokenEntity
     */
    public static function fromArray(array $params): OAuthRefreshTokenEntity
    {
        $token = new self();

        foreach ($params as $property => $value) {
            $token->$property = $value;
        }

        return $token;
    }
}
