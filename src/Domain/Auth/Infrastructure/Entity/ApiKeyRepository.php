<?php
/**
 * ApiKeyRepository.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class ApiKeyRepository
 */
class ApiKeyRepository extends EntityRepository
{
}
