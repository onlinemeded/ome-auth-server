<?php
/**
 * OAuthClientRepository.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;
use Doctrine\ORM\EntityRepository;
use OAuth2\Storage\ClientCredentialsInterface;

/**
 * Class OAuthClientRepository
 */
class OAuthClientRepository extends EntityRepository implements ClientCredentialsInterface
{
    /**
     * @param string $clientIdentifier
     *
     * @return array|bool
     */
    public function getClientDetails($clientIdentifier)
    {
        /** @var OAuthClientEntity $client */
        $client = $this->findOneBy(['clientIdentifier' => $clientIdentifier]);

        if (null !== $client) {
            return $client->toArray();
        }

        return false;
    }

    /**
     * @param string $clientIdentifier
     * @param string $clientSecret
     *
     * @return bool
     */
    public function checkClientCredentials($clientIdentifier, $clientSecret = ''): bool
    {
        /** @var OAuthClientEntity $client */
        $client = $this->findOneBy(['clientIdentifier' => $clientIdentifier]);

        if (null !== $client) {
            return $client->verifyClientSecret($clientSecret);
        }

        return false;
    }

    /**
     * @param int    $clientId
     * @param string $grantType
     *
     * @return bool
     */
    public function checkRestrictedGrantType($clientId, $grantType): bool
    {
        return true;
    }

    /**
     * @param int $clientId
     *
     * @return bool
     */
    public function isPublicClient($clientId): bool
    {
        return false;
    }

    /**
     * @param int $clientId
     *
     * @return string|null
     */
    public function getClientScope($clientId): ?string
    {
        return null;
    }
}
