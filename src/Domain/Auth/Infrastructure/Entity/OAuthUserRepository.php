<?php
/**
 * OAuthUserRepository.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use OAuth2\Storage\UserCredentialsInterface;

/**
 * Class OAuthUserRepository
 */
class OAuthUserRepository extends EntityRepository implements UserCredentialsInterface
{
    /**
     * @param string $email
     * @param string $clientIdentifier
     *
     * @return bool
     */
    public function verifyUserClientMembership(string $email, string $clientIdentifier): bool
    {
        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('Domain\Auth\Infrastructure\Entity\OAuthUserEntity', 'u');
        $rsm->addFieldResult('u', 'id', 'id');

        $query = $this->_em->createNativeQuery(
            '
        SELECT
            u.id
        FROM
            oauth_user u
            JOIN api_application api ON (u.api_application_id = api.id)
            JOIN oauth_client c ON (api.oauth_client_id = c.id)
        WHERE
            u.email = :email
            AND c.client_identifier = :clientIdentifier
        ',
            $rsm
        );
        $query->setParameter(':email', $email);
        $query->setParameter(':clientIdentifier', $clientIdentifier);

        return !empty($query->getResult());
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return bool
     */
    public function checkUserCredentials($email, $password): bool
    {
        /** @var OAuthUserEntity $user */
        $user = $this->findOneBy(['email' => $email]);

        if (null !== $user) {
            return $user->verifyPassword($password);
        }

        return false;
    }

    /**
     * @param string $email
     *
     * @return array|null
     */
    public function getUserDetails($email): ?array
    {
        /** @var OAuthUserEntity $user */
        $user = $this->findOneBy(['email' => $email]);

        if (null !== $user) {
            $user = $user->toArray();
        }

        return $user;
    }
}
