<?php
/**
 * OAuthAuthorizationCodeEntity.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Infrastructure\Conversion\ArrayConvertibleInterface;
use Infrastructure\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * OAuthAuthorizationCodeEntity
 *
 * @ORM\Table(name="oauth_authorization_codes")
 * @ORM\Entity(repositoryClass="Domain\Auth\Infrastructure\Entity\OAuthAuthorizationCodeRepository")
 */
class OAuthAuthorizationCodeEntity extends AbstractEntity implements ArrayConvertibleInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", unique=true)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="client_id", type="integer")
     */
    private $client_id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $user_id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="expires", type="datetime")
     */
    private $expires;

    /**
     * @var string
     *
     * @ORM\Column(name="redirect_uri", type="string")
     */
    private $redirect_uri;

    /**
     * @var string|null
     *
     * @ORM\Column(name="scope", type="string", nullable=true)
     */
    private $scope;

    /**
     * @var OAuthClientEntity
     *
     * @ORM\ManyToOne(targetEntity="Domain\Auth\Infrastructure\Entity\OAuthClientEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var OAuthUserEntity
     *
     * @ORM\ManyToOne(targetEntity="Domain\Auth\Infrastructure\Entity\OAuthUserEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $code
     *
     * @return OAuthAuthorizationCodeEntity
     */
    public function setCode($code): OAuthAuthorizationCodeEntity
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param int $clientId
     *
     * @return OAuthAuthorizationCodeEntity
     */
    public function setClientId($clientId): OAuthAuthorizationCodeEntity
    {
        $this->client_id = $clientId;

        return $this;
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return $this->client_id;
    }

    /**
     * @param int|null $userId
     *
     * @return OAuthAuthorizationCodeEntity
     */
    public function setUserId($userId = null): OAuthAuthorizationCodeEntity
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    /**
     * @param DateTime $expires
     *
     * @return OAuthAuthorizationCodeEntity
     */
    public function setExpires(DateTime $expires): OAuthAuthorizationCodeEntity
    {
        $this->expires = $expires;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getExpires(): DateTime
    {
        return $this->expires;
    }

    /**
     * @param string $redirectUri
     *
     * @return OAuthAuthorizationCodeEntity
     */
    public function setRedirectUri($redirectUri): OAuthAuthorizationCodeEntity
    {
        $this->redirect_uri = $redirectUri;

        return $this;
    }

    /**
     * @return string
     */
    public function getRedirectUri(): string
    {
        return $this->redirect_uri;
    }

    /**
     * @param string|null $scope
     *
     * @return OAuthAuthorizationCodeEntity
     */
    public function setScope($scope = null): OAuthAuthorizationCodeEntity
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getScope(): ?string
    {
        return $this->scope;
    }

    /**
     * @param OAuthClientEntity $client
     *
     * @return OAuthAuthorizationCodeEntity
     */
    public function setClient(OAuthClientEntity $client): OAuthAuthorizationCodeEntity
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return OAuthClientEntity
     */
    public function getClient(): OAuthClientEntity
    {
        return $this->client;
    }

    /**
     * @param OAuthUserEntity $user
     *
     * @return OAuthAuthorizationCodeEntity
     */
    public function setUser(OAuthUserEntity $user): OAuthAuthorizationCodeEntity
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return OAuthUserEntity
     */
    public function getUser(): OAuthUserEntity
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'code'      => $this->code,
            'client_id' => $this->client->getClientIdentifier(),
            'user_id'   => (null !== $this->user)
                ? $this->user->getId()
                : null,
            'expires'   => $this->expires,
            'scope'     => $this->scope
        ];
    }

    /**
     * @param array $params
     *
     * @return OAuthAuthorizationCodeEntity
     */
    public static function fromArray(array $params): OAuthAuthorizationCodeEntity
    {
        $code = new self();

        foreach ($params as $property => $value) {
            $code->$property = $value;
        }

        return $code;
    }
}
