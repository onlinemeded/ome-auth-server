<?php
/**
 * OAuthAuthorizationCodeRepository.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use OAuth2\Storage\AuthorizationCodeInterface;
use DateTime;
use Exception;

/**
 * Class OAuthAuthorizationCodeRepository
 */
class OAuthAuthorizationCodeRepository extends EntityRepository implements AuthorizationCodeInterface
{
    /**
     * @param string $code
     *
     * @return array|null
     */
    public function getAuthorizationCode($code): ?array
    {
        /** @var OAuthAuthorizationCodeEntity $code */
        $code = $this->findOneBy(['code' => $code]);

        if (null !== $code) {
            $authCode = $code->toArray();
            $authCode['expires'] = $code->getExpires()->getTimestamp();
            $authCode['client_id'] = $code->getClient()->getClientIdentifier();

            return $authCode;
        }

        return null;
    }

    /**
     * @param string $code
     * @param mixed  $clientIdentifier
     * @param mixed  $userEmail
     * @param string $redirectUri
     * @param int    $expires
     * @param null   $scope
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function setAuthorizationCode(
        $code,
        $clientIdentifier,
        $userEmail,
        $redirectUri,
        $expires,
        $scope = null
    ): void {
        $authCode = OAuthAuthorizationCodeEntity::fromArray(
            [
                'code'         => $code,
                'client'       => $this->_em->getRepository(OAuthClientEntity::class)->findOneBy(
                    ['clientIdentifier' => $clientIdentifier]
                ),
                'user'         => $this->_em->getRepository(OAuthUserEntity::class)->findOneBy(['email' => $userEmail]),
                'redirect_uri' => $redirectUri,
                'expires'      => (new DateTime())->setTimestamp($expires),
                'scope'        => $scope,
            ]
        );

        $this->_em->persist($authCode);
        $this->_em->flush();
    }

    /**
     * @param string $code
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function expireAuthorizationCode($code): void
    {
        $entity = $this->findOneBy(['code' => $code]);

        if (null !== $entity) {
            $this->_em->remove($entity);
            $this->_em->flush();
        }
    }
}
