<?php
/**
 * UserRepository.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Class UserRepository
 */
class UserRepository extends EntityRepository
{
    /**
     * @param string   $clientIdentifier
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return UserEntity[]
     */
    public function findUsersByClientIdentifier(
        string $clientIdentifier,
        ?int $limit = null,
        ?int $offset = null
    ): array {
        $query = $this->_em->createNativeQuery(
            '
        SELECT
            u.id as userId,
            u.first_name as userFirstName,
            u.last_name as userLastName,
            oau.id as oauId,
            oau.email as oauEmail,
            api.id as apiId,
            api.name as apiName,
            api.description as apiDescription,
            c.id as clientId,
            c.client_identifier as clientIdentifier,
            c.redirect_uri as clientRedirectUri,
            c.name as clientName,
            c.description as clientDescription,
            "*****" as password
        FROM
            user u
                JOIN oauth_user oau ON (u.oauth_user_id = oau.id)
                JOIN api_application api ON (oau.api_application_id = api.id)
                JOIN oauth_client c ON (api.oauth_client_id = c.id)
        WHERE
            c.client_identifier = :clientIdentifier
        LIMIT ' . $offset . ', ' . $limit . '
        ',
            $this->getCustomUserResultSetMapping()
        );
        $query->setParameter(':clientIdentifier', $clientIdentifier);

        return $query->getResult();
    }

    /**
     * @param string $userIdentifier
     * @param string $clientIdentifier
     *
     * @return UserEntity
     */
    public function findUser(string $userIdentifier, string $clientIdentifier): ?UserEntity
    {
        $query = $this->_em->createNativeQuery(
            '
        SELECT
            u.id as userId,
            u.first_name as userFirstName,
            u.last_name as userLastName,
            oau.id as oauId,
            oau.email as oauEmail,
            api.id as apiId,
            api.name as apiName,
            api.description as apiDescription,
            c.id as clientId,
            c.client_identifier as clientIdentifier,
            c.redirect_uri as clientRedirectUri,
            c.name as clientName,
            c.description as clientDescription,
            "*****" as password
        FROM
            user u
                JOIN oauth_user oau ON (u.oauth_user_id = oau.id)
                JOIN api_application api ON (oau.api_application_id = api.id)
                JOIN oauth_client c ON (api.oauth_client_id = c.id)
        WHERE
            oau.email = :userIdentifier
            AND c.client_identifier = :clientIdentifier
        ',
            $this->getCustomUserResultSetMapping()
        );
        $query->setParameter(':userIdentifier', $userIdentifier);
        $query->setParameter(':clientIdentifier', $clientIdentifier);

        return $query->getResult()[0] ?? null;
    }

    /**
     * @return ResultSetMapping
     */
    private function getCustomUserResultSetMapping(): ResultSetMapping
    {
        static $rsm = null;

        if (null === $rsm) {
            $rsm = new ResultSetMapping;
            $rsm->addEntityResult(UserEntity::class, 'u');
            $rsm->addFieldResult('u', 'userId', 'id');
            $rsm->addFieldResult('u', 'userFirstName', 'firstName');
            $rsm->addFieldResult('u', 'userLastName', 'lastName');
            $rsm->addJoinedEntityResult(OAuthUserEntity::class, 'oau', 'u', 'oauthUser');
            $rsm->addFieldResult('oau', 'oauId', 'id');
            $rsm->addFieldResult('oau', 'oauEmail', 'email');
            $rsm->addFieldResult('oau', 'password', 'password');
            $rsm->addJoinedEntityResult(ApiApplicationEntity::class, 'api', 'oau', 'apiApplication');
            $rsm->addFieldResult('api', 'apiId', 'id');
            $rsm->addFieldResult('api', 'apiName', 'name');
            $rsm->addFieldResult('api', 'apiDescription', 'description');
            $rsm->addFieldResult('api', 'password', 'applicationSecret');
            $rsm->addJoinedEntityResult(OAuthClientEntity::class, 'c', 'api', 'oauthClient');
            $rsm->addFieldResult('c', 'clientId', 'id');
            $rsm->addFieldResult('c', 'clientIdentifier', 'clientIdentifier');
            $rsm->addFieldResult('c', 'clientRedirectUri', 'redirectUri');
            $rsm->addFieldResult('c', 'password', 'clientSecret');
            $rsm->addFieldResult('c', 'clientName', 'name');
            $rsm->addFieldResult('c', 'clientDescription', 'description');
        }

        return $rsm;
    }
}
