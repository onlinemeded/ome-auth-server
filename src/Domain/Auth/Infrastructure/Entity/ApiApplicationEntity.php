<?php
/**
 * ApiApplicationEntity.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Infrastructure\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * ApiApplicationEntity
 *
 * @ORM\Table(name="api_application", indexes={@ORM\Index(name="fk_users_oauth_clients_idx",
 *      columns={"oauth_client_id"})})
 * @ORM\Entity(repositoryClass="Domain\Auth\Infrastructure\Entity\ApiApplicationRepository")
 */
class ApiApplicationEntity extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="application_identifier", type="string", unique=true)
     */
    private $applicationIdentifier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="application_secret", type="string", nullable=true)
     */
    private $applicationSecret;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var OAuthClientEntity
     *
     * @ORM\ManyToOne(targetEntity="Domain\Auth\Infrastructure\Entity\OAuthClientEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="oauth_client_id", referencedColumnName="id")
     * })
     */
    private $oauthClient;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set applicationIdentifier.
     *
     * @param string $applicationIdentifier
     *
     * @return ApiApplicationEntity
     */
    public function setApplicationIdentifier($applicationIdentifier)
    {
        $this->applicationIdentifier = $applicationIdentifier;

        return $this;
    }

    /**
     * Get applicationIdentifier.
     *
     * @return string
     */
    public function getApplicationIdentifier()
    {
        return $this->applicationIdentifier;
    }

    /**
     * Set applicationSecret.
     *
     * @param string|null $applicationSecret
     *
     * @return ApiApplicationEntity
     */
    public function setApplicationSecret($applicationSecret = null)
    {
        $this->applicationSecret = $applicationSecret;

        return $this;
    }

    /**
     * Get applicationSecret.
     *
     * @return string|null
     */
    public function getApplicationSecret()
    {
        return $this->applicationSecret;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ApiApplicationEntity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return ApiApplicationEntity
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set oauthClient.
     *
     * @param OAuthClientEntity|null $oauthClient
     *
     * @return ApiApplicationEntity
     */
    public function setOauthClient(OAuthClientEntity $oauthClient = null)
    {
        $this->oauthClient = $oauthClient;

        return $this;
    }

    /**
     * Get oauthClient.
     *
     * @return OAuthClientEntity|null
     */
    public function getOauthClient()
    {
        return $this->oauthClient;
    }
}
