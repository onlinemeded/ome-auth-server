<?php
/**
 * ApiKeyEntity.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Infrastructure\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * ApiKeyEntity
 *
 * @ORM\Table(name="api_key",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="value_UNIQUE", columns={"value"})},
 *     indexes={@ORM\Index(name="fk_api_key_api_application_id_idx", columns={"api_application_id"})})
 * @ORM\Entity(repositoryClass="Domain\Auth\Infrastructure\Entity\ApiKeyRepository")
 */
class ApiKeyEntity extends AbstractEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=30, nullable=false)
     */
    private $value;

    /**
     * @var ApiApplicationEntity
     *
     * @ORM\ManyToOne(targetEntity="Domain\Auth\Infrastructure\Entity\ApiApplicationEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="api_application_id", referencedColumnName="id")
     * })
     */
    private $apiApplication;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $value
     *
     * @return ApiKeyEntity
     */
    public function setValue($value): ApiKeyEntity
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * Set apiApplication.
     *
     * @param ApiApplicationEntity|null $apiApplication
     *
     * @return ApiKeyEntity
     */
    public function setApiApplication(ApiApplicationEntity $apiApplication = null)
    {
        $this->apiApplication = $apiApplication;

        return $this;
    }

    /**
     * Get apiApplication.
     *
     * @return ApiApplicationEntity|null
     */
    public function getApiApplication()
    {
        return $this->apiApplication;
    }
}
