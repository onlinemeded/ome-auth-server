<?php
/**
 * OAuthAccessTokenEntity.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Infrastructure\Entity;

use Infrastructure\Conversion\ArrayConvertibleInterface;
use Infrastructure\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * OAuthAccessTokenEntity
 *
 * @ORM\Table(name="oauth_access_token")
 * @ORM\Entity(repositoryClass="Domain\Auth\Infrastructure\Entity\OAuthAccessTokenRepository")
 */
class OAuthAccessTokenEntity extends AbstractEntity implements ArrayConvertibleInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", unique=true)
     */
    private $token;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="expires", type="datetime")
     */
    private $expires;

    /**
     * @var string|null
     *
     * @ORM\Column(name="scope", type="string", nullable=true)
     */
    private $scope;

    /**
     * @var OAuthClientEntity
     *
     * @ORM\ManyToOne(targetEntity="Domain\Auth\Infrastructure\Entity\OAuthClientEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var OAuthUserEntity
     *
     * @ORM\ManyToOne(targetEntity="Domain\Auth\Infrastructure\Entity\OAuthUserEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $token
     *
     * @return OAuthAccessTokenEntity
     */
    public function setToken($token): OAuthAccessTokenEntity
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param DateTime $expires
     *
     * @return OAuthAccessTokenEntity
     */
    public function setExpires(DateTime $expires): OAuthAccessTokenEntity
    {
        $this->expires = $expires;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getExpires(): DateTime
    {
        return $this->expires;
    }

    /**
     * @param string|null $scope
     *
     * @return OAuthAccessTokenEntity
     */
    public function setScope($scope = null): OAuthAccessTokenEntity
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getScope(): ?string
    {
        return $this->scope;
    }

    /**
     * @param OAuthClientEntity $client
     *
     * @return OAuthAccessTokenEntity
     */
    public function setClient(OAuthClientEntity $client): OAuthAccessTokenEntity
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return OAuthClientEntity
     */
    public function getClient(): OAuthClientEntity
    {
        return $this->client;
    }

    /**
     * @param OAuthUserEntity $user
     *
     * @return OAuthAccessTokenEntity
     */
    public function setUser(OAuthUserEntity $user): OAuthAccessTokenEntity
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return OAuthUserEntity
     */
    public function getUser(): OAuthUserEntity
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'token'     => $this->token,
            'client_id' => $this->client->getClientIdentifier(),
            'user_id'   => (null !== $this->user)
                ? $this->user->getId()
                : null,
            'expires'   => $this->expires,
            'scope'     => $this->scope
        ];
    }

    /**
     * @param array $params
     *
     * @return OAuthAccessTokenEntity
     */
    public static function fromArray(array $params): OAuthAccessTokenEntity
    {
        $token = new self();

        foreach ($params as $property => $value) {
            $token->$property = $value;
        }

        return $token;
    }
}
