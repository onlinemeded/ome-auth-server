<?php
/**
 * OAuthFactory.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Factory;

use Application\Http\Response\OAuthResponse;
use Application\Http\StatusCode;

/**
 * Class OAuthFactory
 */
class OAuthFactory
{
    /**
     * @param array $parameters
     * @param int   $statusCode
     * @param array $headers
     *
     * @return OAuthResponse
     */
    public static function newResponse(
        array $parameters = [],
        int $statusCode = StatusCode::HTTP_OK,
        array $headers = []
    ): OAuthResponse {
        return new OAuthResponse($parameters, $statusCode, $headers);
    }
}
