<?php
/**
 * UseCaseFactory.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Factory;

use Domain\Auth\UseCase\ApiApplicationUseCase;
use Domain\Auth\UseCase\OAuthAccessTokenUseCase;
use Domain\Auth\UseCase\OAuthAuthorizationCodeUseCase;
use Domain\Auth\UseCase\OAuthClientUseCase;
use Domain\Auth\UseCase\OAuthResourceUseCase;
use Domain\Auth\UseCase\OAuthUserUseCase;
use Domain\Auth\UseCase\UserUseCase;

/**
 * Class UseCaseFactory
 */
class UseCaseFactory
{
    /**
     * @var RepositoryFactory
     */
    private $repositoryFactory;

    /**
     * @param RepositoryFactory $repositoryFactory
     */
    public function __construct(RepositoryFactory $repositoryFactory)
    {
        $this->repositoryFactory = $repositoryFactory;
    }

    /**
     * @return ApiApplicationUseCase
     */
    public function newApiApplicationUseCase(): ApiApplicationUseCase
    {
        return new ApiApplicationUseCase($this->repositoryFactory->newApiApplicationRepository());
    }

    /**
     * @return OAuthAccessTokenUseCase
     */
    public function newOAuthAccessTokenUseCase(): OAuthAccessTokenUseCase
    {
        return new OAuthAccessTokenUseCase(
            $this->repositoryFactory->newOAuthRepository(),
            $this->repositoryFactory->newOAuthUserRepository()
        );
    }

    /**
     * @return OAuthAuthorizationCodeUseCase
     */
    public function newOAuthAuthorizationCodeUseCase(): OAuthAuthorizationCodeUseCase
    {
        return new OAuthAuthorizationCodeUseCase($this->repositoryFactory->newOAuthRepository());
    }

    /**
     * @return OAuthClientUseCase
     */
    public function newOAuthClientUseCase(): OAuthClientUseCase
    {
        return new OAuthClientUseCase(
            $this->repositoryFactory->newOAuthClientRepository(),
            $this->repositoryFactory->newApiKeyRepository()
        );
    }

    /**
     * @return OAuthResourceUseCase
     */
    public function newOAuthResourceUseCase(): OAuthResourceUseCase
    {
        return new OAuthResourceUseCase($this->repositoryFactory->newOAuthRepository());
    }

    /**
     * @return OAuthUserUseCase
     */
    public function newOAuthUserUseCase(): OAuthUserUseCase
    {
        return new OAuthUserUseCase($this->repositoryFactory->newOAuthUserRepository());
    }

    /**
     * @return UserUseCase
     */
    public function newUserUseCase(): UserUseCase
    {
        return new UserUseCase(
            $this->repositoryFactory->newUserRepository(),
            $this->repositoryFactory->newOAuthUserRepository()
        );
    }
}
