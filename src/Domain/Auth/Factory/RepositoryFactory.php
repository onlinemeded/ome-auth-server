<?php
/**
 * RepositoryFactory.php
 *
 * @copyright 2019 OnlineMedEd, LLC
 */

namespace Domain\Auth\Factory;

use Domain\Auth\Infrastructure\Entity\ApiApplicationEntity;
use Domain\Auth\Infrastructure\Entity\ApiApplicationRepository;
use Domain\Auth\Infrastructure\Entity\ApiKeyEntity;
use Domain\Auth\Infrastructure\Entity\ApiKeyRepository;
use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;
use Domain\Auth\Infrastructure\Entity\OAuthClientRepository;
use Domain\Auth\Infrastructure\Entity\OAuthUserEntity;
use Domain\Auth\Infrastructure\Entity\OAuthUserRepository;
use Domain\Auth\Infrastructure\Entity\UserEntity;
use Domain\Auth\Infrastructure\Entity\UserRepository;
use Domain\Auth\Repository\OAuthRepository;
use Infrastructure\Entity\EntityManager;
use OAuth2\Server as OAuthServer;

/**
 * Class RepositoryFactory
 */
class RepositoryFactory
{
    /**
     * @var OAuthServer
     */
    private $oAuthServer;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     * @param OAuthServer   $oAuthServer
     */
    public function __construct(EntityManager $entityManager, OAuthServer $oAuthServer)
    {
        $this->entityManager = $entityManager;
        $this->oAuthServer = $oAuthServer;
    }

    /**
     * @return OAuthRepository
     */
    public function newOAuthRepository(): OAuthRepository
    {
        return new OAuthRepository($this->oAuthServer);
    }

    /**
     * @return ApiApplicationRepository
     */
    public function newApiApplicationRepository(): ApiApplicationRepository
    {
        return $this->entityManager->getRepository(ApiApplicationEntity::class);
    }

    /**
     * @return ApiKeyRepository
     */
    public function newApiKeyRepository(): ApiKeyRepository
    {
        return $this->entityManager->getRepository(ApiKeyEntity::class);
    }

    /**
     * @return OAuthClientRepository
     */
    public function newOAuthClientRepository(): OAuthClientRepository
    {
        return $this->entityManager->getRepository(OAuthClientEntity::class);
    }

    /**
     * @return OAuthUserRepository
     */
    public function newOAuthUserRepository(): OAuthUserRepository
    {
        return $this->entityManager->getRepository(OAuthUserEntity::class);
    }

    /**
     * @return UserRepository
     */
    public function newUserRepository(): UserRepository
    {
        return $this->entityManager->getRepository(UserEntity::class);
    }
}
