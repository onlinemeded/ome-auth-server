<?php
/**
 * OAuthRequestService.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\Service;

use OAuth2\Request as OAuthRequest;

/**
 * Class OAuthRequestService
 */
class OAuthRequestService
{
    /**
     * @param string      $method
     * @param array       $query
     * @param array       $request
     * @param array       $attributes
     * @param array       $cookies
     * @param array       $files
     * @param array       $server
     * @param string|null $content
     * @param array|null  $headers
     *
     * @return OAuthRequest
     */
    public static function getRequestFromHttp(
        string $method = 'GET',
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        string $content = null,
        array $headers = null
    ): OAuthRequest {
        $originalMethod = $_SERVER['REQUEST_METHOD'];
        $_SERVER['REQUEST_METHOD'] = $method;

        $request = new OAuthRequest(
            array_merge($_GET, $query),
            array_merge($_POST, $request),
            $attributes,
            array_merge($_COOKIE, $cookies),
            array_merge($_FILES, $files),
            array_merge($_SERVER, $server),
            $content,
            $headers
        );
        $contentType = $request->server('CONTENT_TYPE', '');
        $requestMethod = strtoupper($method);
        $data = $request->request;

        if ((0 === strpos($contentType, 'application/x-www-form-urlencoded'))
            && (true === in_array($requestMethod, ['PUT', 'DELETE']))) {
            parse_str((string)$request->getContent(), $data);
        }

        if ((0 === strpos($contentType, 'application/json'))
            && (true === in_array($requestMethod, ['POST', 'PUT', 'DELETE']))) {
            $data = json_decode((string)$request->getContent(), true);
        }

        $request->request = $data;

        $_SERVER['REQUEST_METHOD'] = $originalMethod;

        return $request;
    }
}
