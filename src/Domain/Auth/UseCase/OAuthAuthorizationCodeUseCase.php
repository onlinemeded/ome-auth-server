<?php
/**
 * OAuthAuthorizationCodeUseCase.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\UseCase;

use Domain\Auth\Infrastructure\Exception\AuthenticationException;
use Domain\Auth\Service\OAuthRequestService;

/**
 * Class OAuthAuthorizationCodeUseCase
 */
class OAuthAuthorizationCodeUseCase extends AbstractOAuthRepositoryUseCase
{
    /**
     * @param string      $clientId
     * @param string      $redirectUri
     * @param string      $responseType
     * @param string|null $state
     *
     * @return string
     * @throws AuthenticationException
     */
    public function authenticateWithAuthorizationCode(
        string $clientId,
        string $redirectUri,
        string $responseType = 'code',
        string $state = null
    ): string {
        $response = $this->repository->authenticateWithAuthorizationCode(
            OAuthRequestService::getRequestFromHttp(
                'GET',
                [
                    'client_id'     => $clientId,
                    'redirect_uri'  => $redirectUri,
                    'response_type' => $responseType,
                    'state'         => $state
                ]
            )
        );

        return $response->getHttpHeader('Location');
    }
}
