<?php
/**
 * OAuthAccessTokenUseCase.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\UseCase;

use Application\Http\StatusCode;
use Domain\Auth\Infrastructure\Entity\OAuthUserRepository;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;
use Domain\Auth\Repository\OAuthRepository;
use Domain\Auth\Service\OAuthRequestService;
use Slim\Http\Request;

/**
 * Class OAuthAccessTokenUseCase
 */
class OAuthAccessTokenUseCase
{
    /**
     * @var OAuthRepository
     */
    private $oAuthRepository;

    /**
     * @var OAuthUserRepository
     */
    private $userRepository;

    /**
     * @param OAuthRepository     $oAuthRepository
     * @param OAuthUserRepository $userRepository
     */
    public function __construct(OAuthRepository $oAuthRepository, OAuthUserRepository $userRepository)
    {
        $this->oAuthRepository = $oAuthRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     *
     * @return array
     * @throws AuthenticationException
     */
    public function handleAuthenticateRequest(Request $request): array
    {
        $clientInfo = explode(':', $request->getUri()->getUserInfo());
        $response = $this->oAuthRepository->authenticateWithClientCredentials(
            OAuthRequestService::getRequestFromHttp(
                'POST',
                [],
                [],
                [],
                [],
                [],
                array_combine(['PHP_AUTH_USER', 'PHP_AUTH_PW'], $clientInfo)
            )
        );
        $responseBody = $response->getResponseBody();

        if (StatusCode::HTTP_OK !== $response->getStatusCode()) {
            throw new AuthenticationException($responseBody['error_description'], StatusCode::HTTP_FORBIDDEN);
        }

        if ('password' === $request->getParam('grant_type')) {
            if (false === $this->userRepository->verifyUserClientMembership(
                $request->getParam('username'),
                $clientInfo[0]
            )) {
                throw new AuthenticationException('User does not belong to client', StatusCode::HTTP_FORBIDDEN);
            }
        }

        return $responseBody;
    }
}
