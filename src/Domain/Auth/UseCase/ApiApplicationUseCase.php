<?php
/**
 * ApiApplicationUseCase.php
 *
 * @copyright 2019 Patrick Loven. All Rights Reserved.
 */

namespace Domain\Auth\UseCase;

use Domain\Auth\Infrastructure\Entity\ApiApplicationEntity;
use Domain\Auth\Infrastructure\Entity\ApiApplicationRepository;
use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;

/**
 * Class ApiApplicationUseCase
 */
class ApiApplicationUseCase
{
    /**
     * @var ApiApplicationRepository
     */
    private $repository;

    /**
     * @param ApiApplicationRepository $repository
     */
    public function __construct(ApiApplicationRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return array
     * @throws \Exception
     */
    public function getApps(int $limit = null, int $offset = null): array
    {
        $result = [];
        $apps = $this->repository->findBy([], null, $limit, $offset);

        if (false === empty($apps)) {
            /** @var ApiApplicationEntity $app */
            foreach ($apps as $app) {
                /** @var OAuthClientEntity $client */
                $client = $app->getOauthClient();
                $result[] = [
                    'id'                => $app->getId(),
                    'name'              => $app->getName(),
                    'client_identifier' => $client->getClientIdentifier()
                ];
            }
        }

        return $result;
    }
}
