<?php
/**
 * OAuthClientUseCase.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\UseCase;

use Domain\Auth\Infrastructure\Entity\ApiKeyRepository;
use Domain\Auth\Infrastructure\Entity\OAuthClientEntity;
use Domain\Auth\Infrastructure\Entity\OAuthClientRepository;

/**
 * Class OAuthClientUseCase
 */
class OAuthClientUseCase
{
    /**
     * @var OAuthClientRepository
     */
    private $clientRepository;

    /**
     * @var ApiKeyRepository
     */
    private $apiKeyRepository;

    /**
     * @param OAuthClientRepository $clientRepository
     * @param ApiKeyRepository      $apiKeyRepository
     */
    public function __construct(OAuthClientRepository $clientRepository, ApiKeyRepository $apiKeyRepository)
    {
        $this->apiKeyRepository = $apiKeyRepository;
        $this->clientRepository = $clientRepository;
    }

    /**
     * @return array
     */
    public function getClients(): array
    {
        $array = [];
        $clients = $this->clientRepository->findAll();

        if (false === empty($clients)) {
            /** @var OAuthClientEntity $client */
            foreach ($clients as $client) {
                $array[] = [
                    'id'         => $client->getId(),
                    'identifier' => $client->getClientIdentifier()
                ];
            }
        }

        return $array;
    }

    /**
     * @param string $clientIdentifier
     *
     * @return array
     */
    public function getClientByIdentifier(string $clientIdentifier): array
    {
        $client = $this->clientRepository->findOneBy(['clientIdentifier' => $clientIdentifier]);

        if (true === empty($client)) {
            return [];
        }

        /** @var OAuthClientEntity $client */
        return [
            'clientIdentifier' => $client->getClientIdentifier(),
            'redirectUrl'      => $client->getRedirectUri()
        ];
    }
}
