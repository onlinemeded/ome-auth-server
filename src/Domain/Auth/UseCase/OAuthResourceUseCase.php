<?php
/**
 * OAuthResourceUseCase.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\UseCase;

use Application\Http\StatusCode;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;
use Domain\Auth\Service\OAuthRequestService;

/**
 * Class OAuthResourceUseCase
 */
class OAuthResourceUseCase extends AbstractOAuthRepositoryUseCase
{
    /**
     * @param string $accessToken
     *
     * @return array
     * @throws AuthenticationException
     */
    public function authenticateWithAccessToken(string $accessToken): array
    {
        $response = $this->repository->authenticateWithAccessToken(
            OAuthRequestService::getRequestFromHttp(
                'POST',
                [],
                ['access_token' => $accessToken],
                [],
                [],
                [],
                [],
                null,
                ['AUTHORIZATION' => null]
            )
        );

        if (StatusCode::HTTP_OK !== $response->getStatusCode()) {
            $message = (false === empty($response->getResponseBody()))
                ? $response->getResponseBody()['error_description']
                : 'The access token provided is invalid';

            throw new AuthenticationException($message, StatusCode::HTTP_FORBIDDEN);
        }

        return ['success' => true];
    }
}
