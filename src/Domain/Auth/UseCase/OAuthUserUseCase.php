<?php
/**
 * OAuthUserUseCase.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\UseCase;

use Application\Http\StatusCode;
use Domain\Auth\Infrastructure\Entity\OAuthUserRepository;
use Domain\Auth\Infrastructure\Exception\AuthenticationException;

/**
 * Class OAuthUserUseCase
 */
class OAuthUserUseCase
{
    /**
     * @var OAuthUserRepository
     */
    private $repository;

    /**
     * @param OAuthUserRepository $repository
     */
    public function __construct(OAuthUserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return array
     * @throws AuthenticationException
     */
    public function authenticateUser(string $username, string $password): array
    {
        if (false === $this->repository->checkUserCredentials($username, $password)) {
            throw new AuthenticationException('Invalid username or password', StatusCode::HTTP_FORBIDDEN);
        }

        return ['success' => true];
    }
}
