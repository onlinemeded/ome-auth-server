<?php
/**
 * AbstractOAuthRepositoryUseCase.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\UseCase;

use Domain\Auth\Repository\OAuthRepository;

/**
 * Class AbstractUseCase
 */
abstract class AbstractOAuthRepositoryUseCase
{
    /**
     * @var OAuthRepository
     */
    protected $repository;

    /**
     * @param OAuthRepository $repository
     */
    public function __construct(OAuthRepository $repository)
    {
        $this->repository = $repository;
    }
}
