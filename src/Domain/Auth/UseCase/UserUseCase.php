<?php
/**
 * UserUseCase.php
 *
 * Copyright 2019 OnlineMedEd, LLC
 */
declare(strict_types=1);

namespace Domain\Auth\UseCase;

use Domain\Auth\Infrastructure\Entity\OAuthUserRepository;
use Domain\Auth\Infrastructure\Entity\UserEntity;
use Domain\Auth\Infrastructure\Entity\UserRepository;

/**
 * Class UserUseCase
 */
class UserUseCase
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var OAuthUserRepository
     */
    private $oAuthUserRepository;

    /**
     * @param UserRepository      $repository
     * @param OAuthUserRepository $oAuthUserRepository
     */
    public function __construct(UserRepository $repository, OAuthUserRepository $oAuthUserRepository)
    {
        $this->repository = $repository;
        $this->oAuthUserRepository = $oAuthUserRepository;
    }

    /**
     * @param string $clientIdentifier
     * @param int    $limit
     * @param int    $offset
     *
     * @return array
     */
    public function getUsers(string $clientIdentifier, int $limit = 20, int $offset = 0): array
    {
        $result = [];
        $users = $this->repository->findUsersByClientIdentifier($clientIdentifier, $limit, $offset);

        if (false === empty($users)) {
            /** @var UserEntity $user */
            foreach ($users as $user) {
                $result[] = $this->formatCustomUser($user);
            }
        }

        return $result;
    }

    /**
     * @param string $userIdentifier
     * @param string $clientIdentifier
     *
     * @return array
     */
    public function getUserById(string $userIdentifier, string $clientIdentifier): array
    {
        $user = $this->repository->findUser($userIdentifier, $clientIdentifier);

        if ((true === empty($user))
            || ($user->getOauthUser()
                    ->getApiApplication()->getOauthClient()->getClientIdentifier() !== $clientIdentifier)
        ) {
            return [];
        }

        return $this->formatCustomUser($user);
    }

    /**
     * @param UserEntity $user
     *
     * @return array
     */
    private function formatCustomUser(UserEntity $user): array
    {
        $userIdentifier = $user->getOauthUser()->getEmail();

        return [
            'id'        => $user->getId(),
            'email'     => $userIdentifier,
            'name'      => $user->getFirstName() . ' ' . $user->getLastName(),
            'user_name' => substr($userIdentifier, 0, strpos($userIdentifier, '@')),
            'title'     => 'Title'
        ];
    }
}
