import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Router, Route, Switch} from 'react-router';

// Helpers & utilities
require('./app/helpers/utils.number');
require('./app/helpers/utils.text');

import history from './app/history';

// Store
import store from './app/store';

// Components
import AuthorizeForm from './app/component/Authorize';
import Error from './app/component/Error';
import LoginForm from './app/component/Login';
import NoMatchError from './app/component/NoMatchError';
import Portal from './app/component/Portal';

const routes = [
    {view: 'account'},
    {view: 'administration', path: '/'},
    {view: 'documentation'},
    {view: 'profile'},
    {view: 'administration', portlet: {view: 'apps'}},
    {view: 'administration', portlet: {view: 'dashboard'}},
    {view: 'administration', portlet: {view: 'logs'}},
    {view: 'administration', portlet: {view: 'users'}}
];

const App = (() => {
    return (
        <Error history={history}>
            <div id='content' className='texture'>
                <Router history={history}>
                    <Switch>
                        {routes.map((route, i) => {
                            const path = route.path || '/' + route.view + (route.portlet ? '/' + route.portlet.view : '');

                            return (
                                <Route
                                    key={i}
                                    exact path={path}
                                    render={(props) => <Portal
                                        {...props}
                                        view={route.view}
                                        portlet={route.portlet || {}}/>}/>
                            );
                        })}
                        <Route exact path='/login' component={LoginForm}/>
                        <Route exact path='/authorize' component={AuthorizeForm}/>
                        <Route component={NoMatchError}/>
                    </Switch>
                </Router>
            </div>
        </Error>
    );
});

import {load} from './app/action/sessionActions';

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('app'),
    function () {
        this.props.store.dispatch(load({
            name: 'Patrick Loven',
            title: 'Job Title',
            email: 'patrick@onlinemeded.org',
            user_name: 'ploven'
        }));
    }
);
