import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Gravatar from 'react-gravatar';
import LinearProgress from '@material-ui/core/LinearProgress';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import MaterialAppBar from '@material-ui/core/AppBar';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

class AppBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleToggle = this.handleToggle.bind(this);

        this.anchorRef = React.createRef();
    }

    handleChange(e) {
        const view = e.target.getAttribute('view');
        const {onChange} = this.props;
        this.props.history.push('/' + view);

        this.setState({open: false}, () => onChange(view));
    }

    handleClose() {
        this.setState({open: false});
    }

    handleToggle() {
        this.setState({open: !this.state.open});
    }

    render() {
        const {open} = this.state;
        const {fetched, session} = this.props;

        return (
            <MaterialAppBar id='app-bar' position='static' component='div'>
                <Toolbar>
                    <Typography
                        component='div'
                        className='app-bar-title'
                        variant='h6'
                        noWrap
                        onClick={(e) => this.handleChange(e, 0)}>
                        OME Identity Management
                    </Typography>
                    <div className='separator'/>
                    {fetched ? (
                        <ButtonGroup
                            component='div'
                            variant='contained'
                            color='primary'
                            ref={(ref) => this.anchorRef = ref}>
                            <Button>
                                <div className='avatar'>
                                    <Gravatar email={session.email} size={32}/>
                                </div>
                                <div className='username'>{session.user_name}</div>
                            </Button>
                            <Button
                                color='primary'
                                variant='contained'
                                size='small'
                                aria-haspopup='true'
                                onClick={this.handleToggle}
                            >
                                <ArrowDropDownIcon/>
                            </Button>
                        </ButtonGroup>
                    ) : (
                        <div className='loading'><LinearProgress/></div>
                    )}
                    {this.anchorRef &&
                    <Popper open={open} anchorEl={this.anchorRef} placement='bottom-end' disablePortal>
                        <Paper>
                            <ClickAwayListener onClickAway={this.handleClose}>
                                <div className='profile-container'>
                                    <List component='ul'>
                                        <ListItem component='li' view='profile' button onClick={this.handleChange}>
                                            Profile
                                        </ListItem>
                                        <ListItem component='li' view='account' button onClick={this.handleChange}>
                                            My account
                                        </ListItem>
                                        <ListItem component='li' view='logout' button onClick={this.handleChange}>
                                            Logout
                                        </ListItem>
                                    </List>
                                </div>
                            </ClickAwayListener>
                        </Paper>
                    </Popper>
                    }
                </Toolbar>
            </MaterialAppBar>
        );
    }
}

AppBar.defaultProps = {
    onChange: () => {
        return false
    }
};

AppBar.propTypes = {
    onChange: PropTypes.func
};

export default connect(
    (store) => {
        return {
            session: store.session.session,
            fetching: store.session.fetching,
            fetched: store.session.fetched,
            error: store.session.error
        };
    }
)(AppBar);
