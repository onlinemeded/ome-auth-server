import React, {Component} from 'react';
import Typography from '@material-ui/core/Typography';

class Footer extends Component {
    render() {
        return (
            <footer>
                <Typography variant='subtitle2' component='div'>
                    Copyright &#169; 2019 OnlineMedEd. All rights reserved.
                </Typography>
                <div className='separator'/>
                {this.props.children}
            </footer>
        );
    }
}

export default Footer;
