import React, {Component} from 'react';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import {authenticateUser} from '../../action/authActions';
import {load} from '../../action/sessionActions';

class LoginForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: 'patrick@onlinemeded.org',
            password: 'medipass'
        };

        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onUsernameChange = this.onUsernameChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onUsernameChange(e) {
        this.setState({username: e.target.value});
    }

    onPasswordChange(e) {
        this.setState({password: e.target.value});
    }

    onSubmit() {
        const {username, password} = this.state;
        const {fetched, session} = this.props;

        authenticateUser(process.env.CLIENT_IDENTIFIER, process.env.CLIENT_SECRET, username, password)
            .then((response) => {
                console.log(response);
            });
    }

    render() {
        const {username, password} = this.state;

        return (
            <div id='login-main' className='texture'>
                <Paper id='login-card' elevation={1}>
                    <Typography variant='h5'>&#160;</Typography>
                    <Typography variant='h4'>OnLineMedEd</Typography>
                    <div id='login-card-content'>
                        <TextField
                            value={username}
                            onChange={this.onUsernameChange}
                            placeholder='Username'/>
                        <TextField
                            value={password}
                            type='password'
                            onChange={this.onPasswordChange}
                            placeholder='Password'/>
                        <Button size='large'
                                variant='contained'
                                color='primary'
                                onClick={this.onSubmit}
                        >
                            Login
                        </Button>
                        <Typography variant='subtitle2'>Forgot your password?</Typography>
                    </div>
                </Paper>
            </div>
        );
    }
}

export default connect(
    (store) => {
        return {
            session: store.session.session,
            fetching: store.session.fetching,
            fetched: store.session.fetched,
            error: store.session.error
        };
    }
)(LoginForm);
