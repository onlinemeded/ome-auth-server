import React, {Component} from 'react';
import store from '../store';
import history from '../history';

class NoMatchError extends Component {
    constructor(props) {
        super(props);

        store.dispatch({
            type: 'SET_ERROR', payload: {
                response: {
                    data: history.location.pathname + ' could not be found',
                    status: 404,
                    statusText: 'Not Found'
                }
            }
        });
    }

    render() {
        return null;
    }
}

export default NoMatchError;
