import React, {Component} from 'react';

import Account from './Account';
import Administration from './Administration';
import AppBar from './Header/AppBar';
import Footer from './Footer';
import Profile from './Profile';

class Portal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            view: props.view,
            portlet: props.portlet
        };

        this.handleAccountOnChange = this.handleAccountOnChange.bind(this);
    }

    handleAccountOnChange(view) {
        this.setState({view: view});
    }

    render() {
        const {history} = this.props;
        const {portlet, view} = this.state;

        const usersView = (() => {
            switch (view) {
                case 'documentation':
                    return (<div>Documentation</div>);
                case 'account':
                    return (<Account/>);
                case 'profile':
                    return (<Profile/>);
                default:
                    return (<Administration history={history} view={portlet.view}/>);
            }
        })();

        return (
            <div id='portal'>
                <div className='viewport'>
                    <div className='viewport-header'><AppBar history={history}/></div>
                    <div className='viewport-body no-pad'>{usersView}</div>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Portal;
