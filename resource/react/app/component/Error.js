import React, {Component} from 'react';
import {connect} from 'react-redux';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import AppBar from './Header/AppBar';
import Footer from './Footer';

class Error extends Component {
    constructor(props) {
        super(props);

        this.state = {
            history: props.history
        };
    }

    render() {
        const {hasError, error, history} = this.props;

        if (true === hasError) {
            const {status, statusText, data} = error.response;

            return (
                <div id='error' className='texture'>
                    <div className='viewport'>
                        <div className='viewport-header'><AppBar history={history}/></div>
                        <div className='viewport-body texture'>
                            <Paper className='error-info' elevation={1}>
                                <Typography variant='h5'>&#160;</Typography>
                                <div className='error-type'>
                                    <Typography variant='h4' component='div'>{status} {statusText}</Typography>
                                </div>
                                <div className='error-details'>
                                    <Typography variant='subtitle1' component='p'>{data}</Typography>
                                </div>
                            </Paper>
                        </div>
                    </div>
                    <Footer/>
                </div>
            );
        }

        return this.props.children;
    }
}

export default connect(
    (store) => {
        return {
            hasError: store.error.hasError,
            error: store.error.error
        }
    }
)(Error);
