import React, {Component} from 'react';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import {findByClientIdentifier} from '../../action/clientActions';

class AuthorizeForm extends Component {
    constructor(props) {
        super(props);

        const query = new URLSearchParams(props.location.search);

        this.state = {
            username: '',
            password: '',
            query: query,
            client: query.get('client')
        };

        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onUsernameChange = this.onUsernameChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        if (this.state.client) {
            this.props.dispatch(findByClientIdentifier(this.state.client));
        }
    }

    onUsernameChange(e) {
        this.setState({username: e.target.value});
    }

    onPasswordChange(e) {
        this.setState({password: e.target.value});
    }

    onSubmit() {
        console.log(this.state);
    }

    render() {
        const {fetched, clientDetail} = this.props;
        const {username, password, client} = this.state;

        if (!fetched) {
            return null;
        }

        return (
            <div id='login-main' className='texture'>
                <Paper id='login-card' elevation={1}>
                    <Typography variant='h5'>&#160;</Typography>
                    <Typography variant='h4'>OnLineMedEd</Typography>
                    <div id='login-card-content'>
                        <Typography component='div'>Do you want authorize <span>{client}</span>.<br/>
                            The following access
                            <ul>
                                <li>{clientDetail.redirectUrl}</li>
                                <li>Item Two</li>
                            </ul>
                        </Typography>
                        <TextField
                            value={username}
                            onChange={this.onUsernameChange}
                            placeholder='Username'/>
                        <TextField
                            value={password}
                            type='password'
                            onChange={this.onPasswordChange}
                            placeholder='Password'/>
                        <Button size='large'
                                variant='contained'
                                color='primary'
                                onClick={this.onSubmit}
                        >
                            Authorize {client}
                        </Button>
                    </div>
                </Paper>
            </div>
        );
    }
}

export default connect(
    (store) => {
        return {
            clientDetail: store.client.details,
            fetching: store.client.fetching,
            fetched: store.client.fetched,
            error: store.client.error
        };
    }
)(AuthorizeForm);
