import React, {Component} from 'react';
import UsersIcon from '@material-ui/icons/Contacts';
import AppsIcon from '@material-ui/icons/Widgets';
import LogsIcon from '@material-ui/icons/Receipt';
import DashboardIcon from '@material-ui/icons/AvTimer';

import Apps from './Apps';
import Dashboard from './Dashboard';
import NavBar from './NavBar';
import UserExplorer from './UserExplorer';

class Administration extends Component {
    constructor(props) {
        super(props);

        this.state = {
            view: props.view || 'apps'
        };

        this.handleItemClick = this.handleItemClick.bind(this);
    }

    handleItemClick(view) {
        this.props.history.push('/administration/' + view);
        this.setState({view: view});
    }

    render() {
        const {view} = this.state;
        const {history} = this.props;
        const listItems = [
            {value: 'dashboard', icon: <DashboardIcon/>, label: 'Dashboard', handleItemClick: this.handleItemClick},
            {value: 'apps', icon: <AppsIcon/>, label: 'App\'s and API\'s', handleItemClick: this.handleItemClick},
            {value: 'users', icon: <UsersIcon/>, label: 'Users', handleItemClick: this.handleItemClick},
            {value: 'logs', icon: <LogsIcon/>, label: 'Logs', handleItemClick: this.handleItemClick}
        ];

        const usersView = (() => {
            switch (view) {
                case 'apps':
                    return (<Apps/>);
                case 'dashboard':
                    return (<Dashboard/>);
                case 'users':
                    return (<UserExplorer/>);
                default:
                    return (<div>Default</div>);
            }
        })();

        return (
            <div id='administration'>
                <div className='nav-bar'>
                    <NavBar history={history} selected={view} listItems={listItems}/>
                </div>
                <div className='content no-pad'>{usersView}</div>
            </div>
        );
    }
}

export default Administration;
