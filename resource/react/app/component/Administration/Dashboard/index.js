import React, {Component} from 'react';

import UserQuickStats from './UserQuickStat';

class Dashboard extends Component {
    render() {
        return (
            <div id='dashboard'>
                <UserQuickStats/>
            </div>
        );
    }
}

export default Dashboard;
