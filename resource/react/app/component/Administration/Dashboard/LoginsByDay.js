import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import {
    event as d3Event,
    nest as d3Nest,
    range as d3Range,
    scale as d3Scale,
    select as d3Select,
    time as d3Time
} from 'd3';

import {loginMetrics} from '../../../action/metricsActions';

class LoginsByDay extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userLogin: []
        };

        this.drawChart = this.drawChart.bind(this);
    }

    componentDidMount() {
        loginMetrics().then((data) => this.setState({userLogin: data}, () => {
            this.drawChart();
            window.addEventListener('resize', () => this.drawChart());
        }));
    }

    drawChart() {
        const {userLogin} = this.state;
        const {calendarDateSize, chartProperties, monthsPerRow} = this.props;

        let chartDiv = document.getElementById(chartProperties.id),
            cellSize = calendarDateSize,
            noMonthsInARow = monthsPerRow,
            shiftUp = cellSize * 3 * 2,
            day = d3Time.format('%w'),
            week = d3Time.format('%U'),
            month = d3Time.format('%m'),
            year = d3Time.format('%Y'),
            format = d3Time.format('%Y-%m-%d'),
            color = d3Scale.quantize()
                .domain([-.05, .05])
                .range(d3Range(11).map((d) => {
                    return 'q' + d + '-11';
                }));

        let svg = d3Select(chartDiv)
            .selectAll('svg')
            .data([new Date().getFullYear()])
            .enter()
            .append('svg')
            .attr('width', chartDiv.clientWidth)
            .attr('height', chartDiv.clientHeight)
            .append('g');

        let rect = svg.selectAll('.day')
            .data((d) => {
                return d3Time.days(new Date(d, 0, 1), new Date(d + 1, 0, 1));
            })
            .enter()
            .append('rect')
            .attr('class', 'day')
            .attr('width', cellSize)
            .attr('height', cellSize)
            .attr('x', (d) => {
                return day(d) * cellSize + (1.2 * cellSize * 7 * ((month(d) - 1) % (noMonthsInARow)));
            })
            .attr('y', (d) => {
                return ((week(d) - week(new Date(year(d), month(d) - 1, 1))) * cellSize) + (Math.ceil(month(d) / (noMonthsInARow))) * cellSize * 8 - cellSize / 2 - shiftUp;
            })
            .datum(format);

        svg.selectAll('.month-title')
            .data((d) => {
                return d3Time.months(new Date(d, 0, 1), new Date(d + 1, 0, 1));
            })
            .enter()
            .append('text')
            .text(monthTitle)
            .attr('x', (d, i) => {
                return 1.2 * cellSize * 7 * ((month(d) - 1) % (noMonthsInARow));
            })
            .attr('y', (d, i) => {
                return ((week(d) - week(new Date(year(d), month(d) - 1, 1))) * cellSize) + (Math.ceil(month(d) / (noMonthsInARow))) * cellSize * 8 + cellSize + 4;
            })
            .attr('class', 'month-title')
            .attr('d', monthTitle);


        //  Tooltip Object
        let tooltip = document.getElementById('logins-tooltip') || d3Select('body')
            .append('div')
            .attr('id', 'logins-tooltip')
            .style('position', 'absolute')
            .style('z-index', '10')
            .style('visibility', 'hidden')
            .text('a simple tooltip');

        let data = d3Nest()
            .key((d) => {
                return d.date;
            })
            .rollup((d) => {
                return {'weight': (d[0].logins / userLogin.total), 'count': d[0].logins};
            })
            .map(userLogin.days);

        rect.filter((d) => {
            return d in data;
        })
            .attr('class', (d) => {
                return 'day ' + color(data[d].weight);
            })
            .select('title')
            .text((d) => {
                return d + ': ' + data[d].logins;
            });

        //  Tooltip
        rect.on('mouseover', mouseover);
        rect.on('mouseout', mouseout);

        function mouseover(d) {
            let percent_data = (data[d] !== undefined) ? data[d].count : 0,
                purchase_text = d + ': ' + percent_data.numberFormat();

            tooltip.style('visibility', 'visible');
            tooltip
                .transition()
                .duration(200)
                .style('opacity', .9);
            tooltip
                .html(purchase_text)
                .style('left', (d3Event.pageX) + 30 + 'px')
                .style('top', (d3Event.pageY) + 'px');
        }

        function mouseout(d) {
            tooltip.transition()
                .duration(500)
                .style('opacity', 0);
        }

        function monthTitle(t0) {
            return t0.toLocaleString('en-us', {month: 'long'});
        }
    }

    render() {
        return (
            <Card elevation={0} id='logins-chart'>
                <CardContent className='chart-content'>
                    <Typography variant='subtitle1' className='chart-title'>Logins for the Last 12 mos.</Typography>
                    <div id='chart-panel'/>
                </CardContent>
            </Card>
        );
    }
}

LoginsByDay.defaultProps = {
    calendarDateSize: 10,
    monthsPerRow: 12,
    chartProperties: {
        id: 'chart-panel'
    }
};

LoginsByDay.propTypes = {
    calendarDateSize: PropTypes.number,
    monthsPerRow: PropTypes.number,
    chartProperties: PropTypes.object
};

export default LoginsByDay;
