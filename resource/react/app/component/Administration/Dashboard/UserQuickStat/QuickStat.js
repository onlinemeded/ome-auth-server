import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

class QuickStat extends Component {
    render() {
        const {title, metric, caption} = this.props;

        return (
            <Grid item xs component='div' className='quick-stat'>
                {title &&
                <Typography variant='subtitle1'>{title}</Typography>
                }
                <Typography variant='h4'>{metric.numberFormat()}</Typography>
                {caption &&
                <Typography variant='caption'>{caption}</Typography>
                }
            </Grid>
        );
    }
}

QuickStat.defaultProps = {
    title: null,
    metric: 0,
    caption: null
};

QuickStat.propTypes = {
    title: PropTypes.string,
    metric: PropTypes.string,
    caption: PropTypes.string,
};

export default QuickStat;
