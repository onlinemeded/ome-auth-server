import React, {Component} from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';

import {usersQuickStats} from '../../../../action/metricsActions';
import QuickStat from './QuickStat';

class UsersQuickStats extends Component {
    constructor(props) {
        super(props);

        this.state = {
            metrics: [],
            fetched: false
        };
    }

    componentDidMount() {
        usersQuickStats().then((data) => this.setState({metrics: data, fetched: true}));
    }

    render() {
        const {metrics, fetched} = this.state;

        if (!fetched) {
            return null;
        }

        return (
            <Card elevation={0} id='users-quick-stat'>
                <CardContent>
                    <Grid container component='div'>
                        <QuickStat title='Users' metric={metrics.totalUsers} caption='All Time'/>
                        <QuickStat title='Active Users' metric={metrics.lastActive30Days} caption='Last 30 Days'/>
                        <QuickStat title='Logins' metric={metrics.lastLogin30Days} caption='Last 30 Days'/>
                        <QuickStat title='Sign-Ups' metric={metrics.lastSignUp30Days} caption='Last 30 Days'/>
                    </Grid>
                </CardContent>
            </Card>
        );
    }
}

export default UsersQuickStats;
