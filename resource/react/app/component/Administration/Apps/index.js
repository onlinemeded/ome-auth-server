import React, {Component} from 'react';
import {connect} from 'react-redux';

import {fetchApps} from '../../../action/appActions';

class Apps extends Component {
    constructor(props) {
        super(props);

        this.state = {
            apps: [],
            fetched: false
        };
    }

    componentDidMount() {
        fetchApps().then((data) => this.setState({apps: data, fetched: true}));
    }

    render() {
        const {apps, fetched} = this.state;

        if (!fetched) {
            return null;
        }

        return (
            <div>
                Apps and API's
                {apps.map((app, i) => {
                    return (
                        <div key={i}>{app.name}</div>
                    );
                })
                }
            </div>
        );
    }
}

export default Apps;
