import React, {Component} from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

class NavBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            listItems: props.listItems,
            selected: props.selected
        };

        this.handleItemClick = this.handleItemClick.bind(this);
    }

    componentDidMount() {
        let listItems = this.state.listItems,
            item = this.state.listItems.filter((listItem) => listItem.value === this.state.selected)[0];

        listItems.forEach((listItem) => {
            listItem.selected = listItem.value === item.value;
        });

        this.setState({listItems: listItems});
    }

    handleItemClick(item) {
        let listItems = this.state.listItems;
        listItems.forEach((listItem) => {
            listItem.selected = listItem.value === item.value;
        });

        this.setState({listItems: listItems}, () => item.handleItemClick(item.value, item));
    }

    render() {
        const {listItems} = this.state;

        return (
            <List component='ul' dense>
                {listItems.map((item, i) => {
                    return (
                        <ListItem
                            key={i}
                            component='li'
                            selected={item.selected}
                            value={item.value}
                            title={item.label}
                            onClick={() => this.handleItemClick(item)}>
                            <ListItemIcon>{item.icon}</ListItemIcon>
                            <ListItemText primary={item.label}/>
                        </ListItem>
                    );
                })}
            </List>
        );
    }
}

NavBar.propTypes = {
    selected: PropTypes.string.isRequired,
    listItems: PropTypes.array.isRequired
};

export default NavBar;
