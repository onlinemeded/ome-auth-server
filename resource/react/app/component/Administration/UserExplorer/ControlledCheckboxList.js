import React, {Component} from 'react';
import PropTypes from 'prop-types';

class ControlledCheckboxList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: props.checked || []
        };

        this.handleUserCheck = this.handleUserCheck.bind(this);
        this.getCheckedList = this.getCheckedList.bind(this);
        this.updateChecked = this.updateChecked.bind(this);
    }

    componentDidMount() {
        this.props.childRef(this);
    }

    componentWillUnmount() {
        this.props.childRef(undefined);
    }

    static getDerivedStateFromProps(props, state) {
        if (0 === state.checked.length && 0 < props.users.length) {
            state.checked = new Array(props.users.length).fill(false);
            props.updateChecked(state.checked);
        }

        return state;
    }

    handleUserCheck(index, checked) {
        this.state.checked[index] = checked;
        this.setState({checked: this.state.checked}, () => {
            this.props.updateChecked(this.state.checked);
        });
    }

    getCheckedList() {
        return this.state.checked;
    }

    updateChecked(checked) {
        this.setState({checked: checked});
    }
}

ControlledCheckboxList.defaultProps = {
    checked: []
};

ControlledCheckboxList.propTypes = {
    childRef: PropTypes.func.isRequired,
    updateChecked: PropTypes.func,
    checked: PropTypes.array
};

export default ControlledCheckboxList;
