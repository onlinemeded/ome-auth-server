import React from 'react';
import {connect} from 'react-redux';
import Avatar from '@material-ui/core/Avatar';
import CheckCircle from '@material-ui/icons/CheckCircle';
import Checkbox from '@material-ui/core/Checkbox';
import Gravatar from 'react-gravatar';
import Paper from '@material-ui/core/Paper';
import RadioButtonUncheckedOutlined from '@material-ui/icons/RadioButtonUncheckedOutlined';
import Typography from '@material-ui/core/Typography';

import ControlledCheckboxList from '../ControlledCheckboxList';

class CardList extends ControlledCheckboxList {
    render() {
        const {users} = this.props;
        const {checked} = this.state;

        return (
            <div id='user-explorer-view'>
                {users.map((item, i) => {
                    return (
                        <div key={i} className='user-profile-compact'>
                            <Paper elevation={1} className='paper-container'>
                                <div className='paper-header'>
                                    <Checkbox
                                        color='primary'
                                        checked={checked[i]}
                                        onChange={(e) => {
                                            this.handleUserCheck(i, e.target.checked)
                                        }}
                                        checkedIcon={<CheckCircle/>}
                                        icon={<RadioButtonUncheckedOutlined/>}
                                        value={i}
                                    />
                                </div>
                                <hr/>
                                <Typography variant='h6' component='div' className='paper-name'>{item.name}</Typography>
                                <div className='paper-content'>
                                    <Avatar alt={item.name}><Gravatar email={item.email} size={60}/></Avatar>
                                    <Typography variant='caption' component='div'>{item.title}</Typography>
                                    <hr className='x2'/>
                                    <Typography variant='subtitle2' component='div'>Email</Typography>
                                    <Typography variant='caption' component='div'>{item.email}</Typography>
                                </div>
                            </Paper>
                        </div>
                    );
                })}
            </div>
        );
    }
}

export default connect(
    (store) => {
        return {
            users: store.users.users,
            fetching: store.users.fetching,
            fetched: store.users.fetched,
            error: store.users.error
        };
    }
)(CardList);
