import React, {Component} from 'react';
import {connect} from 'react-redux';
import AvatarListViewIcon from '@material-ui/icons/ViewList';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ListViewIcon from '@material-ui/icons/ViewHeadline';
import MenuItem from '@material-ui/core/MenuItem';
import PaperViewIcon from '@material-ui/icons/ViewModule';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import Typography from '@material-ui/core/Typography';

import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';

import {findAll} from '../../../action/usersActions';
import Select from '../../controls/Select';
import AvatarList from './AvatarList';
import CardList from './CardList';
import DefaultList from './DefaultList';

class UserExplorer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            view: 0,
            sort: 'created',
            checked: [],
            userQuery: ''
        };

        this.viewList = null;
        this.handleChange = this.handleChange.bind(this);
        this.handleSelectMacro = this.handleSelectMacro.bind(this);
        this.handleUserQueryKeyUp = this.handleUserQueryKeyUp.bind(this);
        this.setUserQuery = this.setUserQuery.bind(this);
        this.updateChecked = this.updateChecked.bind(this);

        props.dispatch(findAll('omeMain'));
    }

    handleChange(event, view) {
        this.setState({view: view});
    }

    handleSelectMacro(e) {
        let selector = e.target,
            checked = this.state.checked;

        if (!selector.hasAttribute('value')) {
            selector = e.target.parentNode;
        }

        switch (selector.value) {
            case 'all':
                checked = new Array(checked.length).fill(true);
                break;
            case 'none':
                checked = new Array(checked.length).fill(false);
                break;
            case 'inverse':
                checked.map((item, i) => checked[i] = !item);
                break;
        }

        this.setState({checked: checked}, () => this.viewList.updateChecked(checked));
    }

    handleUserQueryKeyUp(e) {
        this.setUserQuery(e.target.value);

        if (e.target.value.length > 2) {

        }
    }

    setUserQuery(query) {
        this.setState({userQuery: query})
    }

    updateChecked(checked) {
        this.setState({checked: checked});
    }

    render() {
        const {fetched} = this.props;
        const {view, sort, checked, userQuery} = this.state;

        if (!fetched) {
            return null;
        }

        const usersView = (() => {
            switch (view) {
                case 1:
                    return (<AvatarList
                        updateChecked={this.updateChecked}
                        childRef={(ref) => this.viewList = ref}
                        checked={checked}/>);
                case 2:
                    return (<CardList
                        updateChecked={this.updateChecked}
                        childRef={(ref) => this.viewList = ref}
                        checked={checked}/>);
                default:
                    return (<DefaultList
                        updateChecked={this.updateChecked}
                        childRef={(ref) => this.viewList = ref}
                        checked={checked}/>);
            }
        })();

        return (
            <div id='user-explorer'>
                <div id='user-explorer-controls'>
                    <Paper className='search-container'>
                        <InputBase placeholder='Search' value={userQuery} onChange={this.handleUserQueryKeyUp}/>
                        <IconButton aria-label='search'>
                            <SearchIcon/>
                        </IconButton>
                        <Divider/>
                        <IconButton aria-label='menu'>
                            <MenuIcon/>
                        </IconButton>
                    </Paper>
                    <div className='spacer'/>
                    <Typography variant='subtitle2' component='div'>Sort</Typography>
                    <Select
                        value={sort}
                        onChange={() => {
                            console.log('I changed');
                        }}
                        inputProps={{
                            name: 'user-sort',
                            id: 'user-sort-select',
                        }}
                    >
                        <MenuItem value='name'>Name</MenuItem>
                        <MenuItem value='created'>Created Date</MenuItem>
                    </Select>
                    <div className='spacer'/>
                    <Typography variant='subtitle2' component='div'>Select</Typography>
                    <ButtonGroup component='div' size='small'>
                        <Button value='all' onClick={this.handleSelectMacro}>All</Button>
                        <Button value='none' onClick={this.handleSelectMacro}>None</Button>
                        <Button value='inverse' onClick={this.handleSelectMacro}>Inverse</Button>
                    </ButtonGroup>
                    <div className='separator'/>
                    <ToggleButtonGroup
                        value={view}
                        exclusive
                        onChange={this.handleChange}
                        variant='contained'
                        size='small'
                        aria-label='small contained button group'>
                        <ToggleButton value={0}><ListViewIcon/></ToggleButton>
                        <ToggleButton value={1}><AvatarListViewIcon/></ToggleButton>
                        <ToggleButton value={2}><PaperViewIcon/></ToggleButton>
                    </ToggleButtonGroup>
                </div>
                {usersView}
            </div>);
    }
}

export default connect(
    (store) => {
        return {
            users: store.users.users,
            fetching: store.users.fetching,
            fetched: store.users.fetched,
            error: store.users.error
        };
    }
)(UserExplorer);
