import React from 'react';
import {connect} from 'react-redux';
import CheckCircle from '@material-ui/icons/CheckCircle';
import Checkbox from '@material-ui/core/Checkbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Paper from '@material-ui/core/Paper';
import RadioButtonUncheckedOutlined from '@material-ui/icons/RadioButtonUncheckedOutlined';

import ControlledCheckboxList from '../ControlledCheckboxList';

class DefaultList extends ControlledCheckboxList {
    render() {
        const {users} = this.props;
        const {checked} = this.state;

        return (
            <Paper id='user-explorer-view'>
                <List className='user-profile-list' component='ul' dense>
                    {users.map((item, i) => {
                        return (
                            <ListItem key={i} component='li' alignItems='flex-start'
                                      divider={!(i + 1 === users.length)}>
                                <ListItemText
                                    primary={item.name}
                                    primaryTypographyProps={{className: 'list-name'}}
                                    onClick={(e) => {
                                        this.handleUserCheck(i, !checked[i])
                                    }}
                                />
                                <ListItemSecondaryAction>
                                    <Checkbox
                                        edge='end'
                                        checked={checked[i]}
                                        onChange={(e) => {
                                            this.handleUserCheck(i, e.target.checked)
                                        }}
                                        checkedIcon={<CheckCircle/>}
                                        icon={<RadioButtonUncheckedOutlined/>}
                                    />
                                </ListItemSecondaryAction>
                            </ListItem>
                        );
                    })}
                </List>
            </Paper>
        );
    }
}

export default connect(
    (store) => {
        return {
            users: store.users.users,
            fetching: store.users.fetching,
            fetched: store.users.fetched,
            error: store.users.error
        };
    }
)(DefaultList);
