import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import MUI_Select from '@material-ui/core/Select';

const CustomInput = withStyles(theme => ({
    input: {
        borderRadius: 2,
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: 'solid transparent 1px',
        fontSize: 13,
        padding: '4px 20px 5px 6px',
        boxShadow: '0px 1px 3px 0px rgba(0, 0, 0, 0.1), 0px 1px 1px 0px rgba(0, 0, 0, 0.04), 0px 2px 1px -1px rgba(0, 0, 0, 0.02)',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        // Use the system font instead of the default Roboto font.
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            'Segoe UI',
            'Roboto',
            'Helvetica Neue',
            'Arial',
            'sans-serif',
            'Apple Color Emoji',
            'Segoe UI Emoji',
            'Segoe UI Symbol',
        ].join(','),
        '&:focus': {
            borderRadius: 4,
            borderColor: '#80bdff',
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
        }
    }
}))(InputBase);

class Select extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: props.value
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({value: e.target.value});
    }

    render() {
        const {value} = this.state;
        const {inputProps} = this.props;

        return (
            <MUI_Select
                value={value}
                onChange={this.handleChange}
                input={<CustomInput {...inputProps} />}
            >
                {this.props.children}
            </MUI_Select>
        );
    }
}

Select.defaultProps = {
    inputProps: {}
};

Select.propTypes = {
    inputProps: PropTypes.object
};

export default Select;
