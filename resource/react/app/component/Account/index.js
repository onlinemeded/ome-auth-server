import React, {Component} from 'react';
import Gravatar from 'react-gravatar';
import Avatar from '@material-ui/core/Avatar';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Typography from '@material-ui/core/Typography';

class Account extends Component {
    render() {
        return (
            <div id='account'>
                <Card>
                    <CardHeader
                        avatar={
                            <Avatar alt='AltText'><Gravatar email={'patrick@onlinemeded.org'}/></Avatar>
                        }
                        action={
                            <IconButton aria-label='settings'>
                                <MoreVertIcon/>
                            </IconButton>
                        }
                        title='Patrick Loven'
                        subheader='Job Title'
                    />
                    <CardContent>
                        <Typography variant='body2' component='p'>
                            There should probably be something interesting here instead.
                        </Typography>
                    </CardContent>
                </Card>
            </div>
        );
    }
}

export default Account;
