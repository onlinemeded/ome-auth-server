import signedRequest from '../helpers/signed-request';

export function usersQuickStats() {
    const response = {
        data: {
            totalUsers: '302444',
            lastActive30Days: '47921',
            lastLogin30Days: '47921',
            lastSignUp30Days: '5497'
        }
    };

    return Promise.resolve(response.data);
}
