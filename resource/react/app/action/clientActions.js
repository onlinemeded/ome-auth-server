import signedRequest from '../helpers/signed-request';

export function findByClientIdentifier(identifier, callback = () => {}) {
    return (dispatch) => {
        signedRequest.get('/v1/client/' + identifier, callback)
            .then((response) => {
                dispatch({type: 'FETCH_CLIENT_BY_IDENTIFIER_FULFILLED', payload: response.data});

                return Promise.resolve(response.data);
            })
            .catch((err) => {
                dispatch({type: 'FETCH_CLIENT_BY_IDENTIFIER_REJECTED', payload: err});
            });
    }
}

export function findAll() {
    return (dispatch) => {
        signedRequest.get('/v1/clients')
            .then((response) => {
                dispatch({type: 'FETCH_CLIENTS_FULFILLED', payload: response.data});

                return Promise.resolve(response.data);
            })
            .catch((err) => {
                dispatch({type: 'FETCH_CLIENTS_REJECTED', payload: err});
            });
    }
}
