import signedRequest from '../helpers/signed-request';

export function findAll(identifier) {
    return (dispatch) => {
        signedRequest.get('/v1/client/' + identifier + '/users')
            .then((response) => {
                dispatch({type: 'FETCH_USERS_FULFILLED', payload: response.data});

                return Promise.resolve(response.data);
            })
            .catch((err) => {
                dispatch({type: 'FETCH_USERS_REJECTED', payload: err});
            });
    }
}
