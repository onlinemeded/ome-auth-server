import signedRequest from '../helpers/signed-request';

export function fetchApps() {
    return signedRequest.get('/v1/apps')
        .then((response) => {
            return Promise.resolve(response.data);
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}
