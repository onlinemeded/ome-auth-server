import moment from 'moment';

const sync = {
    lastSync: false
 };

export function load(defaultState) {
    return (dispatch) => {
        if (sync.lastSync) {
            return {};
        }

        let session = localStorage.getItem('_uS');

        if (session && typeof JSON.parse(session) === 'object') {
            session = JSON.parse(session);
        } else {
            session = defaultState;
            localStorage.setItem('_uS', JSON.stringify(session));
        }

        sync.lastSync = moment.utc();
        dispatch({type: 'FETCH_SESSION_FULFILLED', payload: session});

        return Promise.resolve(session);
    }
}
