import signedRequest from '../helpers/signed-request';

export function authenticateUser(client, secret, username, password) {
    return signedRequest.authenticate(client, secret, username, password, {uri: '/v1/sso/token'})
        .then((response) => {
            return Promise.resolve(response.data);
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}
