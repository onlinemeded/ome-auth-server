export default function reducer(state = {
    clients: [],
    details: [],
    fetching: false,
    fetched: false,
    error: null
}, action) {
    switch (action.type) {
        case 'FETCH_CLIENT_BY_IDENTIFIER': {
            return {...state, fetching: true}
        }

        case 'FETCH_CLIENT_BY_IDENTIFIER_REJECTED': {
            return {...state, fetching: false, error: action.payload}
        }

        case 'FETCH_CLIENT_BY_IDENTIFIER_FULFILLED': {
            return {...state, fetching: false, fetched: true, details: action.payload}
        }

        case 'FETCH_CLIENTS': {
            return {...state, fetching: true}
        }

        case 'FETCH_CLIENTS_REJECTED': {
            return {...state, fetching: false, error: action.payload}
        }

        case 'FETCH_CLIENTS_FULFILLED': {
            return {...state, fetching: false, fetched: true, details: action.payload}
        }
    }

    return state;
};
