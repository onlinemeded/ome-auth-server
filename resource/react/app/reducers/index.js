import {combineReducers} from 'redux';

import client from './clientReducer';
import error from './errorReducer';
import session from './sessionReducer';
import users from './usersReducer';

export default combineReducers({client, error, session, users});
