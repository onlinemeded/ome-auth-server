export default function reducer(state = {
    session: {},
    fetching: false,
    fetched: false,
    error: null
}, action) {
    switch (action.type) {
        case 'FETCH_SESSION': {
            return {...state, fetching: true}
        }

        case 'FETCH_SESSION_REJECTED': {
            return {...state, fetching: false, error: action.payload}
        }

        case 'FETCH_SESSION_FULFILLED': {
            return {...state, fetching: false, fetched: true, session: action.payload}
        }
    }

    return state;
};
