export default function reducer(state = {
    hasError: false,
    error: {}
}, action) {
    switch (action.type) {
        case 'SET_ERROR': {
            return {...state, hasError: true, error: action.payload}
        }
    }

    return state;
};