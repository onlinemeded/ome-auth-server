import axios from "axios";
import moment from 'moment';
import CryptoJS from 'crypto-js';

const AESEncryptionLength = 256;

const prepareUrl = (uri) => {
    if (-1 === uri.indexOf('://')) {
        uri = 'http://' + process.env.API_HOST + uri;
    }

    return uri;
};

const createJwt = (apiKey, header, data) => {
    // {...{iat: Math.floor(Date.now() / 1000) + 257}, ...data}
    let token = base64url(CryptoJS.enc.Utf8.parse(JSON.stringify(header))) + '.'
        + base64url(CryptoJS.enc.Utf8.parse(JSON.stringify(data)));

    return token + '.' + base64url(CryptoJS.HmacSHA512(token, apiKey));
};

const base64url = (source) => {
    let encodedSource = CryptoJS.enc.Base64.stringify(source);
    encodedSource = encodedSource.replace(/=+$/, '');
    encodedSource = encodedSource.replace(/\+/g, '-');
    encodedSource = encodedSource.replace(/\//g, '_');

    return encodedSource;
};

const encrypt = (string, key) => {
    let iv = CryptoJS.lib.WordArray.random(16),
        salt = CryptoJS.lib.WordArray.random(16),
        encryptMethodLength = (AESEncryptionLength / 4),
        hashKey = CryptoJS.PBKDF2(key, salt, {
            hasher: CryptoJS.algo.SHA512,
            keySize: (encryptMethodLength / 8),
            iterations: 1
        }),
        encryptedString = CryptoJS.enc.Base64.stringify(CryptoJS.AES.encrypt(string, hashKey, {
            mode: CryptoJS.mode.CBC,
            iv: iv
        }).ciphertext),
        hmac = CryptoJS.HmacSHA512(encryptedString.toString(), key);

    return base64url(CryptoJS.enc.Utf8.parse(CryptoJS.enc.Hex.stringify(iv) + CryptoJS.enc.Hex.stringify(salt) + hmac.toString() + encryptedString));
};

const hydratePayload = (payload) => {
    if ('function' === typeof payload) {
        payload = payload();
    }

    return payload;
};

const signedRequest = (apiKey = null) => {
    const defaultOptions = {};

    function genFromUri(uri, payload) {
        uri = prepareUrl(uri);
        let ts = moment().utc().format('ddd, DD MMM Y HH:mm:ss') + ' GMT',
            url = uri.substring(uri.indexOf(process.env.API_HOST) + process.env.API_HOST.length),
            sig = [process.env.API_HOST, url, ts].join(':');

        return {
            headers: {
                'OME-SIG': [apiKey, CryptoJS.HmacSHA256(sig, apiKey).toString()].join(';'),
                'OME-Date': ts,
                'Authorization': 'Bearer '
                    + createJwt(
                        apiKey, {typ: 'JWT', alg: 'HS512'},
                        {payload: encrypt(JSON.stringify(payload), apiKey)}
                    )
            }
        };
    }

    return {
        get: (uri, payload = {}, options = {}) => {
            return axios.get(prepareUrl(uri), {...defaultOptions, ...genFromUri(uri, hydratePayload(payload)), ...options});
        },
        post: (uri, payload = {}, options = {}) => {
            payload = hydratePayload(payload);

            return axios.post(prepareUrl(uri), payload, {...defaultOptions, ...genFromUri(uri, payload), ...options});
        },
        authenticate: (client, secret, username, password, options = {}) => {
            let payload = {'grant_type': 'password', username: username, password: password},
                config = genFromUri(options.uri, payload);

            config.headers.Authorization = 'Basic ' + base64url(CryptoJS.enc.Utf8.parse(client + ':' + secret));

            return axios.post(prepareUrl(options.uri), payload, {...defaultOptions, ...config, ...options});
        }
    };
};

export default signedRequest(process.env.API_KEY);
