const NumberFormatter = new Intl.NumberFormat('en-us', {style: 'decimal', currency: 'USD'});

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

String.prototype.numberFormat = function () {
    return NumberFormatter.format(this);
};
