const NumberFormatter = new Intl.NumberFormat('en-us', {style: 'decimal', currency: 'USD'});

Number.prototype.numberFormat = function () {
    return NumberFormatter.format(this);
};
